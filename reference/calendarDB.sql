drop table NPP_reserve_period_state;

create table NPP_reserve_period_state(
  reservationNum VarChar(20)  primary key, --예약고유번호
  roomID VarChar(20) not null,  --  방ID
  startDate VarChar(20)  not null, -- 시작일
  endDate VarChar(20)  not null, -- 종료일
  subscriberID VarChar(20)  not null -- 예약자ID
);

INSERT INTO NPP_RESERVE_PERIOD_STATE VALUES ('1', '1', '2018-03-27', '2018-03-30', 'guest1');
--일자 입력 시 0000-00-00 형식으로 넣어야 API에서 정확히 인식됨. 중간의 '-' 절대 생략하지 말 것

commit; -- 자료넣고나서 커밋좀 해라!!

select * from NPP_reserve_period_state order by reservationNum;