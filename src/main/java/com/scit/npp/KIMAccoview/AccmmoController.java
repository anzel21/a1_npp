package com.scit.npp.KIMAccoview;

import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_others.NaverLoginBO;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.SocialMember;
import com.scit.npp.wontae.dao.PictureDAO;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.search.SearchController;
import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.RoomInfo;

@Controller
public class AccmmoController {

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	RoomInfoDAO roomInfoDAO;
	@Autowired
	MemberDAO dao;
	@Autowired
	PictureDAO pictureDAO;
	
	@Autowired
	ServletContext servletContext;
	
	
	ArrayList<ArrayList<NormalPicture>> mainpiclist = new ArrayList<ArrayList<NormalPicture>>();

	/* NaverLoginBO */
	private NaverLoginBO naverLoginBO;
	private String apiResult = null;

	@Autowired
	private void setNaverLoginBO(NaverLoginBO naverLoginBO) {
		this.naverLoginBO = naverLoginBO;
	}

	// 숙소 리스트 View로 넘어가는 메서드
	@RequestMapping(value = "accmmolist", method = RequestMethod.GET)
	public String Accmmolist(String inputAddress, Locale locale, HttpSession session, Model model) {
		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);
		
		
		String path = servletContext.getRealPath("/resources");
		
		logger.debug("사진 저장 경로 : " + path);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);

		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");

		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		session.removeAttribute("PicturePath");
		
		String location = inputAddress;
		// 검색한 주소가 없을 경우
		if (location == null || location.equals("")) {
			logger.debug("검색한 주소 : {}", inputAddress);
			ArrayList<RoomInfo> Accolist = roomInfoDAO.getAccolist();
			
			
			for (RoomInfo roomInfo : Accolist) {
				ArrayList<NormalPicture> temp = pictureDAO.getPictureList(roomInfo.getRoomID());
				mainpiclist.add(temp);
			}
			
			ArrayList<String> picpath = new ArrayList<>();
			
			for(int i=0; i<Accolist.size(); i++)
			{
				//logger.debug("사진경로?? : " + mainpiclist.get(i).get(0).getPicturePath());
				picpath.add(mainpiclist.get(i).get(0).getPicturePath());
			}
			
			//logger.debug("보낼경로 : " + picpath);
			
			session.setAttribute("PicturePath", picpath);
			session.setAttribute("path", path);
			session.setAttribute("Accolist", Accolist);

			return "kyusoo/AccommoList/AccommoListView";

		}
		// 주소 검색을 통해 접속 한 경우
		else if (location != null) {
			logger.debug("검색한 주소 : {}", inputAddress);
			ArrayList<RoomInfo> Accolist = roomInfoDAO.getAddressAccolist(inputAddress);
			
			for (RoomInfo roomInfo : Accolist) {
				ArrayList<NormalPicture> temp = pictureDAO.getPictureList(roomInfo.getRoomID());
				mainpiclist.add(temp);
			}
			
			ArrayList<String> picpath = new ArrayList<>();
			
			for(int i=0; i<Accolist.size(); i++)
			{
				//logger.debug("사진경로?? : " + mainpiclist.get(i).get(0).getPicturePath());
				picpath.add(mainpiclist.get(i).get(0).getPicturePath());
			}
			
			logger.debug("" + picpath);
			
			session.setAttribute("PicturePath", picpath);
			session.setAttribute("Accolist", Accolist);

			return "kyusoo/AccommoList/AccommoListView";
		}
		return "kyusoo/AccommoList/AccommoListView";
	}

	// Ŭ���� ���� ���� �������� �̵�
	@RequestMapping(value = "accommoInfo", method = RequestMethod.GET)
	public String accommoInfo(Model model, HttpSession session) {
		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);

		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");

		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}
		return "wontae/accoView";
	}

}
