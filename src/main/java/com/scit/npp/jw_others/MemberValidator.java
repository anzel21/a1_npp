package com.scit.npp.jw_others;

import com.scit.npp.jw_vo.Member;

public class MemberValidator {

	public String errMsg;
	
	public String validate(Member m) {
		// ID 에러 체크 1
		if (m.getId() == null || m.getId().equals("")) {
			errMsg = "아이디를 입력해주세요.";
		}
		// ID 에러 체크 2
		if (m.getId() != null) {
			for (int i = 0; i < m.getId().length(); i++) {
				char ch = m.getId().charAt(i);
				if (!(ch >= '0') && ch <= '9' || ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'Z') {
					errMsg = "특수기호는 입력하실 수 없습니다.";
				}
			}
		}
		// ID 에러 체크 3
		if (m.getId().length() >= 20) {
			errMsg = "아이디는 15자 이내로 입력해주세요."; 
		}
		// password 에러 체크 1
		if (m.getPassword1() == null || m.getPassword1().equals("")) {
			errMsg = "비밀번호를 입력해주세요.";
		}
		// password 에러 체크 2
		if (m.getPassword2() == null || m.getPassword2().equals("")) {
			errMsg = "비밀번호를 입력해주세요. ";
		}
		// password 에러 체크 3
		if (!m.getPassword1().equals(m.getPassword2())) {
			errMsg = "비밀번호가 일치하지 않습니다.";
		}
		// password 에러 체크 4
		if (m.getPassword1().length() > 15 || m.getPassword1().length() < 5 ) {
			errMsg = "비밀번호는 5~15자리 이내로 입력해주세요.";
		}
		// password 에러 체크 5
		if (m.getPassword2().length() > 15 || m.getPassword2().length() <5) {
			errMsg = "비밀번호는 5~15자리 이내로 입력해주세요. ";
		}
		
		// name 에러 체크
		if (m.getName() == null || m.getName().equals("")) {
			errMsg = "이름을 입력해주세요.";
		}
		/*
		 * Test 끝나면 주석 풀고 적용하기
		// name 에러 체크2
		if (m.getName() != null || !m.getName().equals("")) {
			for (int i = 0; i < m.getName().length(); i++) {
				char ch = m.getName().charAt(i);
				if (((ch >= '0') && ch <= '9')) {
					errMsg = "잘못된 이름입니다.";
				}
			}
		}
		*/
		// countyryNum 에러 체크
		if (m.getCountryNum() == null || m.getCountryNum().equals("")) {
			errMsg = "국가번호를 선택해주세요.";
		}
		// phoneNumber 에러 체크 1
		if (m.getPhonenumber1() == null || m.getPhonenumber1().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (m.getPhonenumber1() != null || !m.getPhonenumber1().equals("")) {
			for (int i = 0; i < m.getPhonenumber1().length(); i++) {
				char ch = m.getPhonenumber1().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// phoneNumber 에러 체크 2
		if (m.getPhonenumber2() == null || m.getPhonenumber2().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (m.getPhonenumber2() != null || !m.getPhonenumber2().equals("")) {
			for (int i = 0; i < m.getPhonenumber2().length(); i++) {
				char ch = m.getPhonenumber2().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// phoneNumber 에러 체크 3
		if (m.getPhonenumber3() == null || m.getPhonenumber3().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (m.getPhonenumber3() != null || !m.getPhonenumber3().equals("")) {
			for (int i = 0; i < m.getPhonenumber3().length(); i++) {
				char ch = m.getPhonenumber3().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// 생년월일 에러 체크1
		if (m.getBirthdateCheck() == null || m.getBirthdateCheck().equals("")) {
			errMsg = "생년월일을 입력해주세요.";
		}
		// 생년월일 에러 체크2
		if (m.getBirthdateCheck() != null || !m.getBirthdateCheck().equals("")) {
			for (int i = 0; i < m.getBirthdateCheck().length(); i++) {
				char ch = m.getBirthdateCheck().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "생년월일은 숫자로만 입력해주세요.";
				}
			}
		}
		// 생년월일 에러 체크 3
		if (m.getBirthdateCheck() != null || !m.getBirthdateCheck().equals("")) {
			if (m.getBirthdateCheck().length() != 8) {
				errMsg = "생년월일 8자리를 입력해주세요.";
			}
		}
		// 생년월일 에러 체크 4
		
		return errMsg;
	}
}
