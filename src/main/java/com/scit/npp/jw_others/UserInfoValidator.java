package com.scit.npp.jw_others;

import com.scit.npp.jw_vo.userInfoVO;

public class UserInfoValidator {
public String errMsg;
	
	public String validate(userInfoVO uivo) {
		
		// countyryNum 에러 체크
		if (uivo.getCountryNum() == null || uivo.getCountryNum().equals("")) {
			errMsg = "국가번호를 선택해주세요.";
		}
		// phoneNumber 에러 체크 1
		if (uivo.getPhonenumber1() == null || uivo.getPhonenumber1().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (uivo.getPhonenumber1() != null || !uivo.getPhonenumber1().equals("")) {
			for (int i = 0; i < uivo.getPhonenumber1().length(); i++) {
				char ch = uivo.getPhonenumber1().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// phoneNumber 에러 체크 2
		if (uivo.getPhonenumber2() == null || uivo.getPhonenumber2().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (uivo.getPhonenumber2() != null || !uivo.getPhonenumber2().equals("")) {
			for (int i = 0; i < uivo.getPhonenumber2().length(); i++) {
				char ch = uivo.getPhonenumber2().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// phoneNumber 에러 체크 3
		if (uivo.getPhonenumber3() == null || uivo.getPhonenumber3().equals("")) {
			errMsg = "전화번호를 입력해주세요.";
		}
		if (uivo.getPhonenumber3() != null || !uivo.getPhonenumber3().equals("")) {
			for (int i = 0; i < uivo.getPhonenumber3().length(); i++) {
				char ch = uivo.getPhonenumber3().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "전화번호는 숫자로만 입력해주세요.";
				}
			}
		}
		// 생년월일 에러 체크1
		if (uivo.getBirthdateCheck() == null || uivo.getBirthdateCheck().equals("")) {
			errMsg = "생년월일을 입력해주세요.";
		}
		// 생년월일 에러 체크2
		if (uivo.getBirthdateCheck() != null || !uivo.getBirthdateCheck().equals("")) {
			for (int i = 0; i < uivo.getBirthdateCheck().length(); i++) {
				char ch = uivo.getBirthdateCheck().charAt(i);
				if (!((ch >= '0') && ch <= '9')) {
					errMsg = "생년월일은 숫자로만 입력해주세요.";
				}
			}
		}
		// 생년월일 에러 체크 3
		if (uivo.getBirthdateCheck() != null || !uivo.getBirthdateCheck().equals("")) {
			if (uivo.getBirthdateCheck().length() != 8) {
				errMsg = "생년월일 8자리를 입력해주세요.";
			}
		}
		// 생년월일 에러 체크 4
		
		return errMsg;
	}
}
