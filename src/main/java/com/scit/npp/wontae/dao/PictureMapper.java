package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.PanoPicture;
import com.scit.npp.wontae.vo.VRPicture;

public interface PictureMapper {
	
	public void insertPicture(NormalPicture normalPicture);
	
	public String getSavedName(NormalPicture normalPicture);
	
	public void deletePic(NormalPicture normalPicture);
	
	public ArrayList<NormalPicture> getPicNum(int roomID);
	
	public void insertpanopic(NormalPicture normalPicture);
	
	public String getSavedPanoName(NormalPicture normalPicture);
	
	public void deletePanoPic(NormalPicture normalPicture);
	
	public ArrayList<Integer> getPanoPicNum(int roomID);
	
	public void insertvrpic(NormalPicture normalPicture);
	
	public String getSavedVrName(NormalPicture normalPicture);
	
	public void deleteVrPic(NormalPicture normalPicture);
	
	public ArrayList<Integer> getVrNum(int roomID);

	public ArrayList<NormalPicture> getPictureList(int roomID);
	
	//파노라마 사진 리스트
	public ArrayList<PanoPicture> getPanoList(int roomID);
	
	//VR사진 리스트
	public ArrayList<VRPicture> getVRList(int roomID);
	
	// 메인사진으로 가져올 사진의 경로
	public NormalPicture getMainPicture(int roomID);

}
