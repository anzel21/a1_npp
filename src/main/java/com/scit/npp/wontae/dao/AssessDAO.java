package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.wontae.vo.Assessment;

@Repository
public class AssessDAO {
	
	@Autowired
	SqlSession sqlSession;
	
	public void insertAssess(Assessment assessment) {
		AssessMapper mapper = 
				sqlSession.getMapper(AssessMapper.class);
		
		mapper.insertAssess(assessment);
	}
	
	public Assessment getAvg(int roomID) {
		AssessMapper mapper = 
				sqlSession.getMapper(AssessMapper.class);
		
		Assessment assessment = mapper.getAvg(roomID);
		
		return assessment;
	}
	
	public ArrayList<Assessment> getAssess(int roomID) {
		AssessMapper mapper = 
				sqlSession.getMapper(AssessMapper.class);
		
		ArrayList<Assessment> list = mapper.getAssess(roomID);
		
		return list;
	}
	
	public Assessment getAssessDetail(int assessmentNum) {
		AssessMapper mapper = 
				sqlSession.getMapper(AssessMapper.class);
		
		Assessment assess = mapper.getAssessDetail(assessmentNum);
		
		return assess;
	}

}
