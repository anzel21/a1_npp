package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.PanoPicture;
import com.scit.npp.wontae.vo.VRPicture;

@Repository
public class PictureDAO {
	
	@Autowired
	SqlSession sqlSession;
	
	public void insertPicture(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.insertPicture(normalPicture);
	}
	
	public String getSavedName(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		String savedName = mapper.getSavedName(normalPicture);
		
		return savedName;
	}
	
	public void deletePic(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.deletePic(normalPicture);
	}
	
	public ArrayList<NormalPicture> getPicNum(int roomID) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		ArrayList<NormalPicture> picNum = mapper.getPicNum(roomID);
		
		return picNum;
	}
	
	public void insertpanopic(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.insertpanopic(normalPicture);
	}
	
	public String getSavedPanoName(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		String savedPanoName = mapper.getSavedPanoName(normalPicture);
		
		return savedPanoName;
	}
	
	public void deletePanoPic(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.deletePanoPic(normalPicture);
	}
	
	public ArrayList<Integer> getPanoPicNum(int roomID) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		ArrayList<Integer> panoPicNum = mapper.getPanoPicNum(roomID);
		
		return panoPicNum;
	}
	
	public void insertvrpic(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.insertvrpic(normalPicture);
	}
	
	public String getSavedVrName(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		String savedVRName = mapper.getSavedVrName(normalPicture);
		
		return savedVRName;
	}
	
	public void deleteVrPic(NormalPicture normalPicture) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		mapper.deleteVrPic(normalPicture);
	}
	
	public ArrayList<Integer> getVrNum(int roomID) {
		PictureMapper mapper = 
				sqlSession.getMapper(PictureMapper.class);
		
		ArrayList<Integer> vrPicNum = mapper.getVrNum(roomID);
		
		return vrPicNum;
	}
	
	// 해당 숙소글 사진 리스트 가져오기
	public ArrayList<NormalPicture> getPictureList(int roomID) {
		PictureMapper mapper = sqlSession.getMapper(PictureMapper.class);

		ArrayList<NormalPicture> piclist = mapper.getPictureList(roomID);

		return piclist;
	}
	
	// 해당 숙소글 파노라마 리스트 가져오기
	public ArrayList<PanoPicture> getPanoList(int roomID) {
		PictureMapper mapper = sqlSession.getMapper(PictureMapper.class);

		ArrayList<PanoPicture> piclist = mapper.getPanoList(roomID);

		return piclist;
	}
	
	// 해당 숙소글 파노라마 리스트 가져오기
	public ArrayList<VRPicture> getVRList(int roomID) {
		PictureMapper mapper = sqlSession.getMapper(PictureMapper.class);

		ArrayList<VRPicture> piclist = mapper.getVRList(roomID);

		return piclist;
	}
	
	// 메인사진으로 가져올 사진의 경로
	public NormalPicture getMainPicture(int roomID) {

		PictureMapper mapper = sqlSession.getMapper(PictureMapper.class);

		NormalPicture picpath = mapper.getMainPicture(roomID);

		return picpath;
	}	
	
}
