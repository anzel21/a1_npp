package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import com.scit.npp.wontae.vo.RoomInfo;

public interface RoomInfoMapper {
	
	public void insertAcco(RoomInfo roomInfo);
	
	public RoomInfo getAcco(int roomID);
	
	public int getAccoNum(String hostID);
	
	// 방 리스트를 가져오는 메서드
	public ArrayList<RoomInfo> getAccolist();

	// 등록되어진 방 중 주소가 포함된 방의 리스트를 가져오는 메소드
	public ArrayList<RoomInfo> getAddressAccolist(String location);
	
	// 본인이 등록한 숙소가 있는지 확인
	public RoomInfo accoRegistState(String loginId);
}
