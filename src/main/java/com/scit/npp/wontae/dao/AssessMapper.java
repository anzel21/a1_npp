package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import com.scit.npp.wontae.vo.Assessment;

public interface AssessMapper {
	
	public void insertAssess(Assessment assessment);
	
	public Assessment getAvg(int roomID);
	
	public ArrayList<Assessment> getAssess(int roomID);
	
	public Assessment getAssessDetail(int assessmentNum);

}
