package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import com.scit.npp.wontae.vo.AccoReply;

public interface ReplyMapper {
	
	public void insertReply(AccoReply accoReply);
	
	public ArrayList<AccoReply> getReply(int roomID);
	
	public void deleteReply(int replynum);

}
