package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.wontae.vo.AccoReply;

@Repository
public class ReplyDAO {
	
	@Autowired
	SqlSession sqlSession;
	
	public void insertReply(AccoReply accoReply) {
		ReplyMapper mapper = 
				sqlSession.getMapper(ReplyMapper.class);
		
		mapper.insertReply(accoReply);
	}
	
	public ArrayList<AccoReply> getReply(int roomID) {
		ReplyMapper mapper = 
				sqlSession.getMapper(ReplyMapper.class);
		
		ArrayList<AccoReply> reply = mapper.getReply(roomID);
		
		return reply;
	}
	
	public void deleteReply(int replynum) {
		ReplyMapper mapper = 
				sqlSession.getMapper(ReplyMapper.class);
		
		mapper.deleteReply(replynum);
	}

}
