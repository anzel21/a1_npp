package com.scit.npp.wontae.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.wontae.vo.RoomInfo;

@Repository
public class RoomInfoDAO {
	
	@Autowired
	SqlSession sqlSession;
	
	public void insertAcco(RoomInfo roomInfo) {
		RoomInfoMapper mapper = 
				sqlSession.getMapper(RoomInfoMapper.class);
		
		mapper.insertAcco(roomInfo);
	}
	
	public RoomInfo getAcco(int roomID) {
		RoomInfoMapper mapper = 
				sqlSession.getMapper(RoomInfoMapper.class);
		
		RoomInfo roomInfo = mapper.getAcco(roomID);
		
		return roomInfo;
	}
	
	public int getAccoNum(String hostID) {
		RoomInfoMapper mapper = 
				sqlSession.getMapper(RoomInfoMapper.class);
		
		int roomID = mapper.getAccoNum(hostID);
		
		return roomID;
	}
	
	// 방 리스트를 가져오는 메서드
	public ArrayList<RoomInfo> getAccolist() {
		RoomInfoMapper mapper = sqlSession.getMapper(RoomInfoMapper.class);

		ArrayList<RoomInfo> Accolist = mapper.getAccolist();

		return Accolist;
	}

	// 등록되어진 방 중 주소가 포함된 방의 리스트를 가져오는 메소드
	public ArrayList<RoomInfo> getAddressAccolist(String location) {
		RoomInfoMapper mapper = sqlSession.getMapper(RoomInfoMapper.class);

		ArrayList<RoomInfo> Accolist = mapper.getAddressAccolist(location);

		return Accolist;
	}
	
	// 본인이 등록한 숙소가 있는지 확인
	public RoomInfo accoRegistState(String loginId){
		RoomInfo ri = new RoomInfo();
		RoomInfoMapper mapper = sqlSession.getMapper(RoomInfoMapper.class);
		ri = mapper.accoRegistState(loginId);
		return ri;
	}
		
}
