package com.scit.npp.wontae.evaluation;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scit.npp.wontae.dao.AssessDAO;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.vo.Assessment;

@Controller
@RequestMapping("evaluation")
public class EvaluationController {
	
	@Autowired
	AssessDAO assessDAO;
	@Autowired
	RoomInfoDAO roomInfoDAO;
	
	@RequestMapping(value = "/starrank", method = RequestMethod.GET)
	public String accoType(int roomID, Model model) {
		
		model.addAttribute("roomID", roomID);
		
		return "wontae/starrank";
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/submitReview", method = RequestMethod.POST)
	public void submitReview(int roomID, int convenience, String convereason,
							 int accessibility, String accessreason, int kindness,
							 String kindreason, int sanitation, String sanireason,
							 int surroundings, String surroundreason, int total) {
		
		Assessment assessment = new Assessment();
		
		assessment.setRoomID(roomID);
		assessment.setConvenience(convenience);
		assessment.setConvereason(convereason);
		assessment.setAccessibility(accessibility);
		assessment.setAccessreason(accessreason);
		assessment.setKindness(kindness);
		assessment.setKindreason(kindreason);
		assessment.setSanitation(sanitation);
		assessment.setSanireason(sanireason);
		assessment.setSurroundings(surroundings);
		assessment.setSurroundreason(surroundreason);
		assessment.setTotal(total);
		
		assessDAO.insertAssess(assessment);
	}
	
	@RequestMapping(value = "/assessTable", method = RequestMethod.GET)
	public String assessTable(HttpSession session, Model model) {
		String id = (String) session.getAttribute("loginId");
		
		int roomID = roomInfoDAO.getAccoNum(id);
		
		ArrayList<Assessment> list = assessDAO.getAssess(roomID);
		
		model.addAttribute("list", list);
		
		return "wontae/assessTable";
	}
	
	@RequestMapping(value = "/viewAssess", method = RequestMethod.GET)
	public String viewAssess(int assessNum, Model model) {
		
		Assessment assess = assessDAO.getAssessDetail(assessNum);
		
		model.addAttribute("assess", assess);
		
		return "wontae/viewAssess";
	}
	
}
