package com.scit.npp.wontae.registerAcco;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.SocialMember;
import com.scit.npp.wontae.dao.PictureDAO;
import com.scit.npp.wontae.dao.PictureMapper;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.RoomInfo;

@Controller
@RequestMapping("hostAcco")
@SessionAttributes("RoomInfo")
public class RegisterController implements ServletContextAware {

	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	RoomInfoDAO roomInfoDAO;
	@Autowired
	PictureDAO pictureDAO;
	@Autowired
	MemberDAO dao;
	
	@Autowired
	ServletContext servletContext;
	

	@RequestMapping(value = "/selectType", method = RequestMethod.GET)
	public String accoType(Model model, HttpSession session) {
		
		
		RoomInfo roomInfo = new RoomInfo();
		String id = (String) session.getAttribute("loginId");
		logger.debug("로그인한 아이디는 : " + id);
		roomInfo.setHostID(id);

		model.addAttribute("RoomInfo", roomInfo);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		return "wontae/host/typeOfAcco";

	}

	// ajax로 숙소 타입
	@ResponseBody
	@RequestMapping(value = "/setAccoType", method = RequestMethod.POST)
	public void setAccoType(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, String accoType) {

		logger.debug("선택한 값 : " + accoType);

		roomInfo.setAccoType(accoType);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/bedrooms", method = RequestMethod.GET)
	public String bedrooms(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/bedrooms";

	}

	@ResponseBody
	@RequestMapping(value = "/setbedroom", method = RequestMethod.POST)
	public void setBedroom(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, int bedroom) {

		logger.debug("선택한 침실 : " + bedroom);

		roomInfo.setBedroom(bedroom);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@ResponseBody
	@RequestMapping(value = "/setaccommodation", method = RequestMethod.POST)
	public void setAccommodation(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, int accommodation) {

		logger.debug("선택한 최대인원 : " + accommodation);

		roomInfo.setAccommodation(accommodation);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/bathroom", method = RequestMethod.GET)
	public String bathrooms(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/bathroom";

	}

	@ResponseBody
	@RequestMapping(value = "/setBathroom", method = RequestMethod.POST)
	public void setBathroom(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, int bathroom) {

		logger.debug("선택한 화장실 : " + bathroom);

		roomInfo.setBathroom(bathroom);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@ResponseBody
	@RequestMapping(value = "/setBathtype", method = RequestMethod.POST)
	public void setBathtype(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, int bathtype) {

		logger.debug("선택한 타입 : " + bathtype);

		roomInfo.setBathroomType(bathtype);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/location", method = RequestMethod.GET)
	public String location(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/setloc";

	}

	@ResponseBody
	@RequestMapping(value = "/saveLoc", method = RequestMethod.POST)
	public void saveLoc(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, double latitude, double longitude,
			String address) {

		logger.debug("선택한 위도 : " + latitude);
		logger.debug("선택한 경도 : " + longitude);
		logger.debug("선택한 주소 : " + address);

		roomInfo.setLatitude(latitude);
		roomInfo.setLongitude(longitude);
		roomInfo.setLocation(address);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/ammenities", method = RequestMethod.GET)
	public String ammenities(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/ammenities";

	}

	@ResponseBody
	@RequestMapping(value = "/insertAmme", method = RequestMethod.POST)
	public void insertAmme(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, String ammenity) {

		logger.debug(ammenity);

		roomInfo.setAmnenities(ammenity);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/description", method = RequestMethod.GET)
	public String description(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/description";

	}

	@ResponseBody
	@RequestMapping(value = "/insertDes", method = RequestMethod.POST)
	public void insertDes(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, String acconame,
			String description) {

		logger.debug(acconame + "," + description);

		roomInfo.setRoomExplain(description);
		roomInfo.setRoomName(acconame);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/setPrice", method = RequestMethod.GET)
	public String setPrice(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model) {

		return "wontae/host/setPrice";

	}

	@ResponseBody
	@RequestMapping(value = "/insertPrice", method = RequestMethod.POST)
	public void insertPrice(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, int price) {

		logger.debug(price + "원");

		roomInfo.setPrice(price);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		model.addAttribute("RoomInfo", roomInfo);

	}

	@RequestMapping(value = "/complete", method = RequestMethod.GET)
	public String complete(@ModelAttribute("RoomInfo") RoomInfo roomInfo, Model model, SessionStatus sessionStatus) {

		roomInfoDAO.insertAcco(roomInfo);

		logger.debug("입력한 정보들 : " + roomInfo.toString());

		sessionStatus.setComplete();

		return "wontae/host/finish";

	}

	@RequestMapping(value = "/pictures", method = RequestMethod.GET)
	public String pictures(HttpSession session, Model model) {

		String hostID = (String) session.getAttribute("loginId");

		int roomID = roomInfoDAO.getAccoNum(hostID);
		ArrayList<NormalPicture> picNum = new ArrayList<NormalPicture>();
		picNum = pictureDAO.getPicNum(roomID);

		if (roomID == 0 || picNum.size() > 6) {

			return "home";
			// 여기는 개인정보페이지로 리턴하게할것

		} else {
			model.addAttribute("picNum", picNum);
			model.addAttribute("roomID", roomID);
			return "wontae/host/pictures";
		}

	}


	// 일반 사진 업로드
	@RequestMapping(value = "/fileUpload/post") // ajax에서 호출하는 부분
	@ResponseBody
	public String upload(MultipartHttpServletRequest multipartRequest, HttpSession session) { // Multipart로
																								// 받는다.

		Iterator<String> itr = multipartRequest.getFileNames();
		
		String filePath = servletContext.getRealPath("/resources/normalpicture");

		//String filePath = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\normalpicture"; // 설정파일로
																									// 뺀다.
																									// 파일이
																									// 저장되는
																									// 곳.
		
		
		File file = new File(filePath);

		if (!file.exists()) {
			file.mkdirs();
		}

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		logger.debug("숙소 번호 : " + roomID);

		while (itr.hasNext()) { // 받은 파일들을 모두 돌린다.

			/*
			 * 기존 주석처리 MultipartFile mpf = multipartRequest.getFile(itr.next());
			 * String originFileName = mpf.getOriginalFilename();
			 * System.out.println("FILE_INFO: "+originFileName); //받은 파일 리스트 출력'
			 */

			MultipartFile mpf = multipartRequest.getFile(itr.next());

			String originalFilename = mpf.getOriginalFilename(); // 파일명

			String ext;
			int lastIndex = originalFilename.lastIndexOf('.');

			// 확장자가 없는 경우
			if (lastIndex == -1) {
				ext = "";
			}
			// 확장자가 있는 경우
			else {
				ext = "." + originalFilename.substring(lastIndex + 1);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddSSsss");
			String savedFilename = sdf.format(new Date()) + ext;

			String fileFullPath = filePath + "/" + savedFilename; // 파일 전체 경로

			NormalPicture normalPicture = new NormalPicture();

			normalPicture.setOriginalName(originalFilename);
			normalPicture.setPicturePath(savedFilename);
			normalPicture.setRoomID(roomID);

			logger.debug(normalPicture.toString());

			pictureDAO.insertPicture(normalPicture);

			try {
				// 파일 저장
				mpf.transferTo(new File(fileFullPath)); // 파일저장 실제로는 service에서
														// 처리

				System.out.println("originalFilename => " + originalFilename);
				System.out.println("fileFullPath => " + fileFullPath);

			} catch (Exception e) {
				System.out.println("postTempFile_ERROR======>" + fileFullPath);
				e.printStackTrace();
			}

		}

		return "success";
	}

	@RequestMapping(value = "/fileDelete")
	@ResponseBody
	public void deleteFile(String fileName, HttpSession session) {

		logger.debug("삭제할 파일 원래 이름 : " + fileName);

		// 여기에 원래 이름으로 변경된 파일 이름 찾는 메소드
		NormalPicture normalPicture = new NormalPicture();

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		normalPicture.setRoomID(roomID);
		normalPicture.setOriginalName(fileName);

		String savedName = pictureDAO.getSavedName(normalPicture);
		
		String path = servletContext.getRealPath("/resources/normalpicture");

		//String path = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\normalpicture";

		String fullPath = path + savedName;

		File delFile = new File(fullPath);

		NormalPicture deletePic = new NormalPicture();

		deletePic.setRoomID(roomID);
		deletePic.setPicturePath(savedName);

		// 해당 파일이 존재하면 삭제
		if (delFile.isFile()) {
			delFile.delete();
			// 그리고 db에서 해당 그림파일 관련 데이터 지운다
			pictureDAO.deletePic(deletePic);
		}

	}

	@RequestMapping(value = "/panopic", method = RequestMethod.GET)
	public String panopic(HttpSession session, Model model) {

		String hostID = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (hostID != null) {
			ri = roomInfoDAO.accoRegistState(hostID);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (hostID != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", hostID, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(hostID);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(hostID);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(hostID);
				model.addAttribute("hosting", sm.getType());
			}
		}
		int roomID = roomInfoDAO.getAccoNum(hostID);
		ArrayList<Integer> picNum = new ArrayList<Integer>();
		picNum = pictureDAO.getPanoPicNum(roomID);

		if (roomID == 0 || picNum.size() > 3) {

			return "home";
			// 여기는 개인정보페이지로 리턴하게할것

		} else {
			model.addAttribute("picNum", picNum.size());
			model.addAttribute("roomID", roomID);
			return "wontae/host/panopic";

		}

	}

	@RequestMapping(value = "panofileUpload/post") // ajax에서 호출하는 부분
	@ResponseBody
	public String panoupload(MultipartHttpServletRequest multipartRequest, HttpSession session) { // Multipart로
																									// 받는다.

		Iterator<String> itr = multipartRequest.getFileNames();

		//String filePath = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\panopicture"; // 설정파일로
																									// 뺀다.
																									// 파일이
																									// 저장되는
																									// 곳.

		String filePath = servletContext.getRealPath("/resources/panopicture");
		
		
		File file = new File(filePath);

		if (!file.exists()) {
			file.mkdirs();
		}

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		logger.debug("숙소 번호 : " + roomID);

		while (itr.hasNext()) { // 받은 파일들을 모두 돌린다.

			/*
			 * 기존 주석처리 MultipartFile mpf = multipartRequest.getFile(itr.next());
			 * String originFileName = mpf.getOriginalFilename();
			 * System.out.println("FILE_INFO: "+originFileName); //받은 파일 리스트 출력'
			 */

			MultipartFile mpf = multipartRequest.getFile(itr.next());

			String originalFilename = mpf.getOriginalFilename(); // 파일명

			String ext;
			int lastIndex = originalFilename.lastIndexOf('.');

			// 확장자가 없는 경우
			if (lastIndex == -1) {
				ext = "";
			}
			// 확장자가 있는 경우
			else {
				ext = "." + originalFilename.substring(lastIndex + 1);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddSSsss");
			String savedFilename = sdf.format(new Date()) + ext;

			String fileFullPath = filePath + "/" + savedFilename; // 파일 전체 경로

			NormalPicture normalPicture = new NormalPicture();

			normalPicture.setOriginalName(originalFilename);
			normalPicture.setPicturePath(savedFilename);
			normalPicture.setRoomID(roomID);

			logger.debug(normalPicture.toString());

			pictureDAO.insertpanopic(normalPicture);

			try {
				// 파일 저장
				mpf.transferTo(new File(fileFullPath)); // 파일저장 실제로는 service에서
														// 처리

				System.out.println("originalFilename => " + originalFilename);
				System.out.println("fileFullPath => " + fileFullPath);

			} catch (Exception e) {
				System.out.println("postTempFile_ERROR======>" + fileFullPath);
				e.printStackTrace();
			}

		}

		return "success";
	}

	@RequestMapping(value = "/panofileDelete")
	@ResponseBody
	public void panodeleteFile(String fileName, HttpSession session) {

		logger.debug("삭제할 파일 원래 이름 : " + fileName);

		// 여기에 원래 이름으로 변경된 파일 이름 찾는 메소드
		NormalPicture normalPicture = new NormalPicture();

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		normalPicture.setRoomID(roomID);
		normalPicture.setOriginalName(fileName);

		String savedName = pictureDAO.getSavedPanoName(normalPicture);

		String path = servletContext.getRealPath("/resources/panopicture");
		
		//String path = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\panopicture";

		String fullPath = path + savedName;

		File delFile = new File(fullPath);

		NormalPicture deletePic = new NormalPicture();

		deletePic.setRoomID(roomID);
		deletePic.setPicturePath(savedName);

		// 해당 파일이 존재하면 삭제
		if (delFile.isFile()) {
			delFile.delete();
			// 그리고 db에서 해당 그림파일 관련 데이터 지운다
			pictureDAO.deletePanoPic(deletePic);
		}

	}

	@RequestMapping(value = "/vrpictures", method = RequestMethod.GET)
	public String vrpictures(HttpSession session, Model model) {

		String hostID = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (hostID != null) {
			ri = roomInfoDAO.accoRegistState(hostID);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (hostID != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", hostID, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(hostID);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(hostID);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(hostID);
				model.addAttribute("hosting", sm.getType());
			}
		}
		int roomID = roomInfoDAO.getAccoNum(hostID);
		ArrayList<Integer> picNum = new ArrayList<Integer>();
		picNum = pictureDAO.getVrNum(roomID);

		if (roomID == 0 || picNum.size() > 3) {

			return "home";
			// 여기는 개인정보페이지로 리턴하게할것

		} else {
			model.addAttribute("picNum", picNum.size());
			model.addAttribute("roomID", roomID);
			return "wontae/host/vrpicture";

		}

	}

	// 파노라마 사진 관련
	@RequestMapping(value = "/panopictures", method = RequestMethod.GET)
	public String panopictures(HttpSession session, Model model) {

		String hostID = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (hostID != null) {
			ri = roomInfoDAO.accoRegistState(hostID);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (hostID != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", hostID, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(hostID);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(hostID);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(hostID);
				model.addAttribute("hosting", sm.getType());
			}
		}
		int roomID = roomInfoDAO.getAccoNum(hostID);
		ArrayList<Integer> picNum = pictureDAO.getPanoPicNum(roomID);

		if (roomID == 0 || picNum.size() > 3) {

			return "home";
			// 여기는 개인정보페이지로 리턴하게할것

		} else {
			model.addAttribute("picNum", picNum.size());
			model.addAttribute("roomID", roomID);
			return "wontae/host/panopic";

		}

	}

	@RequestMapping(value = "vrfileUpload/post") // ajax에서 호출하는 부분
	@ResponseBody
	public String VRupload(MultipartHttpServletRequest multipartRequest, HttpSession session) { // Multipart로
																								// 받는다.

		Iterator<String> itr = multipartRequest.getFileNames();

		//String filePath = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\vrpicture"; // 설정파일로
																								// 뺀다.
																								// 파일이
																								// 저장되는
																								// 곳.

		
		String filePath = servletContext.getRealPath("/resources/vrpicture");
		
		File file = new File(filePath);

		if (!file.exists()) {
			file.mkdirs();
		}

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		logger.debug("숙소 번호 : " + roomID);

		while (itr.hasNext()) { // 받은 파일들을 모두 돌린다.

			/*
			 * 기존 주석처리 MultipartFile mpf = multipartRequest.getFile(itr.next());
			 * String originFileName = mpf.getOriginalFilename();
			 * System.out.println("FILE_INFO: "+originFileName); //받은 파일 리스트 출력'
			 */

			MultipartFile mpf = multipartRequest.getFile(itr.next());

			String originalFilename = mpf.getOriginalFilename(); // 파일명

			String ext;
			int lastIndex = originalFilename.lastIndexOf('.');

			// 확장자가 없는 경우
			if (lastIndex == -1) {
				ext = "";
			}
			// 확장자가 있는 경우
			else {
				ext = "." + originalFilename.substring(lastIndex + 1);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddSSsss");
			String savedFilename = sdf.format(new Date()) + ext;

			String fileFullPath = filePath + "/" + savedFilename; // 파일 전체 경로

			NormalPicture normalPicture = new NormalPicture();

			normalPicture.setOriginalName(originalFilename);
			normalPicture.setPicturePath(savedFilename);
			normalPicture.setRoomID(roomID);

			logger.debug(normalPicture.toString());

			pictureDAO.insertvrpic(normalPicture);

			try {
				// 파일 저장
				mpf.transferTo(new File(fileFullPath)); // 파일저장 실제로는 service에서
														// 처리

				System.out.println("originalFilename => " + originalFilename);
				System.out.println("fileFullPath => " + fileFullPath);

			} catch (Exception e) {
				System.out.println("postTempFile_ERROR======>" + fileFullPath);
				e.printStackTrace();
			}

		}

		return "success";
	}

	@RequestMapping(value = "/vrfileDelete")
	@ResponseBody
	public void vrdeleteFile(String fileName, HttpSession session) {

		logger.debug("삭제할 파일 원래 이름 : " + fileName);

		// 여기에 원래 이름으로 변경된 파일 이름 찾는 메소드
		NormalPicture normalPicture = new NormalPicture();

		String hostID = (String) session.getAttribute("loginId");
		int roomID = roomInfoDAO.getAccoNum(hostID);

		normalPicture.setRoomID(roomID);
		normalPicture.setOriginalName(fileName);

		String savedName = pictureDAO.getSavedVrName(normalPicture);

		//String path = "C:\\Users\\SCIT\\Documents\\a1_npp\\src\\main\\webapp\\resources\\vrpicture";
		
		String path = servletContext.getRealPath("/resources/vrpicture");

		String fullPath = path + savedName;

		File delFile = new File(fullPath);

		NormalPicture deletePic = new NormalPicture();

		deletePic.setRoomID(roomID);
		deletePic.setPicturePath(savedName);

		// 해당 파일이 존재하면 삭제
		if (delFile.isFile()) {
			delFile.delete();
			// 그리고 db에서 해당 그림파일 관련 데이터 지운다
			pictureDAO.deleteVrPic(deletePic);
			System.out.println("파일 제거 성공");
		}

	}

	// 파일업로드 경로를 구하기 위한 ServletContext
	@Override
	public void setServletContext(ServletContext servletContext) {
		
		this.servletContext = servletContext;
		
		
	}

}
