package com.scit.npp.wontae.vo;

public class VRPicture {
	
	private int vrpicnum;
	private String picturePath;
	private String originalname;
	private int roomid;
	
	public VRPicture() {}

	public VRPicture(int vrpicnum, String picturePath, String originalname, int roomid) {
		super();
		this.vrpicnum = vrpicnum;
		this.picturePath = picturePath;
		this.originalname = originalname;
		this.roomid = roomid;
	}

	public int getVrpicnum() {
		return vrpicnum;
	}

	public void setVrpicnum(int vrpicnum) {
		this.vrpicnum = vrpicnum;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getOriginalname() {
		return originalname;
	}

	public void setOriginalname(String originalname) {
		this.originalname = originalname;
	}

	public int getRoomid() {
		return roomid;
	}

	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}

	@Override
	public String toString() {
		return "VRPicture [vrpicnum=" + vrpicnum + ", picturePath=" + picturePath + ", originalname=" + originalname
				+ ", roomid=" + roomid + "]";
	}
	
		
}
