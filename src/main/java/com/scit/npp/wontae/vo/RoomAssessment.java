package com.scit.npp.wontae.vo;

public class RoomAssessment {
	
    private int assessnum;
	private int convenience;
	private String convereason;
	private int accessibility;
	private String accessreason;
	private int kindness;
	private String kindreason;
	private int sanitation;
	private String sanireason;
	private int surroundings;
	private String surroundreason;
	private String evaluation;
	
	public RoomAssessment() {
		super();
	}
	
	public int getAssessnum() {
		return assessnum;
	}
	
	public void setAssessnum(int assessnum) {
		this.assessnum = assessnum;
	}
	
	public int getConvenience() {
		return convenience;
	}
	
	public void setConvenience(int convenience) {
		this.convenience = convenience;
	}
	
	public String getConvereason() {
		return convereason;
	}
	
	public void setConvereason(String convereason) {
		this.convereason = convereason;
	}
	
	public int getAccessibility() {
		return accessibility;
	}
	
	public void setAccessibility(int accessibility) {
		this.accessibility = accessibility;
	}
	
	public String getAccessreason() {
		return accessreason;
	}
	
	public void setAccessreason(String accessreason) {
		this.accessreason = accessreason;
	}
	
	public int getKindness() {
		return kindness;
	}
	
	public void setKindness(int kindness) {
		this.kindness = kindness;
	}
	
	public String getKindreason() {
		return kindreason;
	}
	
	public void setKindreason(String kindreason) {
		this.kindreason = kindreason;
	}
	
	public int getSanitation() {
		return sanitation;
	}
	
	public void setSanitation(int sanitation) {
		this.sanitation = sanitation;
	}
	
	public String getSanireason() {
		return sanireason;
	}
	
	public void setSanireason(String sanireason) {
		this.sanireason = sanireason;
	}
	
	public int getSurroundings() {
		return surroundings;
	}
	
	public void setSurroundings(int surroundings) {
		this.surroundings = surroundings;
	}
	
	public String getSurroundreason() {
		return surroundreason;
	}
	
	public void setSurroundreason(String surroundreason) {
		this.surroundreason = surroundreason;
	}
	
	public String getEvaluation() {
		return evaluation;
	}
	
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	
}
