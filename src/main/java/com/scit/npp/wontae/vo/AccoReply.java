package com.scit.npp.wontae.vo;

public class AccoReply {
	
	private int replynum;
	private int roomID;
	private String guestID;
	private String replyContent;
	private String inputDate;
	
	public AccoReply() {
		super();
	}

	public AccoReply(int replynum, int roomID, String guestID, String replyContent, String inputDate) {
		super();
		this.replynum = replynum;
		this.roomID = roomID;
		this.guestID = guestID;
		this.replyContent = replyContent;
		this.inputDate = inputDate;
	}

	public int getReplynum() {
		return replynum;
	}

	public void setReplynum(int replynum) {
		this.replynum = replynum;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public String getGuestID() {
		return guestID;
	}

	public void setGuestID(String guestID) {
		this.guestID = guestID;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
}
