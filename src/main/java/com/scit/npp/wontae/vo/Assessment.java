package com.scit.npp.wontae.vo;

public class Assessment {
    
    private int assessmentnum;
	private int roomID;
	private int convenience;
	private String convereason;
	private int accessibility;
	private String accessreason;
	private int kindness;
	private String kindreason;
	private int sanitation;
	private String sanireason;
	private int surroundings;
	private String surroundreason;
	private int total;
	private String inputDate;
	
	public Assessment() {
		super();
	}

	public int getAssessmentnum() {
		return assessmentnum;
	}

	public void setAssessmentnum(int assessmentnum) {
		this.assessmentnum = assessmentnum;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public int getConvenience() {
		return convenience;
	}

	public void setConvenience(int convenience) {
		this.convenience = convenience;
	}

	public String getConvereason() {
		return convereason;
	}

	public void setConvereason(String convereason) {
		this.convereason = convereason;
	}

	public int getAccessibility() {
		return accessibility;
	}

	public void setAccessibility(int accessibility) {
		this.accessibility = accessibility;
	}

	public String getAccessreason() {
		return accessreason;
	}

	public void setAccessreason(String accessreason) {
		this.accessreason = accessreason;
	}

	public int getKindness() {
		return kindness;
	}

	public void setKindness(int kindness) {
		this.kindness = kindness;
	}

	public String getKindreason() {
		return kindreason;
	}

	public void setKindreason(String kindreason) {
		this.kindreason = kindreason;
	}

	public int getSanitation() {
		return sanitation;
	}

	public void setSanitation(int sanitation) {
		this.sanitation = sanitation;
	}

	public String getSanireason() {
		return sanireason;
	}

	public void setSanireason(String sanireason) {
		this.sanireason = sanireason;
	}

	public int getSurroundings() {
		return surroundings;
	}

	public void setSurroundings(int surroundings) {
		this.surroundings = surroundings;
	}

	public String getSurroundreason() {
		return surroundreason;
	}

	public void setSurroundreason(String surroundreason) {
		this.surroundreason = surroundreason;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
}
