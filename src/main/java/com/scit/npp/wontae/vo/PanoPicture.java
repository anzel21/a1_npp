package com.scit.npp.wontae.vo;

public class PanoPicture {
	
	private int panopicnum;
	private String picturePath;
	private String origlnalname;
	private int roomid;
	
	public PanoPicture() {}

	public PanoPicture(int panopicnum, String picturePath, String origlnalname, int roomid) {
		super();
		this.panopicnum = panopicnum;
		this.picturePath = picturePath;
		this.origlnalname = origlnalname;
		this.roomid = roomid;
	}

	public int getPanopicnum() {
		return panopicnum;
	}

	public void setPanopicnum(int panopicnum) {
		this.panopicnum = panopicnum;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getOriglnalname() {
		return origlnalname;
	}

	public void setOriglnalname(String origlnalname) {
		this.origlnalname = origlnalname;
	}

	public int getRoomid() {
		return roomid;
	}

	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}

	@Override
	public String toString() {
		return "PanoPicture [panopicnum=" + panopicnum + ", picturePath=" + picturePath + ", origlnalname="
				+ origlnalname + ", roomid=" + roomid + "]";
	}
	
	
	
}
