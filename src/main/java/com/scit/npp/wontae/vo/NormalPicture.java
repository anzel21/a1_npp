package com.scit.npp.wontae.vo;

public class NormalPicture {
	
	private int picturesNum;
	private String picturePath;
	private String originalName;
	private int roomID;
	
	public NormalPicture() {
		super();
	}

	public NormalPicture(int picturesNum, String picturePath, String originalName, int roomID) {
		super();
		this.picturesNum = picturesNum;
		this.picturePath = picturePath;
		this.originalName = originalName;
		this.roomID = roomID;
	}

	public int getPicturesNum() {
		return picturesNum;
	}

	public void setPicturesNum(int picturesNum) {
		this.picturesNum = picturesNum;
	}

	public String getPicturePath() {
		return picturePath;
	}

	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	@Override
	public String toString() {
		return "NormalPicture [사진번호=" + picturesNum + ", 저장된 이름=" + picturePath + ", 원래 이름="
				+ originalName + ", 숙소 번호=" + roomID + "]";
	}
	
}
