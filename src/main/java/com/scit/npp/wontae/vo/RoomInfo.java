package com.scit.npp.wontae.vo;

public class RoomInfo {
	
	private int roomID;
	private String hostID;
	private String location;
	private double latitude;
	private double longitude;
	private int bedroom;
	private int bathroom;
	private int bathroomType;
	private int price;
	private String roomExplain;
	private String amnenities;
	private int accommodation;
	private String accoType;
	private String roomName;
	
	public RoomInfo() {
		super();
	}

	public RoomInfo(int roomID, String hostID, String location, double latitude, double longitude, int bedroom,
			int bathroom, int bathroomType, int price, String roomExplain, String amnenities, int accommodation,
			String accoType, String roomName) {
		super();
		this.roomID = roomID;
		this.hostID = hostID;
		this.location = location;
		this.latitude = latitude;
		this.longitude = longitude;
		this.bedroom = bedroom;
		this.bathroom = bathroom;
		this.bathroomType = bathroomType;
		this.price = price;
		this.roomExplain = roomExplain;
		this.amnenities = amnenities;
		this.accommodation = accommodation;
		this.accoType = accoType;
		this.roomName = roomName;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public String getHostID() {
		return hostID;
	}

	public void setHostID(String hostID) {
		this.hostID = hostID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getBedroom() {
		return bedroom;
	}

	public void setBedroom(int bedroom) {
		this.bedroom = bedroom;
	}

	public int getBathroom() {
		return bathroom;
	}

	public void setBathroom(int bathroom) {
		this.bathroom = bathroom;
	}

	public int getBathroomType() {
		return bathroomType;
	}

	public void setBathroomType(int bathroomType) {
		this.bathroomType = bathroomType;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getRoomExplain() {
		return roomExplain;
	}

	public void setRoomExplain(String roomExplain) {
		this.roomExplain = roomExplain;
	}

	public String getAmnenities() {
		return amnenities;
	}

	public void setAmnenities(String amnenities) {
		this.amnenities = amnenities;
	}

	public int getAccommodation() {
		return accommodation;
	}

	public void setAccommodation(int accommodation) {
		this.accommodation = accommodation;
	}

	public String getAccoType() {
		return accoType;
	}

	public void setAccoType(String accoType) {
		this.accoType = accoType;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	@Override
	public String toString() {
		return "RoomInfo [roomID=" + roomID + ", hostID=" + hostID + ", location=" + location + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", bedroom=" + bedroom + ", bathroom=" + bathroom + ", bathroomType="
				+ bathroomType + ", price=" + price + ", roomExplain=" + roomExplain + ", amnenities=" + amnenities
				+ ", accommodation=" + accommodation + ", accoType=" + accoType + ", roomName=" + roomName + "]";
	}
	
	

}
