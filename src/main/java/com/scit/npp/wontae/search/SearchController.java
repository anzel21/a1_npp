package com.scit.npp.wontae.search;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_others.NaverLoginBO;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.SocialMember;
import com.scit.npp.wontae.dao.AssessDAO;
import com.scit.npp.wontae.dao.PictureDAO;
import com.scit.npp.wontae.dao.ReplyDAO;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.vo.AccoReply;
import com.scit.npp.wontae.vo.Assessment;
import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.PanoPicture;
import com.scit.npp.wontae.vo.RoomInfo;
import com.scit.npp.wontae.vo.VRPicture;

@Controller
@RequestMapping("accoResult")
public class SearchController {

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	RoomInfoDAO roomInfoDAO;
	@Autowired
	AssessDAO assessDAO;
	@Autowired
	ReplyDAO replyDAO;
	@Autowired
	PictureDAO pictureDAO;
	@Autowired
	MemberDAO dao;

	/* NaverLoginBO */
	private NaverLoginBO naverLoginBO;
	private String apiResult = null;

	@Autowired
	private void setNaverLoginBO(NaverLoginBO naverLoginBO) {
		this.naverLoginBO = naverLoginBO;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showResult(String s) {

		logger.debug("전달된 문자열 : " + s);

		// 마이바티스 이용하여 DB에 질의문

		return "wontae/result";
	}

	// /accoResult/accoView
	@RequestMapping(value = "/accoView", method = RequestMethod.GET)
	public String accoView(int accoNo, Model model, HttpSession session) {
		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);

		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");

		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		RoomInfo roomInfo = roomInfoDAO.getAcco(accoNo);
		Assessment assess = assessDAO.getAvg(accoNo);
		ArrayList<AccoReply> reply = replyDAO.getReply(accoNo);
		ArrayList<NormalPicture> piclist = pictureDAO.getPictureList(accoNo);

		model.addAttribute("RoomInfo", roomInfo);
		model.addAttribute("assess", assess);
		model.addAttribute("reply", reply);
		model.addAttribute("piclist", piclist);

		return "wontae/accoView";

	}

	@ResponseBody
	@RequestMapping(value = "/insertReply", method = RequestMethod.POST)
	public void insertReply(int roomID, String guestID, String replyContent) {

		AccoReply reply = new AccoReply();

		reply.setRoomID(roomID);
		reply.setGuestID(guestID);
		reply.setReplyContent(replyContent);

		replyDAO.insertReply(reply);

	}

	@ResponseBody
	@RequestMapping(value = "/deleteReply", method = RequestMethod.GET)
	public void deleteReply(int replynum, int roomID) {

		replyDAO.deleteReply(replynum);

	}

	// 파노라마 뷰 버튼 클릭
	@RequestMapping(value = "/panopic", method = RequestMethod.GET)
	public String panoView(int roomID, HttpSession session, Model model) {
		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		String path = session.getServletContext().getRealPath("/");

		ArrayList<PanoPicture> piclist = pictureDAO.getPanoList(roomID);

		session.setAttribute("piclist", piclist);
		session.setAttribute("filePath", path);

		return "wontae/panopic";

	}

	// VR사진 리스트
	@RequestMapping(value = "/vrpic", method = RequestMethod.GET)
	public String vrView(int roomID, HttpSession session, Model model) {
		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		String path = session.getServletContext().getRealPath("/");

		ArrayList<VRPicture> piclist = pictureDAO.getVRList(roomID);

		session.setAttribute("piclist", piclist);
		session.setAttribute("filePath", path);

		return "wontae/vrpic";

	}

}
