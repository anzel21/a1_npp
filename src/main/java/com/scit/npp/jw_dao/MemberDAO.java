package com.scit.npp.jw_dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.jw_mapper.MemberMapper;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.MemberExtraInfo;
import com.scit.npp.jw_vo.SocialMember;

@Repository
public class MemberDAO {

	@Autowired
	SqlSession sqlSession;

	// 회원가입
	public int joinMember(Member m) {
		int result = 0;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		result = mapper.joinMember(m);
		return result;
	}

	/*
	 * (id)기준, 전체 회원정보 조회 1.id 중복검사 / 2.로그인시 일치하는 아이디 유무 조회
	 */
	public Member selectAllMember(String id) {
		Member member = null;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		member = mapper.selectAllMember(id);
		return member;
	}

	// 소셜로그인시 로그인 정보 등록 (네이버)
	public int insertNaverMember(SocialMember sm) {
		int result = 0;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		result = mapper.insertNaverMember(sm);
		return result;
	}

	// 소셜로그인시 로그인 정보 등록 (카카오)
	public int insertKakaoMember(SocialMember sm) {
		int result = 0;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		result = mapper.insertKakaoMember(sm);
		return result;
	}

	// (emailId)기준, 전체 social회원정보 조회
	// 1. 새로운 접속자인지, 기존에 등록된 정보가 있는지 확인 (naver)
	public SocialMember selectAllNaverMember(String emailid) {
		SocialMember sm = null;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		sm = mapper.selectAllNaverMember(emailid);
		return sm;
	}

	// (emailId)기준, 전체 social회원정보 조회
	// 1. 새로운 접속자인지, 기존에 등록된 정보가 있는지 확인 (kakao)
	public SocialMember selectAllKakaoMember(String emailid) {
		SocialMember sm = null;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		sm = mapper.selectAllKakaoMember(emailid);
		return sm;
	}

	// 회원의 추가정보 입력
	public int insertExtraInfo(MemberExtraInfo extra) {
		int result = 0;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		result = mapper.insertExtraInfo(extra);
		return result;
	}

	// 회원의 추가정보 불러옴 (id)기준
	public MemberExtraInfo selectAllExtraInfo(String id) {
		MemberExtraInfo extra = null;
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		extra = mapper.selectAllExtraInfo(id);
		return extra;
	}

	// Web 회원정보 수정
	public void updateWebMember(Member m) {
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		mapper.updateWebMember(m);
	}

	// Social 회원정보 수정
	public void updateSocialMember(SocialMember sm) {
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		mapper.updateSocialMember(sm);
	}

	// 추가정보 수정
	public void updateExtraInfo(MemberExtraInfo exinfo) {
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		mapper.updateExtraInfo(exinfo);
	}

	// webMember 프로필사진 업데이트
	public void updateWebProfile(Member m) {
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		mapper.updateWebProfile(m);

	}

	// webMember 프로필사진 업데이트
	public void updateSocialProfile(SocialMember sm) {
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		mapper.updateSocialProfile(sm);
	}
}
