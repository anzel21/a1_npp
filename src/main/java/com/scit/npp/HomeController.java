package com.scit.npp;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.silk8192.jpushbullet.PushbulletClient;
import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_others.NaverLoginBO;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.SocialMember;
import com.scit.npp.reservation.dao.ReservationDAO;
import com.scit.npp.reservation.vo.ReservationAlertVO;
import com.scit.npp.wontae.dao.PictureDAO;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.search.SearchController;
import com.scit.npp.wontae.vo.NormalPicture;
import com.scit.npp.wontae.vo.RoomInfo;
import com.scit.npp.jw_others.kakaoLogin;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	MemberDAO dao;

	@Autowired
	RoomInfoDAO roomInfoDAO;

	@Autowired
	ReservationDAO reservationDAO;
	
	@Autowired
	PictureDAO pictureDAO;
	
	// 사진경로 arraylist
	ArrayList<ArrayList<NormalPicture>> mainpiclist = new ArrayList<ArrayList<NormalPicture>>();

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

	// 네이버 로그인 연동 기본 세팅값
	/* NaverLoginBO */
	private NaverLoginBO naverLoginBO;
	private String apiResult = null;

	@Autowired
	private void setNaverLoginBO(NaverLoginBO naverLoginBO) {
		this.naverLoginBO = naverLoginBO;
	}

	// 메인 화면 연동 메소드
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session, Model model) {
		logger.debug("메인화면 실행");
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		ArrayList<RoomInfo> Accolist = roomInfoDAO.getAccolist();
		
		
		// 사진 파일 경로 만들기
		for (RoomInfo roomInfo : Accolist) {
			ArrayList<NormalPicture> temp = pictureDAO.getPictureList(roomInfo.getRoomID());
			mainpiclist.add(temp);
		}
		
		ArrayList<String> picpath = new ArrayList<>();
		
		
		
		for(int i=0; i<Accolist.size(); i++)
		{
			//logger.debug("사진경로?? : " + mainpiclist.get(i).get(0).getPicturePath() );
			picpath.add(mainpiclist.get(i).get(0).getPicturePath());
		}
		
		logger.debug("" + picpath);
		
		session.setAttribute("PicturePath", picpath);
		
		
		session.setAttribute("Accolist", Accolist);
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		if (loginId != null) {
			ri = roomInfoDAO.accoRegistState(loginId);
			if (ri == null) {
				model.addAttribute("registRoomCheck", 0);
			} else {
				model.addAttribute("registRoomCheck", 1);
			}
		}

		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		// 네이버 로그인연동 기능 (주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		// 생성한 인증 URL을 Model에 담아서 View로 전달
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인 연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		// 가져온 인증 URL을 Model에 담아서 View로 전달
		model.addAttribute("kakaoUrl", kakaoUrl);

		int x = reservationDAO.checkDateforAlert();
		if (x != 0) {
			final String idNUM = "o.ZWDgNBywcKs9bqzIKcBwUVmtxoTmtFTi"; // 외부 API
																		// 액세스
																		// 코드!![수정
																		// 금지]!!
			PushbulletClient client = new PushbulletClient(idNUM);
			logger.info("예약자에 대한 3일 전 알림");
			ArrayList<ReservationAlertVO> confirmedList = reservationDAO.getListForReservationAlert();
			for (int i = 0; i < confirmedList.size(); i++) {
				System.out.println(confirmedList.get(i).toString());
				String fullMSG = "[NPP 예약알림] 3일 후 " + confirmedList.get(i).getStartDate() + " ~ "
						+ confirmedList.get(i).getEndDate() + "간 " + confirmedList.get(i).getRoomName()
						+ " 숙소 이용을 알려드립니다.";
				client.sendSMSPush(confirmedList.get(i).getPhoneNumber(), fullMSG, "com.pushbullet.android",
						"ujAhVu4jbmCsjAiVsKnSTs");
			}
		}

		return "home";
	}

	// 네이버 로그인 성공시 callback호출 메소드
	@RequestMapping(value = "/naverCallBack", method = { RequestMethod.GET, RequestMethod.POST })
	public String naverCallBack(Model model, @RequestParam String code, @RequestParam String state, HttpSession session)
			throws IOException {
		logger.debug("naverCallBack 메소드 실행");
		// 네이버 회원정보에서 가져올 데이터 변수들
		String naverApiResult = null;
		String naverId = null;
		String naverGender = null;
		String naverEmail = null;
		String uniNaverName = null;
		String naverName = null;
		String naverBirthday = null;

		// 네이버에서 회원정보를 가져오기 위한 기본 설정
		OAuth2AccessToken oauthToken;
		oauthToken = naverLoginBO.getAccessToken(session, code, state);

		// 로그인 사용자 정보를 읽어옴
		apiResult = naverLoginBO.getUserProfile(oauthToken);

		// 담아온 로그인 정보를 model에 담아서 다른 페이지로 넘겨줌
		model.addAttribute("naverLogin", apiResult);

		// 네이버에서 가져온 값을 변수에 담아줌
		naverApiResult = apiResult;

		// 변수에서 네이버ID 고유번호를 잘라서 가져옴
		naverId = naverApiResult.substring(naverApiResult.indexOf("id") + 5,
				naverApiResult.indexOf("profile_image") - 3);
		// 변수에서 성별을 잘라서 가져옴
		naverGender = naverApiResult.substring(naverApiResult.indexOf("gender") + 9,
				naverApiResult.indexOf("email") - 3);
		// 변수에서 네이버 이메일주소를 잘라서 가져옴
		naverEmail = naverApiResult.substring(naverApiResult.indexOf("email") + 8, naverApiResult.indexOf("name") - 3);
		// 변수에서 네이버 이름을 잘라서 가져옴 (유니코드 타입)
		uniNaverName = naverApiResult.substring(naverApiResult.indexOf("name") + 7,
				naverApiResult.indexOf("birthday") - 3);
		// 변수에서 네이버 생일을 잘라서 가져옴
		naverBirthday = naverApiResult.substring(naverApiResult.indexOf("birthday") + 11, naverApiResult.length() - 3);

		// 자른 uniNaverName 값 ( 유니코드 )를 문자로 변환해줌
		StringBuffer str = new StringBuffer();
		char ch = 0;
		for (int i = uniNaverName.indexOf("\\u"); i > -1; i = uniNaverName.indexOf("\\u")) {
			ch = (char) Integer.parseInt(uniNaverName.substring(i + 2, i + 6), 16);
			str.append(uniNaverName.substring(0, i));
			str.append(String.valueOf(ch));
			uniNaverName = uniNaverName.substring(i + 6);
		}
		str.append(uniNaverName);
		naverName = str.toString();

		// 생일 결과값에서 '-' 를 지움
		naverBirthday = naverBirthday.replaceAll("-", "");

		// 접속 후 emailID를 기준으로 VO를 가져옴
		SocialMember checkSm = new SocialMember();
		checkSm = dao.selectAllNaverMember(naverEmail);

		// 등록된 정보가 없을 경우, SocialMember VO에 로그인 후 얻어온 개인정보를 담아줌
		if (checkSm == null) {
			SocialMember newSm = new SocialMember();
			newSm.setIdentifier(naverId);
			newSm.setGender(naverGender);
			newSm.setEmailid(naverEmail);
			newSm.setName(naverName);
			newSm.setLogintype("naver");

			// VO의 내용을 DB에 Insert 시켜줌
			dao.insertNaverMember(newSm);
		} // 가져온 정보가 기존에 등록된 정보에 있는 정보인지 조회
		else if (checkSm != null && checkSm.getEmailid().equals(naverEmail)) {
			logger.debug("등록된 socialMember 접속 확인 / emailId : " + naverEmail);
			// 로그인 성공, session에 담은 값 (이메일주소)
			session.setAttribute("loginId", naverEmail);
			session.setAttribute("loginType", "naver");
			return "home";
		}

		return "home";
	}

	// 카카오 로그인 성공시 callback호출 메소드
	@RequestMapping(value = "/kakaoCallBack", produces = "application/json", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String kakaoLogin(@RequestParam("code") String code, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws Exception {
		logger.debug("kakaoCallBack 메소드 실행");
		// callBack 메소드에 가져올 로그인 후의 결과값을 담아줌
		JsonNode token = kakaoLogin.getAccessToken(code);
		// 카카오로 로그인 한 개인의 프로필 정보 (개인정보) 를 받아옴
		JsonNode profile = kakaoLogin.getKakaoUserInfo(token.path("access_token").toString());

		// VO 형식에 맞게 담아서 보여줌
		SocialMember sm = kakaoLogin.SocialData(profile);
		sm.setIdentifier(sm.getIdentifier());
		System.out.println(sm);

		// 접속 후 emailID를 기준으로 DB에서 VO를 가져옴
		SocialMember checkSm = new SocialMember();
		checkSm = dao.selectAllKakaoMember(sm.getEmailid());

		// 등록된 정보가 없을 경우, SocialMember VO에 로그인 후 얻어온 개인정보를 담아줌
		if (checkSm == null) {
			SocialMember newSm = new SocialMember();
			newSm.setEmailid(sm.getEmailid());
			newSm.setIdentifier(sm.getIdentifier());
			newSm.setName(sm.getName());
			newSm.setLogintype("kakao");

			// VO의 내용을 DB에 Insert 시켜줌
			dao.insertKakaoMember(newSm);
		} // 가져온 정보가 기존에 등록된 정보에 있는 정보인지 조회
		else if (checkSm != null && checkSm.getEmailid().equals(sm.getEmailid())) {
			logger.debug("등록된 socialMember 접속 확인 / emailId : " + sm.getEmailid());
			// 로그인 성공, session에 담은 값 (이메일주소)
			session.setAttribute("loginId", sm.getEmailid());
			session.setAttribute("loginType", "kakao");
			return "home";
		}
		return "home";
	}

}
