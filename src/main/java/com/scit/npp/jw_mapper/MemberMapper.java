package com.scit.npp.jw_mapper;

import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.MemberExtraInfo;
import com.scit.npp.jw_vo.SocialMember;

public interface MemberMapper {
	public int joinMember(Member m); // 회원가입
	public Member selectAllMember(String id); // (id)기준 전체 회원 조회 
											 // 1.id 중복검사  /  2.로그인시 일치하는 아이디 유무 조회 
	public int insertNaverMember(SocialMember sm); // 소셜로그인시 로그인 정보 등록 (네이버)
	public int insertKakaoMember(SocialMember sm); // 소셜로그인시 로그인 정보 등록(카카오)
	
	public SocialMember selectAllNaverMember(String emailid); // (emailId)기준, naver social회원정보 조회
	public SocialMember selectAllKakaoMember(String emailid); // (emailId)기준, kakao social회원 정보 조회
	
	
	public int insertExtraInfo(MemberExtraInfo extra); // 회원의 추가정보 입력
	public MemberExtraInfo selectAllExtraInfo(String id); // 회원의 추가정보 불러옴 (id)기준
	
	public void updateWebMember(Member m); // Web 회원정보 수정
	public void updateSocialMember(SocialMember sm); // Social 회원정보 수정
	public void updateExtraInfo(MemberExtraInfo exinfo); // 추가정보 수정
	
	public void updateWebProfile(Member m); // webMember 프로필사진 업데이트
	public void updateSocialProfile(SocialMember sm); // webMember 프로필사진 업데이트
}
