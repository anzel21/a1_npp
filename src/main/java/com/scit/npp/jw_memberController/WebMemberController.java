package com.scit.npp.jw_memberController;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_others.MemberValidator;
import com.scit.npp.jw_others.NaverLoginBO;
import com.scit.npp.jw_others.UserInfoValidator;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.jw_vo.MemberExtraInfo;
import com.scit.npp.jw_vo.SocialMember;
import com.scit.npp.jw_vo.userInfoVO;
import com.scit.npp.wontae.dao.RoomInfoDAO;
import com.scit.npp.wontae.vo.RoomInfo;

import npp.scit.npp.KIM.FileService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("member")
public class WebMemberController {
	@Autowired
	MemberDAO dao;
	@Autowired
	RoomInfoDAO roomInfoDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(WebMemberController.class);

	/* NaverLoginBO */
	private NaverLoginBO naverLoginBO;
	private String apiResult = null;

	@Autowired
	private void setNaverLoginBO(NaverLoginBO naverLoginBO) {
		this.naverLoginBO = naverLoginBO;
	}

	/* 회원가입 폼으로 이동 */
	@RequestMapping(value = "/joinForm", method = RequestMethod.GET)
	public String joinForm(Locale locale, Model model, HttpSession session) {
		logger.debug("회원가입 폼으로 이동");
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");

		if (loginId != null) {
			logger.debug("접속자 : {}", loginId);
		}

		// 네이버 로그인연동 기능(주소 가져옴)
		String naverAuthUrl = naverLoginBO.getAuthorizationUrl(session);
		model.addAttribute("naverUrl", naverAuthUrl);

		// 카카오 로그인연동 기능 (주소 가져옴)
		String kakaoUrl = "https://kauth.kakao.com/oauth/authorize?client_id=466daa0800d847383065bd413a4b8862&redirect_uri=http://localhost:8888/npp/kakaoCallBack&response_type=code";
		model.addAttribute("kakaoUrl", kakaoUrl);

		return "jw_login/joinForm";
	}

	/* 회원가입 폼에서 받아온 데이터 처리 */
	@ResponseBody
	@RequestMapping(value = "/joinData", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public String joinData(Member member, Model model, HttpSession session) {
		String errorMsg = null;
		String loginId = (String) session.getAttribute("loginId");

		if (loginId != null) {
		} else {
			// validator 호출, 입력한 정보의 에러 확인
			MemberValidator mv = new MemberValidator();
			errorMsg = mv.validate(member);

			// validator의 메세지값
			if (errorMsg != null) {
				return errorMsg;
			}

			// 아이디 중복검사
			Member idDuplicationCheck = dao.selectAllMember(member.getId());
			if (idDuplicationCheck != null) {
				errorMsg = "동일한 아이디가 존재합니다.";
				return errorMsg;
			}

			// validator에서 확인 끝난 나머지 데이터 VO에 저장
			String password = member.getPassword1();
			String phonenumber = member.getCountryNum() + "-" + member.getPhonenumber1() + "-"
					+ member.getPhonenumber2() + "-" + member.getPhonenumber3();
			int birthdate = Integer.parseInt(member.getBirthdateCheck());

			if (member.getTypeCheck() != null) {
				member.setType(1);
			} else {
				member.setType(0);
			}

			member.setPw(password);
			member.setPhonenumber(phonenumber);
			member.setBirthdate(birthdate);

			dao.joinMember(member);
		}
		return errorMsg;
	}

	// 로그인 폼에서 받아온 데이터 처리
	@ResponseBody
	@RequestMapping(value = "/loginData", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public String loginData(String id, String pw, Model model, HttpSession session) {
		logger.debug("로그인 폼에서 받아온 데이터 : {} || {}", id, pw);
		String errorMsg = null;
		// 아이디, 비밀번호 공백 체크
		if (id.equals("") || id == null) {
			errorMsg = "아이디를 입력해주세요.";
			return errorMsg;
		}
		if (pw.equals("") || pw == null) {
			errorMsg = "비밀번호를 입력해주세요.";
			return errorMsg;
		}

		// 아이디 기준, 전체 정보 조회
		Member member = dao.selectAllMember(id);

		// 아이디가 없을 경우
		if (member == null) {
			errorMsg = "없는 아이디 입니다.";
			return errorMsg;
		}
		// 비밀번호가 일치하지 않는 경우
		if (!member.getPw().equals(pw)) {
			errorMsg = "비밀번호가 일치하지 않습니다.";
			return errorMsg;
		}
		// 모든 조건에 부합하는 경우 & 세션에 담음
		if (member != null & member.getPw().equals(pw)) {
			session.setAttribute("loginId", id); // 로그인 성공, session에 담은 값
		}
		return errorMsg;
	}

	// 로그아웃처 처리
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		logger.debug("로그아웃 완료");
		session.invalidate();
		return "redirect:./../";
	}

	/* 회원정보 조회 폼으로 이동 */
	@RequestMapping(value = "/userInfoForm", method = RequestMethod.GET)
	public String userInfoForm(Model model, HttpSession session) {
		logger.debug("회원정보 조회 폼으로 이동 폼으로 이동");
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");
		// 본인이 등록한 숙소가 있는지 확인
		RoomInfo ri = new RoomInfo();
		ri = roomInfoDAO.accoRegistState(loginId);
		if (ri == null) {
			model.addAttribute("registRoomCheck", 0);
		} else {
			model.addAttribute("registRoomCheck", 1);
		}		
		
		// 호스팅을 신청했는지 확인
		if (loginId != null) {
			logger.debug("접속자 : {}, 로그인 타입 : {}", loginId, loginType);
			if (loginType == null || loginType.equals("")) {
				Member m = new Member();
				m = dao.selectAllMember(loginId);
				model.addAttribute("hosting", m.getType());
			} else if (loginType.equals("naver")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllNaverMember(loginId);
				model.addAttribute("hosting", sm.getType());
			} else if (loginType.equals("kakao")) {
				SocialMember sm = new SocialMember();
				sm = dao.selectAllKakaoMember(loginId);
				model.addAttribute("hosting", sm.getType());
			}
		}

		// 회원의 추가정보를 불러옴
		MemberExtraInfo extraVo = new MemberExtraInfo();
		extraVo = dao.selectAllExtraInfo(loginId);

		// 추가정보가 없으면 이름을 기준으로 빈 테이블 만듬
		if (extraVo == null) {
			MemberExtraInfo newExtraVo = new MemberExtraInfo();
			newExtraVo.setId(loginId);
			dao.insertExtraInfo(newExtraVo);
		}
		model.addAttribute("extraVo", extraVo);

		// 세션에서 불러온 loginID와 loginType을 통해 DB에 저장된 회원 정보를 불러옴
		// 1. webMember의 회원 정보를 불러옴 (webMember은 loginType이 null임)
		if (loginType == null) {
			Member webVo = new Member();
			webVo = dao.selectAllMember(loginId);
			model.addAttribute("webVo", webVo);

			// DB에 저장된 전화번호를 분류 (4개)
			HashMap<Integer, String> map = new HashMap<>();

			String phonenunmber = webVo.getPhonenumber();
			String[] phoneNumbers = phonenunmber.split("-");
			for (int i = 0; i < phoneNumbers.length; i++) {
				map.put(i, phoneNumbers[i]);
			}
			model.addAttribute("countryNum", map.get(0));
			model.addAttribute("phonenumber1", map.get(1));
			model.addAttribute("phonenumber2", map.get(2));
			model.addAttribute("phonenumber3", map.get(3));

			return "jw_login/userInfoForm";
		}
		// 2. SocialMember의 회원 정보를 불러옴 (loginType이 naver인 경우)
		else if (loginType.equals("naver")) {
			SocialMember socialVo = new SocialMember();
			socialVo = dao.selectAllNaverMember(loginId);
			model.addAttribute("socialVo", socialVo);

			// DB에 저장된 전화번호를 분류 (4개)
			HashMap<Integer, String> map = new HashMap<>();

			String phonenunmber = socialVo.getPhonenumber();
			// 등록된 전화번호가 있는 경우
			if (phonenunmber != null) {
				String[] phoneNumbers = phonenunmber.split("-");
				for (int i = 0; i < phoneNumbers.length; i++) {
					map.put(i, phoneNumbers[i]);
				}
				model.addAttribute("countryNum", map.get(0));
				model.addAttribute("phonenumber1", map.get(1));
				model.addAttribute("phonenumber2", map.get(2));
				model.addAttribute("phonenumber3", map.get(3));
			}
			// 등록된 전화번호가 없는 경우
			else {
				model.addAttribute("countryNum", "");
				model.addAttribute("phonenumber1", "");
				model.addAttribute("phonenumber2", "");
				model.addAttribute("phonenumber3", "");
			}
			return "jw_login/userInfoForm";
		}
		// 3. SocialMember의 회원 정보를 불러옴 (loginType이 kakao인 경우)
		else if (loginType.equals("kakao")) {
			SocialMember socialVo = new SocialMember();
			socialVo = dao.selectAllKakaoMember(loginId);
			model.addAttribute("socialVo", socialVo);

			HashMap<Integer, String> map = new HashMap<>();

			String phonenunmber = socialVo.getPhonenumber();
			// 등록된 전화번호가 있는 경우
			if (phonenunmber != null) {
				String[] phoneNumbers = phonenunmber.split("-");
				for (int i = 0; i < phoneNumbers.length; i++) {
					map.put(i, phoneNumbers[i]);
				}
				model.addAttribute("countryNum", map.get(0));
				model.addAttribute("phonenumber1", map.get(1));
				model.addAttribute("phonenumber2", map.get(2));
				model.addAttribute("phonenumber3", map.get(3));
			}
			// 등록된 전화번호가 없는 경우
			else {
				model.addAttribute("countryNum", "");
				model.addAttribute("phonenumber1", "");
				model.addAttribute("phonenumber2", "");
				model.addAttribute("phonenumber3", "");
			}

			return "jw_login/userInfoForm";
		}

		return "jw_login/userInfoForm";
	}

	// 회원정보 폼에서 받아온 Web 유저 데이터 처리
	@ResponseBody
	@RequestMapping(value = "/webInfoData", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public String webInfoData(userInfoVO userinfovo, Model model, HttpSession session) {
		logger.debug("Web 회원정보 수정 페이지");
		if (userinfovo.getChk_concern() == null) {
			userinfovo.setType("미정");
		}
		String errorMsg = null;

		// validator 호출, 입력한 정보의 에러 확인
		UserInfoValidator uivalidator = new UserInfoValidator();
		errorMsg = uivalidator.validate(userinfovo);

		// validator의 메세지값
		if (errorMsg != null) {
			return errorMsg;
		}

		Member member = new Member();

		// Member 정보 업데이트
		member.setId(userinfovo.getId());
		member.setPw(userinfovo.getPassword1());
		member.setBirthdate(Integer.parseInt(userinfovo.getBirthdateCheck()));

		String phonenumber = userinfovo.getCountryNum() + "-" + userinfovo.getPhonenumber1() + "-"
				+ userinfovo.getPhonenumber2() + "-" + userinfovo.getPhonenumber3();
		member.setPhonenumber(phonenumber);
		if (userinfovo.getType() == null) {
			member.setType(0);
		} else {
			member.setType(1);
		}
		member.setSelfintroduction(userinfovo.getSelfintroduction());
		dao.updateWebMember(member);

		// extraInfo의 정보 업데이트 해야함
		MemberExtraInfo exinfo = new MemberExtraInfo();
		exinfo.setId(userinfovo.getId());
		exinfo.setGender(userinfovo.getGender());
		exinfo.setJob(userinfovo.getJob());
		exinfo.setConcern(userinfovo.getChk_concern());
		dao.updateExtraInfo(exinfo);

		return errorMsg;
	}

	// 회원정보 폼에서 받아온 Social 유저 데이터 처리
	@ResponseBody
	@RequestMapping(value = "/socialInfoData", method = RequestMethod.POST, produces = "application/text; charset=utf8")
	public String socialInfoData(userInfoVO userinfovo, Model model, HttpSession session) {
		logger.debug("Social 회원정보 수정 페이지");
		if (userinfovo.getChk_concern() == null) {
			userinfovo.setType("미정");
		}
		String errorMsg = null;

		// validator 호출, 입력한 정보의 에러 확인
		UserInfoValidator uivalidator = new UserInfoValidator();
		errorMsg = uivalidator.validate(userinfovo);

		// validator의 메세지값
		if (errorMsg != null) {
			return errorMsg;
		}
		SocialMember sMember = new SocialMember();

		// Member 정보 업데이트
		sMember.setEmailid(userinfovo.getId());
		sMember.setBirthdate(userinfovo.getBirthdateCheck());

		String phonenumber = userinfovo.getCountryNum() + "-" + userinfovo.getPhonenumber1() + "-"
				+ userinfovo.getPhonenumber2() + "-" + userinfovo.getPhonenumber3();
		sMember.setPhonenumber(phonenumber);
		if (userinfovo.getType() == null) {
			sMember.setType(0);
		} else {
			sMember.setType(1);
		}
		sMember.setSelfintroduction(userinfovo.getSelfintroduction());
		dao.updateSocialMember(sMember);

		// extraInfo의 정보 업데이트 해야함
		MemberExtraInfo exinfo = new MemberExtraInfo();
		exinfo.setId(userinfovo.getId());
		exinfo.setGender(userinfovo.getGender());
		exinfo.setJob(userinfovo.getJob());
		exinfo.setConcern(userinfovo.getChk_concern());
		dao.updateExtraInfo(exinfo);
		return errorMsg;
	}

	// 사진 파일 다운로드
	@RequestMapping(value = "download", method = RequestMethod.GET)
	public String fileDownload(String loginId, String loginType, Model model, HttpServletResponse response) {
		logger.debug("사진 파일 다운로드 메소드 실행이동");
		// WebMember로 접속한 경우
		if (loginType == null || loginType.equals("")) {
			Member member = dao.selectAllMember(loginId);
			// 원래의 파일명
			String originalfile = new String(member.getOriginalfile());
			try {
				response.setHeader("Content-Disposition",
						" attachment;filename=" + URLEncoder.encode(originalfile, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 저장된 파일 경로
			String fullPath = filepath + "/" + member.getSavedfile();
			// 서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
			FileInputStream filein = null;
			ServletOutputStream fileout = null;
			try {
				filein = new FileInputStream(fullPath);
				fileout = response.getOutputStream();
				// Spring의 파일 관련 유틸
				FileCopyUtils.copy(filein, fileout);
				filein.close();
				fileout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
			// SocialMember 중 naver로 로그인 한 경우
		} else if (loginType.equals("naver")) {
			SocialMember smember = dao.selectAllNaverMember(loginId);
			// 원래의 파일명
			String originalfile = new String(smember.getOriginalfile());
			try {
				response.setHeader("Content-Disposition",
						" attachment;filename=" + URLEncoder.encode(originalfile, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 저장된 파일 경로
			String fullPath = filepath + "/" + smember.getSavedfile();
			// 서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
			FileInputStream filein = null;
			ServletOutputStream fileout = null;
			try {
				filein = new FileInputStream(fullPath);
				fileout = response.getOutputStream();
				// Spring의 파일 관련 유틸
				FileCopyUtils.copy(filein, fileout);
				filein.close();
				fileout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
			// SocialMember 중 kakao로 로그인 한 경우
		} else if (loginType.equals("kakao")) {
			SocialMember smember = dao.selectAllKakaoMember(loginId);
			// 원래의 파일명
			String originalfile = new String(smember.getOriginalfile());
			try {
				response.setHeader("Content-Disposition",
						" attachment;filename=" + URLEncoder.encode(originalfile, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 저장된 파일 경로
			String fullPath = filepath + "/" + smember.getSavedfile();
			// 서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
			FileInputStream filein = null;
			ServletOutputStream fileout = null;
			try {
				filein = new FileInputStream(fullPath);
				fileout = response.getOutputStream();

				// Spring의 파일 관련 유틸
				FileCopyUtils.copy(filein, fileout);

				filein.close();
				fileout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		return null;
	}

	/* 프로필 사전 수정 페이지로 이동 */
	@RequestMapping(value = "/registPicturePopUp", method = RequestMethod.GET)
	public String registPicturePopUp(HttpSession session) {
		logger.debug("프로필 사진 수정 페이지로 이동");
		// 로그인 한 아이디를 Session에서 불러옴
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");

		return "jw_login/profilePictures";
	}

	// 첨부 파일이 저장될 경로 -- c드라이브 npp 폴더
	public static final String filepath = "C:/npp/profile/images";

	// 등록폼에서 받은 데이터 처리
	@RequestMapping(value = "/pictureRegistData", method = RequestMethod.POST)
	public String fileRegistData(Model model, HttpSession session, MultipartFile upload) {
		String loginId = (String) session.getAttribute("loginId");
		String loginType = (String) session.getAttribute("loginType");

		Member member = new Member();
		SocialMember smember = new SocialMember();

		logger.debug("upload파일 ㅣ {}", upload);

		// webMember 로 로그인 한 경우
		if (loginType == null || loginType.equals("")) {
			String fullpath = filepath + "/" + member.getSavedfile();
			// 등록하기 전에 프로필 사진이 현재 null 이면 그래도 사진 업데이트
			if (member.getSavedfile() == null) {
				// upload 한 사진파일이 있을 경우
				if (!upload.isEmpty()) {
					String savedfile = FileService.saveFile(upload, filepath);
					member.setOriginalfile(upload.getOriginalFilename());
					member.setSavedfile(savedfile);
					member.setId(loginId);
					// 프로필 사진을 webMember 테이블에 업데이트
					dao.updateWebProfile(member);
				}
			}
			// 프로필 사진이 현재 null이 아니라면 현재의 사진을 지우고 새로운 사진 업데이트
			else if (member.getSavedfile() != null) {
				FileService.deleteFile(fullpath);

				String savedfile = FileService.saveFile(upload, filepath);
				member.setOriginalfile(upload.getOriginalFilename());
				member.setSavedfile(savedfile);
				member.setId(loginId);
				// 프로필 사진을 webMember 테이블에 업데이트
				dao.updateWebProfile(member);
			}
			return "jw_login/profilePictures";
		}

		// socialMember 로 로그인 한 경우
		else if (loginType.equals("naver") || loginType.equals("kakao")) {
			String fullpath = filepath + "/" + smember.getSavedfile();
			// 등록하기 전에 프로필 사진이 현재 null 이면 그래도 사진 업데이트
			if (smember.getSavedfile() == null) {
				// upload 한 사진파일이 있을 경우
				if (!upload.isEmpty()) {
					String savedfile = FileService.saveFile(upload, filepath);
					smember.setOriginalfile(upload.getOriginalFilename());
					smember.setSavedfile(savedfile);
					smember.setEmailid(loginId);
					// 프로필 사진을 SocialMember 테이블에 업데이트
					dao.updateSocialProfile(smember);
				}
			}
			// 프로필 사진이 현재 null이 아니라면 현재의 사진을 지우고 새로운 사진 업데이트
			else if (smember.getSavedfile() != null) {
				FileService.deleteFile(fullpath);

				String savedfile = FileService.saveFile(upload, filepath);
				smember.setOriginalfile(upload.getOriginalFilename());
				smember.setSavedfile(savedfile);
				smember.setEmailid(loginId);
				// 프로필 사진을 SocialMember 테이블에 업데이트
				dao.updateSocialProfile(smember);
			}
			return "jw_login/profilePictures";
		}

		return "jw_login/profilePictures";
	}

	// loginInterceptor 기능 추가
	@RequestMapping(value = "/loginError", method = RequestMethod.GET)
	public String loginError() {
		logger.debug("비정상 접근 확인");
		return "jw_login/errorForm";
	}
}