package com.scit.npp.kyusoochartDAO;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.scit.npp.kyusoochartData.CanvasjsChartData;
 
@Repository
public class CanvasjsChartDaoImpl implements CanvasjsChartDao {
 
	@Override
	public List<List<Map<Object, Object>>> getCanvasjsChartData() {
		return CanvasjsChartData.getCanvasjsDataList();
	}
 
}