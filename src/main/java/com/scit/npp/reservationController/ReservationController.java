package com.scit.npp.reservationController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.silk8192.jpushbullet.PushbulletClient;
import com.scit.npp.jw_dao.MemberDAO;
import com.scit.npp.jw_vo.Member;
import com.scit.npp.reservation.dao.ReservationDAO;
import com.scit.npp.reservation.vo.AdditionalIntelForSMS;
import com.scit.npp.reservation.vo.Calendar2VO;
import com.scit.npp.reservation.vo.CalendarVO;
import com.scit.npp.reservation.vo.ReservationVO;

@Controller
public class ReservationController {
	@Autowired
	ReservationDAO reservationDAO;

	private static final Logger logger = LoggerFactory.getLogger(ReservationController.class);
	// [!기능 테스트용 URL!] http://localhost:8888/npp/accoResult/accoView?accoNo=2

	/*
	 * 해당 내용은 HomeController에 이식함[20180428]
	 * 
	 * @RequestMapping(value = "/testRASMS", method = RequestMethod.GET) public
	 * void testRASMS(Locale locale, Model model) { int x =
	 * reservationDAO.checkDateforAlert(); if(x==0) return;
	 * 
	 * final String idNUM = "o.ZWDgNBywcKs9bqzIKcBwUVmtxoTmtFTi"; // 외부 API 액세스
	 * 코드!![수정 금지]!! PushbulletClient client = new PushbulletClient(idNUM);
	 * logger.info("예약자에 대한 3일 전 알림"); ArrayList<ReservationAlertVO>
	 * confirmedList = reservationDAO.getListForReservationAlert(); for(int i
	 * =0;i<confirmedList.size();i++){
	 * System.out.println(confirmedList.get(i).toString()); String fullMSG =
	 * "[NPP 예약알림] 3일 후 " + confirmedList.get(i).getStartDate() + " ~ " +
	 * confirmedList.get(i).getEndDate() + "간 " +
	 * confirmedList.get(i).getRoomName() + " 숙소 이용을 알려드립니다.";
	 * client.sendSMSPush(confirmedList.get(i).getPhoneNumber(), fullMSG,
	 * "com.pushbullet.android", "ujAhVu4jbmCsjAiVsKnSTs"); }
	 * 
	 * }
	 */

	
	@RequestMapping(value = "/RS_Change", method = RequestMethod.GET)
	public String NPP_RS_intergrated(Locale locale, Model model, HttpSession session) {
		logger.info("Mypage내 예약 변경 페이지");
		String currentID = (String) session.getAttribute("loginId"); // 현재 아이디
																		// 받아옴.
		if (currentID == null) {
			logger.info("로그인하지 않음", locale);
			return "home";
		}
		
		Member currentMember = new Member();
		currentMember.setId(currentID);
		Member memberData = new Member();
		memberData = reservationDAO.getMemberType(currentMember);
		session.setAttribute("HostorNot", memberData.getType());
		
		return "/kjw/NPP_RS_Change";
	}

	@RequestMapping(value = "/RS_confirmForHost", method = RequestMethod.GET)
	public String RS_confirmForHost(Locale locale, Model model, HttpSession session) {
		logger.info("Mypage내 Host용 예약현황 확인 페이지");
		
		String currentID = (String) session.getAttribute("loginId"); // 현재 아이디
																		// 세션에서
																		// 받아옴.
		if (currentID == null) {
			logger.info("로그인하지 않음", locale);
			return "home";
		}
			
		logger.info("접속자 ID:" + currentID);
		Member currentMember = new Member();
		currentMember.setId(currentID);
		Member memberData = new Member();
		memberData = reservationDAO.getMemberType(currentMember);
		
		if (memberData.getType() == 1) {
			session.setAttribute("HostorNot", memberData.getType());
			return "/kjw/NPP_RS_ConfirmforHost";
		} else {
			logger.info("Host 아님");
			return "home";
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/getreserveList", method = RequestMethod.GET)
	public ArrayList<CalendarVO> getreserveList(Locale locale, Model model, HttpSession session) {
		logger.info("JSON으로 리스트 읽어옴[1]", locale);
		String currentID = (String) session.getAttribute("loginId"); // 현재 아이디
		int HostorNot = (Integer) session.getAttribute("HostorNot"); 
		
		if(HostorNot!=1) return null;

		ArrayList<ReservationVO> list = new ArrayList<ReservationVO>();
		ArrayList<CalendarVO> modifiedList = new ArrayList<CalendarVO>();
		Member m = new Member();
		m.setId(currentID);
		list = reservationDAO.getReservation(m);
		
		if(list==null) return null;

		String roomNameforPrint = reservationDAO.getRoomName(currentID);
		System.out.println("방이름" + roomNameforPrint);
		session.setAttribute("roomNameforPrint", roomNameforPrint);
		for (int i = 0; i < list.size(); i++) {
			CalendarVO tmp = new CalendarVO(list.get(i).getReservationNum(), list.get(i).getSubscriberID(),
					list.get(i).getStartDate(), list.get(i).getEndDate());
			modifiedList.add(tmp);
		}
		return modifiedList;
	}

	@ResponseBody
	@RequestMapping(value = "/cancelReservation", method = RequestMethod.POST)
	public int cancelReservation(Locale locale, Model model, String x) {
		logger.info("[예약취소기능]전달받은 예약번호: " + x);
		ReservationVO canceledReservation = new ReservationVO();
		canceledReservation.setReservationNum(x);
		int res = reservationDAO.cancelReservation(canceledReservation);
		return res;
	}

	@ResponseBody // 현재 로그인한 ID의 모든 숙소 예약정보를 불러온다.[최종작업일 : 180411]
	@RequestMapping(value = "/getreserveListforGuest", method = RequestMethod.GET)
	public ArrayList<ReservationVO> getreserveListforGuest(Locale locale, Model model, HttpSession session) {
		logger.info("현재 로그인한 ID의 모든 숙소 예약정보를 불러옴");
		String currentID = (String) session.getAttribute("loginId");
		ArrayList<ReservationVO> list = new ArrayList<ReservationVO>();
		list = reservationDAO.getreserveListforGuest(currentID);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		return list;
	}

	@ResponseBody
	@RequestMapping(value = "/getreserveList2", method = RequestMethod.POST)
	public ArrayList<Calendar2VO> getreserveList2(Locale locale, Model model, String roomID) {
		logger.info("JSON으로 리스트 읽어옴[2]", locale);
		ArrayList<ReservationVO> list = new ArrayList<ReservationVO>();
		ArrayList<Calendar2VO> modifiedList = new ArrayList<Calendar2VO>();
		list = reservationDAO.getReservation(roomID);
		for (int i = 0; i < list.size(); i++) {
			Calendar2VO tmp = new Calendar2VO(list.get(i).getStartDate(), list.get(i).getEndDate(), false, "background",
					"#ff0000");
			System.out.println(tmp);
			modifiedList.add(tmp);
		}
		return modifiedList;
	}

	/*
	 * 미사용 기능??? 삭제 예정
	 * 
	 * @ResponseBody
	 * 
	 * @RequestMapping(value = "/changeReservation", method =
	 * RequestMethod.POST) public void changeReservation(Locale locale, Model
	 * model, String x, String y) { logger.info("[예약변경기능]전달받은 예약번호: " + x +
	 * "     해당 roomID :" + y); return; }
	 */

	@ResponseBody
	@RequestMapping(value = "/updateReservation", method = RequestMethod.POST)
	public int updateReservation(Locale locale, Model model, Calendar2VO newEvent, String reservationNum) {
		logger.info("[예약변경갱신기능]전달받은 예약번호: " + reservationNum);
		logger.info("[예약변경갱신기능] 시작일 및 종료일: " + newEvent.getStart() + " " + newEvent.getEnd());
		ReservationVO updatedReservation = new ReservationVO();
		updatedReservation.setStartDate(newEvent.getStart());
		updatedReservation.setEndDate(newEvent.getEnd());
		updatedReservation.setReservationNum(reservationNum);
		int res = reservationDAO.updateReservation(updatedReservation);

		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/createCalforChange", method = RequestMethod.POST)
	public ArrayList<Calendar2VO> createCalforChange(Locale locale, Model model, String selectedNum_outSide,
			String selectedRoomNum_outSide) {
		String x = selectedNum_outSide;
		String y = selectedRoomNum_outSide;
		logger.info("변경화면에서 예약선택시, 해당 숙소 예약만 읽어옴.", locale);
		System.out.println(x); // reservationNum
		System.out.println(y); // roomID
		ArrayList<ReservationVO> list = new ArrayList<ReservationVO>();
		ArrayList<Calendar2VO> modifiedList = new ArrayList<Calendar2VO>();
		list = reservationDAO.getReservation();

		Calendar2VO tmp2 = new Calendar2VO(); // 나의 예약

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getRoomID().equals(y) && list.get(i).getReservationNum().equals(x)) {
				tmp2 = new Calendar2VO(list.get(i).getStartDate(), list.get(i).getEndDate(), false, "background",
						"#00ff00");
				continue;
			}
			Calendar2VO tmp = new Calendar2VO(list.get(i).getStartDate(), list.get(i).getEndDate(), false, "background",
					"#ff0000");
			modifiedList.add(tmp);
		}
		modifiedList.add(tmp2);
		for (int i = 0; i < modifiedList.size(); i++) {
			System.out.println(i + "번째   -> " + modifiedList.get(i).toString());
		}
		return modifiedList;
	}

	@ResponseBody // JSP로부터 전달받은 예약정보를 DB에 기록한다.[최종작업일 : 180414]
	@RequestMapping(value = "/setReserve", method = RequestMethod.POST)
	public int setReserve(Locale locale, Model model, Calendar2VO newEvent, String roomID, HttpSession session) {

		if ((String) session.getAttribute("loginId") == null)
			return 0;
		String currentID = (String) session.getAttribute("loginId"); // 현재 아이디
																		// 세션에서
																		// 받아옴.(180414
																		// 추가)

		logger.info("선택된 기간: " + newEvent);
		logger.info("해당 방번호: " + roomID);

		Calendar c = Calendar.getInstance();
		// 예약번호 설정: 예약시점(단위:밀리초) + 숙소ID
		String reservationNum = "" + c.getTimeInMillis() + roomID;

		// JQuery API용 VO 객체를 DB 저장용 VO 객체로 변환하여 DAO에 전달
		// Calendar2VO ---> ReservationVO
		ReservationVO newReservation = new ReservationVO();
		newReservation.setStartDate(newEvent.getStart());
		newReservation.setEndDate(newEvent.getEnd());
		newReservation.setSubscriberID(currentID);
		newReservation.setRoomID(roomID);
		newReservation.setReservationNum(reservationNum);

		int x = reservationDAO.checkListByReservationNum(newReservation);
		int res = 0;
		if (x == 0) {
			res = reservationDAO.setReserve(newReservation);
			sendSMS(newReservation);
		}
		return res;
	}

	@RequestMapping(value = "/sendSMS", method = RequestMethod.GET)
	public void sendSMS(ReservationVO newReservation) {
		logger.info("SMS발송기능 진입");
		final String idNUM = "o.ZWDgNBywcKs9bqzIKcBwUVmtxoTmtFTi"; // 외부 API 액세스
																	// 코드!![수정
																	// 금지]!!
		PushbulletClient client = new PushbulletClient(idNUM);

		AdditionalIntelForSMS addIntel = new AdditionalIntelForSMS();
		addIntel.setRoomID(newReservation.getRoomID());
		addIntel = reservationDAO.getIntelforSMS(addIntel);

		logger.info(addIntel.toString());

		String fullMSG = "[NPP 예약알림] " + newReservation.getSubscriberID() + "님이 " + newReservation.getStartDate()
				+ " ~ " + newReservation.getEndDate() + "간 회원님의 '" + addIntel.getRoomName() + "'을 예약했습니다.";
		logger.info(fullMSG);
		// 한글 깨질 경우 외부 패키지의 post함수 post.setEntity(new
		// StringEntity(json,"utf-8")); 이렇게 수정해야함..
		client.sendSMSPush(addIntel.getPhoneNumber(), fullMSG, "com.pushbullet.android", "ujAhVu4jbmCsjAiVsKnSTs");
	}

}
