package com.scit.npp.VR;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * VR영상 컨트롤러 페이지 입니다 
 * 1. VR영상 출력 담당
 */
@Controller
public class VRController {

	private static final Logger logger = LoggerFactory.getLogger(VRController.class);

	// VR영상View 페이지로 이동
	@RequestMapping(value = "Pano_media", method = RequestMethod.GET)
	public String Pano_media(String Imagepath, Model model) {

		model.addAttribute("ImagePath", Imagepath);

		logger.debug(Imagepath);

		return "kyusoo/Panoramaview/pano";
	}

	// VR영상 리스트 페이지로 이동
	@RequestMapping(value = "VR_List", method = RequestMethod.GET)
	public String VR_List(Locale locale, Model model) {

		return "kyusoo/form/VRListView";
	}

	@RequestMapping(value = "VR_media", method = RequestMethod.GET)
	public String VR_media(String Imagepath, Model model) {
		
		model.addAttribute("ImagePath", Imagepath);
		
		logger.debug("vr이미지경로 : " + Imagepath);
		
		return "kyusoo/VR/360-panorama";

	}

}
