package com.scit.npp.reservation.dao;

import java.util.ArrayList;

import com.scit.npp.jw_vo.Member;
import com.scit.npp.reservation.vo.AdditionalIntelForSMS;
import com.scit.npp.reservation.vo.ReservationAlertVO;
import com.scit.npp.reservation.vo.ReservationVO;

public interface ReservationMapper {

	ArrayList<ReservationVO> getReservation(String roomID);

	ArrayList<ReservationVO> getReservation2();

	ArrayList<ReservationVO> getreserveListforGuest(ReservationVO tgt);

	int cancelReservation(ReservationVO canceledReservation);

	int setReserve(ReservationVO newReservation);

	int updateReservation(ReservationVO updatedReservation);

	int checkListByReservationNum(ReservationVO newReservation);

	AdditionalIntelForSMS getIntelforSMS(AdditionalIntelForSMS addIntel);

	String getHostTEL1(AdditionalIntelForSMS addIntel);

	String getHostTEL2(AdditionalIntelForSMS addIntel);

	ArrayList<ReservationAlertVO> getListForReservationAlert();

	ReservationAlertVO checkDateforAlert();

	void changeVarforsendAlert(ReservationAlertVO tmp);

	void changeDateforsendAlert(ReservationAlertVO tmp);

	Member getMemberType(Member currentMember);

	String getRoomID(Member m);

	String getRoomName(String hostId);

}
