package com.scit.npp.reservation.dao;

import java.util.ArrayList;
import java.util.Calendar;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scit.npp.jw_vo.Member;
import com.scit.npp.reservation.vo.AdditionalIntelForSMS;
import com.scit.npp.reservation.vo.ReservationAlertVO;
import com.scit.npp.reservation.vo.ReservationVO;

@Repository
public class ReservationDAO {
	@Autowired
	SqlSession sqlsession;

	public ArrayList<ReservationVO> getReservation(String roomID) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 목록 획득(방 ID에 따른)[DAO]");
		ArrayList<ReservationVO> list = mapper.getReservation(roomID);
		System.out.println(list.size());
		return list;
	}

	public ArrayList<ReservationVO> getReservation() {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 목록 획득(방 ID에 따른)[DAO]");
		ArrayList<ReservationVO> list = mapper.getReservation2();
		System.out.println(list.size());
		return list;
	}

	public ArrayList<ReservationVO> getreserveListforGuest(String guestID) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 목록 획득(ID에 따른)[DAO]");
		ReservationVO tgt = new ReservationVO();
		tgt.setSubscriberID(guestID);
		ArrayList<ReservationVO> list = mapper.getreserveListforGuest(tgt);
		System.out.println(list.size());
		return list;
	}

	public int cancelReservation(ReservationVO canceledReservation) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 삭제[DAO]");
		int res = mapper.cancelReservation(canceledReservation);
		System.out.println("취소작업 결과: " + res);
		return res;
	}

	public int setReserve(ReservationVO newReservation) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 등록[DAO]");

		int res = mapper.setReserve(newReservation);
		System.out.println("등록작업 결과: " + res);
		return res;
	}

	// 최종작업일 : 180414
	public int updateReservation(ReservationVO updatedReservation) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 변경[DAO]");
		int res = mapper.updateReservation(updatedReservation);
		System.out.println("등록작업 결과: " + res);
		return res;
	}

	public int checkListByReservationNum(ReservationVO newReservation) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("예약 중복확인[DAO]");
		int res = mapper.checkListByReservationNum(newReservation);
		return res;
	}

	public AdditionalIntelForSMS getIntelforSMS(AdditionalIntelForSMS addIntel) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("SMS발송용 정보 확득[DAO]");
		addIntel = mapper.getIntelforSMS(addIntel);
		addIntel.setPhoneNumber(null);
		String telNUM = null;
		telNUM = mapper.getHostTEL1(addIntel);
		System.out.println(addIntel.getPhoneNumber());

		if (telNUM == null) {
			System.out.println("소셜회원 전화번호 검색 진입");
			telNUM = mapper.getHostTEL2(addIntel);
			System.out.println(addIntel.getPhoneNumber());
		}

		addIntel.setPhoneNumber(telNUM);

		return addIntel;
	}

	public ArrayList<ReservationAlertVO> getListForReservationAlert() {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("숙소 사용 예정 알림 SMS발송용 정보 확득[DAO]");// TODO
															// Auto-generated
															// method stub
		ArrayList<ReservationAlertVO> list = mapper.getListForReservationAlert();
		return list;
	}

	public int checkDateforAlert() {
		int res = 0;
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		System.out.println("날짜 및 발송여부 확인[DAO]");
		ReservationAlertVO x = mapper.checkDateforAlert();
		Calendar c = Calendar.getInstance();
		String currentDate = "" + c.get(c.YEAR) + "-" + (c.get(c.MONTH) + 1) + "-" + c.get(c.DATE);
		if (currentDate.equals(x.getStartDate())) {
			System.out.println("일자 일치");
			if (x.getReservationNum().equals("forReservationAlert") && x.getRoomID().equals("forReservationAlert0")) {
				ReservationAlertVO tmp = new ReservationAlertVO();
				tmp.setRoomID("forReservationAlert1");
				mapper.changeVarforsendAlert(tmp); // 1로 바꿔라...
				return 1;
			}
			
		} else {
			ReservationAlertVO tmp = new ReservationAlertVO();
			tmp.setRoomID("forReservationAlert1");
			tmp.setStartDate(currentDate);
			mapper.changeVarforsendAlert(tmp);
			mapper.changeDateforsendAlert(tmp);
			return 1;
		}

		return 0;
	}

	public Member getMemberType(Member currentMember) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		Member memberData = new Member();
		memberData = mapper.getMemberType(currentMember);
		return memberData;
	}

	public ArrayList<ReservationVO> getReservation(Member m) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		ArrayList<ReservationVO> list = new ArrayList<>();
		String roomID = mapper.getRoomID(m);
		if(roomID==null) return null;
		
		System.out.println("방번호:" + roomID);
		list = getReservation(roomID);
		return list;
	}

	public String getRoomName(String currentID) {
		ReservationMapper mapper = sqlsession.getMapper(ReservationMapper.class);
		String hostId = currentID;
		String roomName = mapper.getRoomName(hostId);
		return roomName;
	}

}
