package com.scit.npp.reservation.vo;

public class Calendar2VO {
	private String start;
	private String end;
	private boolean overlap;
	private String rendering;
	private String color;

	public Calendar2VO() {
	}

	public Calendar2VO(String start, String end, boolean overlap, String rendering, String color) {
		this.start = start;
		this.end = end;
		this.overlap = overlap;
		this.rendering = rendering;
		this.color = color;
	}

	@Override
	public String toString() {
		return "Calendar2VO [start=" + start + ", end=" + end + ", overlap=" + overlap + ", rendering=" + rendering
				+ ", color=" + color + "]";
	}

	public String getStart() {
		return start;
	}

	public boolean isOverlap() {
		return overlap;
	}

	public void setOverlap(boolean overlap) {
		this.overlap = overlap;
	}

	public String getRendering() {
		return rendering;
	}

	public void setRendering(String rendering) {
		this.rendering = rendering;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

}
