package com.scit.npp.reservation.vo;

public class AdditionalIntelForSMS {
	
	private String roomID;
	private String roomName;
	private String HostID;	
	private String phoneNumber;
	
	public AdditionalIntelForSMS(){}
	
	public AdditionalIntelForSMS(String roomID, String roomName, String HostID,String phoneNumber){
		this.roomID = roomID;
		this.roomName = roomName;
		this.HostID = HostID;
		this.phoneNumber = phoneNumber;
	}
	
	
	@Override
	public String toString() {
		return "AdditionalIntelForSMS [roomID=" + roomID + ", roomName=" + roomName + ", HostID=" + HostID
				+ ", phoneNumber=" + phoneNumber + "]";
	}

	public String getRoomID() {
		return roomID;
	}
	
	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getHostID() {
		return HostID;
	}
	public void setHostID(String hostID) {
		HostID = hostID;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
