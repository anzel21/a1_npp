package com.scit.npp.reservation.vo;

public class ReservationVO {
	private String roomID;
	private String startDate;
	private String endDate;
	private String subscriberID;
	private String reservationNum;
	private String roomName;
	

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public ReservationVO() {
	}

	@Override
	public String toString() {
		return "ReservationVO [roomID=" + roomID + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", subscriberID=" + subscriberID + ", reservationNum=" + reservationNum + ", roomName=" + roomName
				+ "]";
	}

	public ReservationVO(String reservationNum, String roomID, String startDate, String endDate, String subscriberID) {
		this.reservationNum = reservationNum;
		this.roomID = roomID;
		this.startDate = startDate;
		this.endDate = endDate;
		this.subscriberID = subscriberID;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSubscriberID() {
		return subscriberID;
	}

	public void setSubscriberID(String subscriberID) {
		this.subscriberID = subscriberID;
	}

	public String getReservationNum() {
		return reservationNum;
	}

	public void setReservationNum(String reservationNum) {
		this.reservationNum = reservationNum;
	}

}
