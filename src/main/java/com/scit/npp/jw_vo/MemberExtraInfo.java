package com.scit.npp.jw_vo;

public class MemberExtraInfo {
	private String id;
	private String gender;
	private String job;
	private String concern;
	public MemberExtraInfo(String id, String gender, String job, String concern) {
		this.id = id;
		this.gender = gender;
		this.job = job;
		this.concern = concern;
	}
	public MemberExtraInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getConcern() {
		return concern;
	}
	public void setConcern(String concern) {
		this.concern = concern;
	}
	@Override
	public String toString() {
		return "MemberExtraInfo [id=" + id + ", gender=" + gender + ", job=" + job + ", concern=" + concern + "]";
	}
}
