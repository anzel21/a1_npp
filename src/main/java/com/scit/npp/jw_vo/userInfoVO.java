package com.scit.npp.jw_vo;

public class userInfoVO {
	private String id;
	private String name;
	private String type;
	private String password1;
	private String birthdateCheck;
	private String countryNum;
	private String phonenumber1;
	private String phonenumber2;
	private String phonenumber3;
	private String selfintroduction;
	private String gender;
	private String job;
	private String chk_concern;
	public userInfoVO(String id, String name, String type, String password1, String birthdateCheck, String countryNum,
			String phonenumber1, String phonenumber2, String phonenumber3, String selfintroduction, String gender,
			String job, String chk_concern) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.password1 = password1;
		this.birthdateCheck = birthdateCheck;
		this.countryNum = countryNum;
		this.phonenumber1 = phonenumber1;
		this.phonenumber2 = phonenumber2;
		this.phonenumber3 = phonenumber3;
		this.selfintroduction = selfintroduction;
		this.gender = gender;
		this.job = job;
		this.chk_concern = chk_concern;
	}
	public userInfoVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	public String getBirthdateCheck() {
		return birthdateCheck;
	}
	public void setBirthdateCheck(String birthdateCheck) {
		this.birthdateCheck = birthdateCheck;
	}
	public String getCountryNum() {
		return countryNum;
	}
	public void setCountryNum(String countryNum) {
		this.countryNum = countryNum;
	}
	public String getPhonenumber1() {
		return phonenumber1;
	}
	public void setPhonenumber1(String phonenumber1) {
		this.phonenumber1 = phonenumber1;
	}
	public String getPhonenumber2() {
		return phonenumber2;
	}
	public void setPhonenumber2(String phonenumber2) {
		this.phonenumber2 = phonenumber2;
	}
	public String getPhonenumber3() {
		return phonenumber3;
	}
	public void setPhonenumber3(String phonenumber3) {
		this.phonenumber3 = phonenumber3;
	}
	public String getSelfintroduction() {
		return selfintroduction;
	}
	public void setSelfintroduction(String selfintroduction) {
		this.selfintroduction = selfintroduction;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getChk_concern() {
		return chk_concern;
	}
	public void setChk_concern(String chk_concern) {
		this.chk_concern = chk_concern;
	}
	@Override
	public String toString() {
		return "userInfoVO [id=" + id + ", name=" + name + ", type=" + type + ", password1=" + password1
				+ ", birthdateCheck=" + birthdateCheck + ", countryNum=" + countryNum + ", phonenumber1=" + phonenumber1
				+ ", phonenumber2=" + phonenumber2 + ", phonenumber3=" + phonenumber3 + ", selfintroduction="
				+ selfintroduction + ", gender=" + gender + ", job=" + job + ", chk_concern=" + chk_concern + "]";
	}
}
