package com.scit.npp.jw_vo;

public class SocialMember {
	private String emailid; // db 입력값
	private String identifier; // db 입력값
	private String name; // db 입력값
	private String birthdate; // db 입력값
	private String gender; // db 입력값
	private String logintype; // db 입력값
	private int type; // db 입력값
	private String phonenumber; // db 입력값
	private String selfintroduction; // db 입력값
	private String originalfile; // db 입력값
	private String savedfile; // db 입력값
	
	private String birthdateCheck; // 확인값
	private String countryNum; // 확인값
	private String phonenumber1; // 확인값
	private String phonenumber2; // 확인값
	private String phonenumber3; // 확인값
	private String typeCheck; // 확인값
	
	public SocialMember(String emailid, String identifier, String name, String birthdate, String gender,
			String logintype, int type, String phonenumber, String selfintroduction, String originalfile,
			String savedfile, String birthdateCheck, String countryNum, String phonenumber1, String phonenumber2,
			String phonenumber3, String typeCheck) {
		this.emailid = emailid;
		this.identifier = identifier;
		this.name = name;
		this.birthdate = birthdate;
		this.gender = gender;
		this.logintype = logintype;
		this.type = type;
		this.phonenumber = phonenumber;
		this.selfintroduction = selfintroduction;
		this.originalfile = originalfile;
		this.savedfile = savedfile;
		this.birthdateCheck = birthdateCheck;
		this.countryNum = countryNum;
		this.phonenumber1 = phonenumber1;
		this.phonenumber2 = phonenumber2;
		this.phonenumber3 = phonenumber3;
		this.typeCheck = typeCheck;
	}

	public SocialMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLogintype() {
		return logintype;
	}

	public void setLogintype(String logintype) {
		this.logintype = logintype;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getSelfintroduction() {
		return selfintroduction;
	}

	public void setSelfintroduction(String selfintroduction) {
		this.selfintroduction = selfintroduction;
	}

	public String getOriginalfile() {
		return originalfile;
	}

	public void setOriginalfile(String originalfile) {
		this.originalfile = originalfile;
	}

	public String getSavedfile() {
		return savedfile;
	}

	public void setSavedfile(String savedfile) {
		this.savedfile = savedfile;
	}

	public String getBirthdateCheck() {
		return birthdateCheck;
	}

	public void setBirthdateCheck(String birthdateCheck) {
		this.birthdateCheck = birthdateCheck;
	}

	public String getCountryNum() {
		return countryNum;
	}

	public void setCountryNum(String countryNum) {
		this.countryNum = countryNum;
	}

	public String getPhonenumber1() {
		return phonenumber1;
	}

	public void setPhonenumber1(String phonenumber1) {
		this.phonenumber1 = phonenumber1;
	}

	public String getPhonenumber2() {
		return phonenumber2;
	}

	public void setPhonenumber2(String phonenumber2) {
		this.phonenumber2 = phonenumber2;
	}

	public String getPhonenumber3() {
		return phonenumber3;
	}

	public void setPhonenumber3(String phonenumber3) {
		this.phonenumber3 = phonenumber3;
	}

	public String getTypeCheck() {
		return typeCheck;
	}

	public void setTypeCheck(String typeCheck) {
		this.typeCheck = typeCheck;
	}

	@Override
	public String toString() {
		return "SocialMember [emailid=" + emailid + ", identifier=" + identifier + ", name=" + name + ", birthdate="
				+ birthdate + ", gender=" + gender + ", logintype=" + logintype + ", type=" + type + ", phonenumber="
				+ phonenumber + ", selfintroduction=" + selfintroduction + ", originalfile=" + originalfile
				+ ", savedfile=" + savedfile + ", birthdateCheck=" + birthdateCheck + ", countryNum=" + countryNum
				+ ", phonenumber1=" + phonenumber1 + ", phonenumber2=" + phonenumber2 + ", phonenumber3=" + phonenumber3
				+ ", typeCheck=" + typeCheck + "]";
	}
	
}
