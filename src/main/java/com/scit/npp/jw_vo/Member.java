package com.scit.npp.jw_vo;

public class Member {
	private String id; // db 입력값
	private String pw; // db 입력값
	private String name; // db 입력값
	private int birthdate; // db입력값
	private String phonenumber; // db 입력값
	private int type; // db 입력값
	private String selfintroduction; // db 입력값
	private String originalfile; // db 입력값
	private String savedfile; // db 입력값
	
	private String password1; // 확인값
	private String password2; // 확인값
	private String birthdateCheck; // 확인값
	private String countryNum; // 확인값
	private String phonenumber1; // 확인값
	private String phonenumber2; // 확인값
	private String phonenumber3; // 확인값
	private String typeCheck; // 확인값
	public Member(String id, String pw, String name, int birthdate, String phonenumber, int type,
			String selfintroduction, String originalfile, String savedfile, String password1, String password2,
			String birthdateCheck, String countryNum, String phonenumber1, String phonenumber2, String phonenumber3,
			String typeCheck) {
		this.id = id;
		this.pw = pw;
		this.name = name;
		this.birthdate = birthdate;
		this.phonenumber = phonenumber;
		this.type = type;
		this.selfintroduction = selfintroduction;
		this.originalfile = originalfile;
		this.savedfile = savedfile;
		this.password1 = password1;
		this.password2 = password2;
		this.birthdateCheck = birthdateCheck;
		this.countryNum = countryNum;
		this.phonenumber1 = phonenumber1;
		this.phonenumber2 = phonenumber2;
		this.phonenumber3 = phonenumber3;
		this.typeCheck = typeCheck;
	}
	public Member() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(int birthdate) {
		this.birthdate = birthdate;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getSelfintroduction() {
		return selfintroduction;
	}
	public void setSelfintroduction(String selfintroduction) {
		this.selfintroduction = selfintroduction;
	}
	public String getOriginalfile() {
		return originalfile;
	}
	public void setOriginalfile(String originalfile) {
		this.originalfile = originalfile;
	}
	public String getSavedfile() {
		return savedfile;
	}
	public void setSavedfile(String savedfile) {
		this.savedfile = savedfile;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public String getBirthdateCheck() {
		return birthdateCheck;
	}
	public void setBirthdateCheck(String birthdateCheck) {
		this.birthdateCheck = birthdateCheck;
	}
	public String getCountryNum() {
		return countryNum;
	}
	public void setCountryNum(String countryNum) {
		this.countryNum = countryNum;
	}
	public String getPhonenumber1() {
		return phonenumber1;
	}
	public void setPhonenumber1(String phonenumber1) {
		this.phonenumber1 = phonenumber1;
	}
	public String getPhonenumber2() {
		return phonenumber2;
	}
	public void setPhonenumber2(String phonenumber2) {
		this.phonenumber2 = phonenumber2;
	}
	public String getPhonenumber3() {
		return phonenumber3;
	}
	public void setPhonenumber3(String phonenumber3) {
		this.phonenumber3 = phonenumber3;
	}
	public String getTypeCheck() {
		return typeCheck;
	}
	public void setTypeCheck(String typeCheck) {
		this.typeCheck = typeCheck;
	}
	@Override
	public String toString() {
		return "Member [id=" + id + ", pw=" + pw + ", name=" + name + ", birthdate=" + birthdate + ", phonenumber="
				+ phonenumber + ", type=" + type + ", selfintroduction=" + selfintroduction + ", originalfile="
				+ originalfile + ", savedfile=" + savedfile + ", password1=" + password1 + ", password2=" + password2
				+ ", birthdateCheck=" + birthdateCheck + ", countryNum=" + countryNum + ", phonenumber1=" + phonenumber1
				+ ", phonenumber2=" + phonenumber2 + ", phonenumber3=" + phonenumber3 + ", typeCheck=" + typeCheck
				+ "]";
	}
	
}
