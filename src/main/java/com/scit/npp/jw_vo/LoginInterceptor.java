package com.scit.npp.jw_vo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.scit.npp.jw_memberController.WebMemberController;

	// 요청 후 전, 후 처리를 담당하는 메소드
	public class LoginInterceptor extends HandlerInterceptorAdapter {

		private static final Logger logger = LoggerFactory.getLogger(WebMemberController.class);
		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws Exception {

			logger.debug("LoginInterceptor 실행");
			
			//세션의 로그인 정보를 읽금
			HttpSession session = request.getSession();
			String loginId = (String) session.getAttribute("loginId");
			// 없으면 로그인 페이지로 리다이렉트
			if (loginId == null) {
				response.sendRedirect(request.getContextPath() + "/member/loginError");
				return false;
			}
			return super.preHandle(request, response, handler);
		}
}
