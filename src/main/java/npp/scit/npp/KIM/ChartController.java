package npp.scit.npp.KIM;

import java.util.*;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scit.npp.kyusoochartService.CanvasjsChartService;


/**
 * 
 * 구글 차트 API 컨트롤러 입니다.
 * 
 * 
 * */



@Controller
public class ChartController {
	
	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	
	@Autowired
	private CanvasjsChartService canvasjsChartService;
	
	// 메인 화면 연동 메소드
	@RequestMapping(value = "mychart", method = RequestMethod.GET)
	public String mychart(ModelMap modelMap) {
		
		logger.debug("차트 진입");
		
		List<List<Map<Object, Object>>> canvasjsDataList = canvasjsChartService.getCanvasjsChartData();
		modelMap.addAttribute("dataPointsList", canvasjsDataList);
		
		return "kyusoo/form/googlechart";
	}
	
	
	
}

