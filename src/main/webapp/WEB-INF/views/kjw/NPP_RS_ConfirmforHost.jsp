<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>contacts</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" />
	type="image/x-icon">
<link rel="shortcut icon"
	href=<c:url value = "/resources/images/favicon.ico"/>
	type="image/x-icon" />

<link rel="stylesheet"
	href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css"
	media="screen">
<link rel="stylesheet"
	href=<c:url value = "/resources/css/bootstrap-responsive.css" />
	type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" />
	type="text/css" media="screen">

<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.js" />></script>
<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.easing.1.3.js"/>></script>
<script type="text/javascript"
	src=<c:url value = "/resources/js/superfish.js"/>></script>

<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.ui.totop.js" />></script>

<script type="text/javascript"
	src=<c:url value = "/resources/js/script.js"/>></script>


<link rel='stylesheet'
	href="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.css' />" />
<script src="<c:url value = '/resources/kjw/lib/moment.min.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/locale/ko.js' />"></script>
<script>

		function makeCalendar(listforHost) {
			
			$('#calendar').fullCalendar({
				locale : 'ko', // 요일 및 년/월 한글로 표시되게 설정. 한글표기 되려면 API Lib 임포트해야함.
				header : {
					left : 'prev', //지난달로 이동하는 버튼
					center : 'title', //XXXX년 XX월 표시 
					right : 'next' //다음달로 이동하는 버튼
				},

				showNonCurrentDates : false, // 해당 월 아니면 일자 표시가 안되게 설정
				
				fixedWeekCount : false,

				events : listforHost, // JSON 형식으로 받아온 자료를, API event 형식으로 표시시켜줌.
				
				selectConstraint : listforHost,
				
				displayEventTime : false, // 예약일만 표시되고 시간은 표시안되게 하는 기능
				
				selectable : true, //선택 기능 부여
				
				unselectAuto : true,
				
				validRange : function(nowDate) {
					var calStartDate = moment(nowDate.clone()).format('YYYY-MM-DD');
					var calEndDate = moment(nowDate.clone().add(2, 'years')).format('YYYY-MM');
					return {
						start : calStartDate,
						end : calEndDate
					};
				}, // 이번달부터 2년 후까지만 달력이 표시되게 설정함.
				
				selectOverlap : false
			});
		}
		
	function loadListforHost() {
		listforGuest = null;
		$.ajax({
					url : "<c:url value = '/getreserveList' />",
					
					type : 'GET',
					
					dataType : 'json',
					
					success : function(listforHost) {
						if(listforHost==null){
							$('#outerDIVforCalendar').html("등록된 숙소가 없습니다.");
							return;
						}
						
						makeCalendar(listforHost);
					},
					
					error : function(e) {
						JSON.stringify();
					},
					
				});
	}
	
	$(document).ready(function() {
		loadListforHost();
	});

	$(window).load(function() {
		
	}); //
	
	
</script>

<style>
.fc-center h2 {
	float: none;
	font-style: normal;
	font-size: medium;
	font-size-adjust: none;
	text-decoration: none;
}

#outerDIVforCalendar {
	margin-top: 20px;
	margin-left: 130px;
	margin-bottom: 30px;
	width: 600px;
	font-size-adjust: none;
	font-size: 12pt;
	text-align: center;
}

#calendar {
	margin-top: 60px;
	margin-left: 90px;
	width: 440px;
	height: 340px;
	font-size-adjust: none;
	font-size: 10pt;
	text-align: left;
}
</style>



<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
	<div id="main">

		<div class="top1_wrapper">
			<div class="top1 clearfix">

				<header>
					<div class="logo_wrapper">
						<a href="index.html" class="logo"><img
							src="<c:url value = '/resources/images/logo.png'/>" alt=""></a>

					</div>
				</header>

				<div class="menu_wrapper clearfix">
					<div class="navbar navbar_">


						<div class="navbar-inner navbar-inner_">
							<a class="btn btn-navbar btn-navbar_" data-toggle="collapse"
								data-target=".nav-collapse_"> <span class="icon-bar"></span>
								<span class="icon-bar"></span> <span class="icon-bar"></span>
							</a>

							<div class="nav-collapse nav-collapse_ collapse">
								<ul class="nav sf-menu clearfix">
									<li class="active"><a href="./">home</a></li>
									<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
										<ul>
											<li><a href="index-1.html">who we are</a></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">find
													an agent<em></em>
											</a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li><a href="index-1.html">testimonials</a></li>
										</ul></li>
									<li><a href="VR_media">services</a></li>
									<li><a href="index-3.html">ACCOMMODATIONS</a></li>

									<c:if test="${loginId == null}">
										<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
										<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
									</c:if>

									<c:if test="${loginId != null}">
										<li class="sub-menu sub-menu-1"><a
											href="./member/userInfoForm">MY PAGE</a>
											<ul>
												<li><a href='./member/userInfoForm'>Account info</a></li>
												<li><a href="<c:url value = '/hostAcco/selectType' />">Start
														Hosting</a></li>
												<%-- 예약변경/조회 --%>
												<li><a href="<c:url value = '/RS_Change' /> ">RESERVATION<br>CONFIRMATION
														<li><a href="<c:url value = '/member/logout' />">LOG
																OUT</a></li>
											</ul></li>
									</c:if>
								</ul>
							</div>
						</div>
					</div>
					<div id="search">

						<a href="#" class="searchBtn"></a>

						<div class="search-form-wrapper">
							<form id="search-form" action="search.php" method="GET"
								accept-charset="utf-8" class="navbar-form">
								<input type="text" name="s" value='Search'
									onBlur="if(this.value=='') this.value='Search'"
									onFocus="if(this.value =='Search' ) this.value=''"> <a
									href="#"
									onClick="document.getElementById('search-form').submit()"></a>
							</form>
						</div>

					</div>
				</div>

			</div>
		</div>

		<div id="inner">

			<div class="top2_wrapper">
				<div class="bg1">
					<img src="<c:url value = '/resources/images/bg1.jpg'/>" alt=""
						class="img">
				</div>
				<div class="top2_inner">
					<div class="container">
						<div class="top2 clearfix">
							<h1>My Page</h1>
						</div>
					</div>
				</div>
			</div>


			<div id="content">

				<div class="container">
					<div class="row">


						<div class="span9">
							<h2>
								<span>소유 숙소 예약 확인</span>
							</h2>
							<div id='outerDIVforCalendar'>
								${sessionScope.roomNameforPrint} 예약 현황<br>
								<div id='calendar'></div>
							</div>
						</div>

						<div class="span3">
							<h2>
								<span>My page Menu</span>
							</h2>
							<div style="text-align: center; float: none;">
								<img
									src="<c:url value = 'member/download?loginId=${loginId}&loginType=${loginType}'/>"
									style="height: 150px; width: 150px;"> <br>
								<c:if test="${loginId != null}">
									${loginId}
								</c:if>
							</div>

							<ul class="ul1">
								<li><a href="#" onclick="registPicturePopUp()">프로필 사진
										변경</a></li>
								<li><a href="<c:url value = './member/userInfoForm' />">개인정보
										조회</a></li>
								<li><a href="<c:url value = '/RS_Change' />">예약 정보 조회</a></li>

								<li><a href='javascript:void(0);'
									onclick="hostingCheck(${hosting}, ${registRoomCheck})">숙소
										등록하기</a></li>

								<li>숙소 이미지 정보 수정</li>
								<ul>
									<li><a href='javascript:void(0);'
										onclick="pictureRegist(${registRoomCheck})">사진 등록</a></li>
									<li><a href='javascript:void(0);'
										onclick="panoRegist(${registRoomCheck})">파노라마 사진 등록</a></li>
									<li><a href='javascript:void(0);'
										onclick="vrRegist(${registRoomCheck})">VR 사진 등록</a></li>
								</ul>
							</ul>

						</div>


					</div>

				</div>

			</div>
		</div>

		<div class="bot1_wrapper">
			<div class="container">
				<div class="bot1 clearfix">
					<div class="row">
						<div class="span3">

							<div class="bot1_title">Copyright</div>

							<div class="logo2_wrapper">
								<a href="index.html" class="logo2"><img
									src="<c:url value = '/resources/images/logo2.png'/>" alt=""></a>
							</div>

							<footer>
								<div class="copyright">
									Copyright © 2014.<br>All rights reserved.
								</div>
							</footer>

						</div>
						<div class="span5">

							<div class="bot1_title">Useful information</div>

							<p>
								<b> Nulla ultricies enim aliquet augue eleifend iaculis. </b>
							</p>

							<p>Nam sollicitudin ligula ac nisi iaculis eu scelerisque
								risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean
								leo nulla, aliquet vitae ultricies sit amet, porttitor id
								sapien. In hac habitasse platea dictumst. Donec pharetra gravida
								augue at hendrerit. Cras ut...</p>

						</div>
						<div class="span3 offset1">

							<div class="bot1_title">Follow Us</div>

							<div class="social_wrapper">
								<ul class="social clearfix">
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic1.png'/>"></a></li>
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic2.png'/>"></a></li>
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic3.png'/>"></a></li>
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic4.png'/>"></a></li>
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic5.png'/>"></a></li>
									<li><a href="#"><img
											src="<c:url value = '/resources/images/social_ic6.png'/>"></a></li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>
	<script type="text/javascript"
		src='<c:url value = "/resources/js/bootstrap.js"/>'></script>



</body>
</html>