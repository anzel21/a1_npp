<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE=html>
<html>
<head>
<meta charset='UTF-8' />
<title>숙소예약체계</title>

<link rel='stylesheet'
	href="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.css' />" />
<script src="<c:url value = '/resources/kjw/lib/moment.min.js' />"></script>

<%-- FullCalendar API 및 한국어 로케일 로딩 --%>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/locale/ko.js' />"></script>
<script>
	var calendarSwitch = 0;
	var guestSwitch = 0;
	var selectedGuestNum = 0;
	var adultGuestNum = 0;
	var kidGuestNum = 0;
	var newEvent;
	var list;
	var pastdays;
	var newEndDate;
	var roomID = ${RoomInfo.roomID};
	var priceOfRoomPerPerson = ${RoomInfo.price};
	var maxPersonalforRoom = ${RoomInfo.accommodation};
	var totalPrice;
	
	$(document).ready(function() {
		// 페이지 오픈 시, 캘린더 및 인원수 선택 창 숨김
		$('#PaymentDIV').hide();
		$('#calendar').hide();
		$('#guestNumber').hide();
		calendarSwitch = 0; // 캘린더 창 여닫는 스위치 변수
		$('#selectedguestnum').html(selectedGuestNum); // 선택된 총인원수(성인+어린이) 표시[기본값  :0]
		$('#adultGuestNum').val(adultGuestNum); // 선택된 성인 인원수 표시[기본값  :0]
		$('#kidGuestNum').val(kidGuestNum); // 선택된 어린이 표시[기본값  :0]
		$('#startDate').html('  체크인'); // 체크인 일자 표시 창에 '체크인' 문자 표시
		$('#endDate').html('  체크아웃'); // 체크인 일자 표시 창에 '체크인' 문자 표시
		$('#totalPriceSpan').html(priceOfRoomPerPerson+"원 / 1인");
		$('#maxPersonalforRoomSpan').html(maxPersonalforRoom+"명");
		
		loadList(); //DB에서 전체 예약 일정 불러옴
	});

	function loadList() { // 각 숙소에 따른 예약현황을 불러옴. !![DB자료 부족으로 아직 변수 전송 안함]!!
		//!! 관련 DB 테이블 개설 시 필수적으로 수정할 것!! (최종작업일:180414)
		$.ajax({
			url : "<c:url value = '/getreserveList2' />",
			type : 'POST',
			data : { roomID },
			dataType : 'json',
			success : function(list) {
				if (newEvent != null)
					// 사용자가 기간을 선택했을 경우, 해당 일정을 캘린더에 표시하기 위해 목록에 넣어줌.

					list.push(newEvent);
				makeCalendar(list);
			},
			error : function(e) {
				JSON.stringify();
			},
		});
	}

	// 예약일정에 따른 캘린더 생성
	function makeCalendar(list) {
		var dateOfFocus = moment().format();

		
		if (list.length != 0 && list[list.length - 1].color == '#00ff00') {
			dateOfFocus = list[list.length - 1].start;
		}

		$('#calendar').fullCalendar(
				{
					locale : 'ko', // 요일 및 년/월 한글로 표시되게 설정. 한글표기 되려면 API Lib 임포트해야함.

					defaultDate : dateOfFocus,

					header : {
						left : 'prev', //지난달로 이동하는 버튼
						center : 'title', //XXXX년 XX월 표시 
						right : 'next' //다음달로 이동하는 버튼
					},

					showNonCurrentDates : false, // 해당 월 아니면 일자 표시가 안되게 설정
					fixedWeekCount : false,

					validRange : function(nowDate) {
						var dateOfToday = moment(nowDate.clone()).format(
								'YYYY-MM-DD');
						var dateOfToday2 = moment(nowDate.clone()).format(
								'YYYY-MM');
						var dayOfToday = dateOfToday.substring(8);
						// alert(dayOfToday); //현재 일자 확인을 위한 alert 함수..

						var calStartDate = moment(nowDate.clone()).format(
								'YYYY-MM-DD'); //이번달 1일 부터
						var calEndDate = moment(
						// 2년후의 기간만 조회할 수 있도록 캘린더 기간범위 고정
						nowDate.clone().add(2, 'years')).format('YYYY-MM');
						return {
							start : calStartDate,
							end : calEndDate
						};
					}, // 이번달부터 2년 후까지만 달력이 표시되게 설정함.

					events : list, // JSON 형식으로 받아온 자료를, API event 형식으로 표시시켜줌.
					selectConstraint : list, // 이미 기예약된 기간은 선택하지 못하게 하는 옵션
					displayEventTime : false, // 예약일만 표시되고 시간은 표시안되게 하는 기능
					selectable : true, //선택 기능 부여
					unselectAuto : true,

					dayClick : false,
//						function(date){
						//var sDate = date.format("YYYY-MM-DD");
						//var eDate = date.clone().add(1, 'days').format("YYYY-MM-DD");
						//$('#startDate').html("   " + sDate + " "); // UI에 체크인 일자 표시
						//$('#endDate').html("   " + eDate + " "); // UI에 체크아웃 일자 표시
						//newEvent = { // 사용자의 예약신청이벤트 생성
							//	start : '' + sDate,
							//	end : '' + eDate,
							//	overlap : '' + "false",
							//	rendering : "" + 'background',
							//	color : '#00ff00'
							//}; // 객체로 만들어서,

							// 이미 선택했던 기간을 다시 다른기간으로 바꿀경우,
							//if (list.length != 0 && list[list.length - 1].color == '#00ff00') {
							//	list.pop(); // 원래 선택했던 기간을 리스트에서 제거함.
							//	focusDate = moment().format('YYYY-MM-DD');
							//	reloadCalendar(); // 캘린더 객체 재생성 함수
							//} else {
							//	list.push(newEvent); // JSON으로 불러온 기존 리스트에 더해준다.
							//	$('#calendar').fullCalendar('renderEvents', list); // 일정 선택 시 해당 기간 표시해 줌.
							//}
	//				},
					
					// 기간 선택 시, 해당 기능을 타 사용자가 예약한 기간과 구분되게 녹색으로 이벤트 구성							
					select : function(startDate, endDate) {
						$('#startDate').html("   " + startDate.format() + " "); // UI에 체크인 일자 표시
						newEndDate = endDate.clone().subtract(1, 'days')
								.format(); // DB에는 예약기간이 저장되므로 일자는 -1해서 저장
								
						$('#endDate').html("   " + newEndDate); // UI에 체크아웃 일자 표시
						if(startDate.format() == newEndDate){
							newEndDate = endDate.clone().format(); // DB에는 예약기간이 저장되므로 일자는 -1해서 저장
						$('#endDate').html("   " + newEndDate); // UI에 체크아웃 일자 표시
						}
						
						newEvent = { // 사용자의 예약신청이벤트 생성
							start : '' + startDate.format(),
							end : '' + endDate.format(),
							overlap : '' + "false",
							rendering : "" + 'background',
							color : '#00ff00'
						}; // 객체로 만들어서,

						// 이미 선택했던 기간을 다시 다른기간으로 바꿀경우,
						if (list.length != 0 && list[list.length - 1].color == '#00ff00') {
							list.pop(); // 원래 선택했던 기간을 리스트에서 제거함.
							focusDate = moment().format('YYYY-MM-DD');
							reloadCalendar(); // 캘린더 객체 재생성 함수
						} else {
							list.push(newEvent); // JSON으로 불러온 기존 리스트에 더해준다.
							$('#calendar').fullCalendar('renderEvents', list); // 일정 선택 시 해당 기간 표시해 줌.
						}
					},
					selectOverlap : false

				});
	}

	function reloadCalendar() { //캘린더 재생성 함수
		$('#calendar').fullCalendar('destroy'); // 캘린더 제거 
		loadList(); // 해당 숙소 예약 리스트 리로딩
	}

	function clearCalendar() { // UI에서 취소버튼 선택 시, 캘린더 내용 비워주는 함수
		$('#calendar').fullCalendar('destroy'); // 캘린더 제거 
		newEvent = null; // 사용자 선택 기간 초기화

		// UI의 체크인/아웃 일자 초기화
		$('#startDate').html('  체크인');
		$('#endDate').html('  체크아웃');

		//목록 다시 불러오기
		loadList();
	}

	function checkdateClicked() { // 캘린더 창 표시-숨김 기능 관련 함수
		if (calendarSwitch == 0) {
			calendarSwitch = 1;
			$('#calendar').show(); // 열리고
		} else {
			calendarSwitch = 0;
			$('#calendar').hide(); // 닫힌다.
		}
	}

	function selectguestnum() { // 숙박인원 수 선택 창 표시-숨김 기능 관련 함수
		if (guestSwitch == 0) {
			guestSwitch = 1;
			$('#guestNumber').show(); // 열리고
		} else {
			guestSwitch = 0;
			$('#guestNumber').hide(); // 닫힌다.
		}
	}

	// 성인 숙박인원수 조정 관련 함수
	function minusAdultGuest() { // 인원 감소
		adultGuestNum--;
		if (adultGuestNum <= 0)
			adultGuestNum = 0;
		$('#adultGuestNum').val(adultGuestNum);
		recalculateGuestNum("AM");
	}
	function plusAdultGuest() { // 인원 증가
		adultGuestNum++;
		$('#adultGuestNum').val(adultGuestNum);
		recalculateGuestNum("AP");
	}
	// 어린이 숙박인원수 조정 관련 함수
	function minusKidGuest() { // 인원 감소
		kidGuestNum--;
		if (kidGuestNum <= 0)
			kidGuestNum = 0;
		$('#kidGuestNum').val(kidGuestNum);
		recalculateGuestNum("KM");
	}
	function plusKidGuest() { // 인원 증가
		kidGuestNum++;
		$('#kidGuestNum').val(kidGuestNum);
		recalculateGuestNum("KP");
	}
	
	// 총 숙박 인원 수(성인+어린이) 계산 관련 함수
	function recalculateGuestNum(receivedCode) {
		selectedGuestNum = kidGuestNum + adultGuestNum;
		if(maxPersonalforRoom < selectedGuestNum){
			alert("최대 숙박 인원은 " + maxPersonalforRoom + "명입니다.");
		 	if(receivedCode == "AP") 	{adultGuestNum--;	$('#adultGuestNum').val(adultGuestNum);}
		 	if(receivedCode == "KP"){kidGuestNum--; $('#kidGuestNum').val(kidGuestNum);}
		 	selectedGuestNum = kidGuestNum + adultGuestNum;
		}
		totalPrice = priceOfRoomPerPerson * selectedGuestNum;
		$('#totalPriceSpan').html(totalPrice + "원");
		if(selectedGuestNum==0) $('#totalPriceSpan').html(priceOfRoomPerPerson+"원 / 1인");
		$('#selectedguestnum').html(selectedGuestNum);
	}

	// 기간 및 인원 선택 후, "예약하기" 버튼 클릭 시, 컨트롤러에 관련 데이터 전송하는 함수
	// 추후 기능 병합 시, 추가작업 필요함. [최종작업일 : 180414]

	
	function goReservation() {

		if (selectedGuestNum == 0) {
			alert('인원수를 선택해 주세요.');
			return;
		}
		newEvent.roomID = roomID;
		var checkforLogin = "${sessionScope.loginId}"; 
		
		if(checkforLogin==""){
			alert("로그인이 필요한 기능입니다.");
			return;
		}
		
		if (newEvent != null) {
			newEvent.end = newEndDate;
			var checkingMsg1 = "예약하신 사항은 다음과 같습니다.\n\n";
			var checkingMsg2 = "방 이름: ";
			var checkingMsg3 = "기간: " + newEvent.start + " ~ " + newEvent.end;
			var checkingMsg4 = "총 인원 수: " + selectedGuestNum + "명  [성인: "
					+ adultGuestNum + "명   /   어린이: " + kidGuestNum + "명]";
			var checkingMsg5 = "총 비용: ";
			var checkingMsg6 = "상기 조건으로 결제를 진행하시겠습니까?";
			
			var finalCheck = confirm(checkingMsg1 + "\n\n" + checkingMsg2
					+ "\n\n" + checkingMsg3 + "\n\n" + checkingMsg4 + "\n\n"
					+ checkingMsg5 + "\n\n" + checkingMsg6);
			
			if (finalCheck) {
				$('#PaymentDIV').show();
				$('#selectingDate').hide();
				$('#periodOfReservation').html(newEvent.start +" ~ "+ newEvent.end);
				$('#numOfPeople').html(selectedGuestNum + "명");
				$('#finalPayment').html(totalPrice + "원");
				$('#nameOfHouse').html("${RoomInfo.roomName}");
			} else {
				alert("비용 결제 보류");
			}
		} else
			alert('기간을 선택하지 않았습니다.');
	}
	
	
	function goReservation2(){ 
		$.ajax({
			url : '<c:url value = "/setReserve" />', // ReservationController에 등록된 주소로 전송
			type : 'post',
			data : newEvent, // 신규 이벤트 함수를
			dataType : 'json',
			success : function(res) {
				if (res == 1) {
					alert("신규 예약 추가 성공");
				} 
				
				if (res == 0) {
					alert("[오류] 로그인이 필요한 기능입니다.");
				}
			},

			error : function(e) {
				JSON.stringify();
			}
		});
	}
	
	
</script>

<style>
.fc-center h2 {
	float: none;
	font-style: normal;
	font-size: medium;
	font-size-adjust: none;
	text-decoration: none;
}

#calendar {
	margin-top: 15px;
	width: 340px;
	height: 280px;
	font-size-adjust: none;
	font-size: 8pt;
	text-align: left;
	width: 320px;
}

#selectingDate {
	width: 340px;
	height: 420px;
	text-align: left;
	border: solid, black, 1px;
}

#selectingDateTable {
	width: 340px;
	color: gray;
}

.date {
	width: 80px;
	border-color: grey;
	border-style: solid;
	font-size: 9pt;
	text-align: right;
	float: left;
}

.numofPeople {
	margin-top: 5px;
	border: none;
}

.goReservation:hover {
	cursor: pointer;
}

.selectButton:hover {
	cursor: pointer;
}

<%-- 결제창 CSS--%>
#PaymentDIV {
	width: 360px;
	height: 320px;
	background-color: purple;
	text-align: left;
	margin: 0 auto;
	padding-top: 10px;
}

#PaymentDIVBody {
	width: 330px;
	height: 300px;
	background-color: white;
	text-align: center;
	margin: 0 auto;
	padding-top: 10px;
}

#PaymentInfoTable, #PaymentInfoTable2{
	font-family: "굴림";
	font-size: 8pt;
	color: black;
	margin: 0 auto;
	text-align: left;
	width: 310px;
	border: thin;
	border-color: gray;
	border-style: solid;
	opacity: 0.7;
}

#PaymentInfoTable3 {
	font-family: "굴림";
	font-size: 8pt;
	color: black;
	margin: 0 auto;
	text-align: left;
	width: 270px;
}

#PaymentInfoTable tr {
	height: 20px;
}

#PaymentInfoTable2 tr {
	height: 25px;
}

.intelOutput {
}

.textBoxforCard {
	width: 35px;
	border: thin;
	border-color: gray;
	border-style: solid;
}

.paymentBtn:hover {
	cursor: pointer;
}

.paymentBtn {
	margin: 0 auto;
	padding-top: 5px;
	color: white;
	text-align: center;
	width: 60px;
	height: 20px;
	background-color: gray;
	vertical-align: middle;
	color: white;
}

.typeofCards {
	width : 60px;
	font-family: "굴림";
	font-size: 8pt;
	color: black;
}

</style>

</head>

<body>

		<div id='selectingDate'>
			<%-- FullCalendar API 내부 명령으로 폰트 사이즈 축소가 어려우므로, 
		외부 div 혹은 span에 CSS로  Font 크기 강제 조정해야함.--%>

			<table style="background-color: white;">

				<tr>
					<td style="height:20px;">
					<span style="font-size: 10pt;"> 
							가격 : <span style=" "height:30px; font-size: 11pt;" id="totalPriceSpan"></span><br> 
					</span></td>
				</tr>
				<%-- <tr>
					<td style="height:20px;">
					<span style="font-size: 10pt;"> 방별 별점을 서버에서 불러와 표시해야 한다. 추후 추가 작업 필요 (최종작업일: 180422)
							별점 : <span style=" "margin-top:5px; font-size: 11pt;" id="roomAssessmentSpan">DB에서 불러와야 함</span>
							</span></td>
				</tr> --%>
				<tr>
					<td style="height:20px; font-size: 10pt;">날짜</td>
				</tr>
				<tr style=" font-size: 11pt;">
					<td>
						<div
							style="border-color: grey; border-style: solid; width: 325px;">
							<table>
								<tr>
									<td style="width: 255px; font-size: 11pt;">
										<div id="startDate" class="date"></div>

										<div class="date"
											style="border: none; text-align: center; font-size: 12pt; font-weight: bolder;">→</div>
										<div id="endDate" class="date"></div>
									</td>

									<td>
										<%--클릭 시, 캘린더 표시/숨김을 조정한다. --%> <image class="selectButton"
											src="<c:url value = '/resources/kjw/magnifier.png' />"
											style="width:25px; height:25px;"
											onclick='javascript:checkdateClicked()'> <%--클릭 시, 선택되었던 기간 제거 및 캘린더를 초기화한다. --%>
										<image class="selectButton"
											src="<c:url value = '/resources/kjw/refresh.png' />"
											style="margin-left:5px; width:25px; height:25px;"
											onclick='javascript:clearCalendar()'>
									</td>
								</tr>
							</table>

						</div> <%--외부 API 사용하는 캘린더 표시용 DIV,  --%>
						<div id='calendar'></div>
					</td>
				</tr>
				<tr>
					<td><span style="font-size:9pt; color:red;">*체크인 - 체크아웃 기간은 드래그로 선택할 수 있습니다.</span></td>
				</tr>

				<tr>

					<td>인원<br>
						<div
							style="border-color: grey; border-style: solid; width: 325px;">
							<table>
								<tr>
									<td style="width: 280px; font-size:10pt;">게스트 <span
											id="selectedguestnum"></span>명
									</td>
									<td>
										<%--클릭 시, 인원 조정 DIV가 표시된다. --%> <image class="selectButton"
											style=" margin-left:20px; width:20px; height:20px;"
											src="<c:url value = '/resources/kjw/arrow_under.png' />"
											onclick='javascript:selectguestnum()'>
									</td>

								</tr>
							</table>
						</div> <%-- 인원 수 조정 관련 DIV --%>
						<div id='guestNumber'
							style="border-color: grey; border-style: solid; background-color: white; width: 325px;">
							<table>


								<%-- 성인 인원 수 조정 관련 DIV --%>
								<tr>
									<td style="font-size:10pt; width: 150px;">성인</td>
									<td style="width: 50px;"><image class="selectButton"
											style="width:25px; height:25px"
											src="<c:url value = '/resources/kjw/minus.png' />"
											onclick='javascript:minusAdultGuest()'></image></td>
									<td style="text-align: right; width: 50px;"><input
										id="adultGuestNum" class="numofPeople" type="text"
										style="width: 30px"></td>
									<td style="width: 50px; text-align: right;"><image
											style="width:25px; height:25px" class="selectButton"
											src="<c:url value = '/resources/kjw/plus.png' />"
											onclick='javascript:plusAdultGuest()'></image></td>
								</tr>

								<%-- 어린이 인원 수 조정 관련 DIV --%>
								<tr>
									<td style="width: 150px; font-size:10pt;">어린이</td>
									<td style="width: 50px;"><image
											style="width:25px; height:25px" class="selectButton"
											src="<c:url value = '/resources/kjw/minus.png' />"
											onclick='javascript:minusKidGuest()'></image></td>
									<td style="text-align: right; width: 50px;"><input
										id="kidGuestNum" class="numofPeople" type="text"
										style="width: 30px"></td>
									<td style="width: 50px; text-align: right;"><image
											style="width:25px; height:25px" class="selectButton"
											src="<c:url value = '/resources/kjw/plus.png' />"
											onclick='javascript:plusKidGuest()'></image></td>
								</tr>
							</table>

							<%-- 관련 DB 구성 시, 수정 필요 [최종작업일 : 180414] --%>
							<span style = "font-size:10pt;">최대 <span style="color: orange;"
								id="maxPersonalforRoomSpan"></span> 숙박 가능합니다. </span>
						</div></td>
				</tr>

				<tr class="goReservation" style="height: 10px;"></tr>
				<tr>
					<td>
						<%-- 예약 진행 버튼 --%>
						<div class="goReservation" style="width: 325px; height: 20px;"
							onclick="javascript:goReservation()">
							<div class="goReservation"
								style="margin: 0 auto; padding:5px; color: white; text-align: center; width: 300px; background-color: purple;">
								<small class="goReservation" style="font-size: 12pt;">예약하기</small>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="PaymentDIV">
		<div id="PaymentDIVBody">
			<table id="PaymentInfoTable">
				<tr>
					<td><h3>NPP PAY</h3></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2">
						<div style="float: left;">예약 기간:&nbsp;</div>
						<span id = "periodOfReservation" class="intelOutput"></span>
					</td>
					<td></td>
				</tr>

				<tr>
					<td>
						<div style="float: left;">숙소 명:&nbsp;&nbsp;&nbsp;</div>
						<span id = "nameOfHouse"class="intelOutput"></span>
					</td>
					<td>
						<div style="float: left;">총 인원수:&nbsp;</div>
						<span id = "numOfPeople" class="intelOutput"></span>
					</td>
				</tr>

				<tr>
					<td>
						<div style="float: left;">결제 금액:&nbsp;</div>
						<span id = "finalPayment" class="intelOutput"></span>
					</td>
					<td></td>
				</tr>
			</table>
			<br>

			<table id="PaymentInfoTable2">
				<tr>
					<td>
						<div style="float: left;">카드번호&nbsp;</div>
					</td>
					<td>
						<div style="float: left;">
							<input class="textBoxforCard" type="text" maxlength="4">-<input
								class="textBoxforCard" type="password" maxlength="4">-<input
								class="textBoxforCard" type="password" maxlength="4">-<input
								class="textBoxforCard" type="text" maxlength="4">
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<div style="float: left;">CVC:&nbsp;</div>
					</td>
					<td>
						<div style="float: left;">
							<input class="textBoxforCard" type="password" maxlength="3" style = "width:40px;">
						</div>&nbsp;&nbsp;&nbsp; 카드 종류: <select class="typeofCards" style = "width:90px;"><option
								class="typeofCards">VISA</option>
							<option class="typeofCards">MASTER</option>
							<option class="typeofCards">UnionPay</option></select>
					</td>
				</tr>

				<tr>
					<td>
						<div style="float: left;">비밀번호:&nbsp;</div>
					</td>
					<td>
						<div style="float: left;">
							<input class="textBoxforCard" type="password" maxlength="15">
						</div>
					</td>
				</tr>
			</table>
			<br>

			<table id="PaymentInfoTable3" style="text-align: center;">
				<tr>
					<td>
						<div class="paymentBtn" style="background-color: green;" onclick="javascript:goReservation2()"> 
							<small style="font-size: 8pt;">결제</small>
						</div>
					</td>
					<td>
						<div class="paymentBtn" style="background-color: red;" onclick="javascript:paymentCalcel()">
							<small style="font-size: 8pt;">취소</small>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
		
		
</body>
</html>

