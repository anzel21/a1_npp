<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>gallery</title>
<!-- PanoramaView 의 Css를 불러옴 -->
<style type="text/css">
	@import url("../resources/css/kyusoo_panorama/pano.css");
</style>
<style type="text/css">
	@import url("./../resources/css/jw_main/login.css");
	@import url("./../resources/css/jw_main/join.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">   
<link rel="stylesheet" href=<c:url value = "/resources/css/touchTouch.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/isotope.css" /> type="text/css" media="screen">      
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" />></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js" />></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js" />></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" />></script>

<script type="text/javascript" src=<c:url value = "/resources/js/touchTouch.jquery.js" />></script>
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.isotope.min.js" />></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js" />></script>
<script>
<%-- 호스팅 신청 페이지로 이동 전 신청자인지 확인 --%>
function hostingCheck(a, registRoomCheck) {
	if (registRoomCheck == 0) {
		if (a == 1) {
			location.href = "../hostAcco/selectType"
		} else {
			alert("회원정보에서 호스팅을 신청해주세요.");
		}
	} else {
		alert("이미 등록하신 숙소가 존재합니다.");
	}
}
<%-- 상단 메뉴의 돋보기 버튼 클릭시, accmmolist 로 이동 --%>
function moveAccmmolist() {
	var inputAddress = $('#inputAddress').val();
	location.href ="./../accmmolist?inputAddress="+inputAddress;
}
<%-- 서비스 조회 전 로그인 여부 확인--%>
function servicesCheck(loginId) {
	if (loginId != null) {
		location.href="/npp/mychart";
	} else {
		alert("로그인 후 이용 가능합니다.");
	}
}
</script>		
<!-- panoGallery.js 에서 panoGallery 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/360panorama/js/panoGallery.js"></script>
<!-- join.js 에서 회원가입 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/join.js"></script>	
<!-- login.js 에서 login 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/login.js"></script>	
</head>

<body class="not-front">
<!-- pano.jsp 에서 파노라마 Overlay 페이지 불러옴 -->
<%@ include file = "../kyusoo/Panoramaview/pano.jsp" %>
<!-- JW_Login 화면 동작 -->
<%@ include file = "./../jw_login/loginForm.jsp" %>
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
	
<header><div class="logo_wrapper"><a href="./../" class="logo"><img src=<c:url value = "/resources/images/logo.png" /> alt=""></a></div></header>

<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
			<li><a href="./../">Home</a></li>
				<%-- <li class="sub-menu sub-menu-1"><a href="#">About</a>
			<ul>
				<li><a href="<c:url value = "/VR_media" />">VR View</a></li>
				<li class="sub-menu sub-menu-2"><a href="<c:url value = "/Pano_media" />">Pano View<em></em></a></li>
				<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>
				<li><a href="index-1.html">testimonials</a></li>
			</ul></li>
			<c:if test="${loginId == null}">
				<li><a href='javascript:void(0);' onclick="servicesCheck(${loginId})">services</a></li>
			</c:if>
			<c:if test="${loginId != null}">
				<li><a href='javascript:void(0);' onclick="servicesCheck('${loginId}')">services</a></li>
			</c:if> --%>
			<li><a href="<c:url value = "/accmmolist" />">Accommodations</a></li>
			<c:if test="${loginId == null}">
				<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
				<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
			</c:if>
			<c:if test="${loginId != null}">
					<li class="sub-menu sub-menu-1"><a href="<c:url value = "/member/userInfoForm" />">MY PAGE</a>
				<ul>
					<li><a href="<c:url value = "/member/userInfoForm" />">Accountinfo</a></li>
					<li><a href='javascript:void(0);' onclick="hostingCheck(${hosting}, ${registRoomCheck})">Start Hosting</a></li>
					<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
					<li><a href="<c:url value = "/member/logout" />">Logout</a></li>
				</ul></li>
			</c:if>
			</ul>
			</div>
		</div>
	</div>
	<div id="search">
		<a href="#" class="searchBtn"></a>
		<!-- 상단 메뉴의 검색  -->
		<div class="search-form-wrapper">
		<form id="search-form" action="../accmmolist" method="GET"	accept-charset="utf-8" class="navbar-form clearfix">
			<input class="searchPlaceHolder" id="inputAddress" type="text" name="inputAddress" placeholder='방문지를 입력해주세요.'> 
			<a href="#" onClick="moveAccmmolist()"></a>
		</form>
</div>

</div>	
</div>

</div>	
</div>

<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="" class="img"></div>
<div class="top2_inner">
<div class="container">
<div class="top2 clearfix">
	
<h1>Panorama Gallery</h1>

</div>	
</div>	
</div>
</div>

<div id="content">
<div class="container">
<div class="row">
<div class="span12">
	
<h2><span>파노라마 사진집</span></h2>

<ul class="thumbnails" id="isotope-items">		  
    <c:forEach var="piclist" items="${piclist}">
	  <li class="span4 isotope-element isotope-filter1">
			<div class="thumb-isotope">
				<div class="thumbnail clearfix">
					<a onclick="openPanoPage('/npp/resources/panopicture/${piclist.picturePath}')">
						<%-- <script type="text/javascript" src="<c:url value="./../resources/360panorama/js/html5pano.js?path=../resources/normalpicture/${piclist.picturePath}" />"></script> --%>
						<figure>
							<img id="popUpimg" src="<c:url value="/resources/panopicture/${piclist.picturePath}"/>"><em></em>
						</figure>
						<div class="caption">
							이미지를 클릭해주세요.
						</div>
					</a>
				</div>
			</div>
	  </li>
	</c:forEach>
       
</ul>

</div>	
</div>


</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	

<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="../resources/images/NPPlogo.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2018.<br>NPP FINAL PROJECT</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">NPP FINAL PROJCET IS</div>

<p style="font-size: 12px;">
	Made By<br>
	<br>
	Coding Slaves Team(C.S Team)<br>
	KYU SOO KIM(LEADER)<br>
	JUN WOO KIM<br>
	JIN WON SEO<br>
	WON TAE CHO<br>
</p>

</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>