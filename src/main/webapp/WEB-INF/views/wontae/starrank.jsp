<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>숙소 총평</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script>
$(document).ready(function() {
	//
	var total = $('input[name="total"]:checked').val();
	
	$('input[name="convenience"]').on('click', function() {
		var taginside = false;
		var convenience = $('input[name="convenience"]:checked').val();
		if (convenience != 10) {
			$('#convreason').html("불만족스러웠던 점을 적어주세요<br><textarea class='convereason' style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'></textarea>");
			taginside = true;
		} else if (convenience == 10 && taginside == false) {
			$('#convreason').empty();
			taginside = false;
		}
	});
	
	$('input[name="accessibility"]').on('click', function() {
		var taginside = false;
		var accessibility = $('input[name="accessibility"]:checked').val();
		if (accessibility != 10) {
			$('#accessreason').html("불만족스러웠던 점을 적어주세요<br><textarea class='accessibilityreason' style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'></textarea>");
			taginside = true;
		} else if (accessibility == 10 && taginside == false) {
			$('#accessreason').empty();
			taginside = false;
		}
	});
	
	$('input[name="kindness"]').on('click', function() {
		var taginside = false;
		var kindness = $('input[name="kindness"]:checked').val();
		if (kindness != 10) {
			$('#kinreason').html("불만족스러웠던 점을 적어주세요<br><textarea class='kindreason' style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'></textarea>");
			taginside = true;
		} else if (kindness == 10 && taginside == false) {
			$('#kinreason').empty();
			taginside = false;
		}
	});
	
	$('input[name="sanitation"]').on('click', function() {
		var taginside = false;
		var sanitation = $('input[name="sanitation"]:checked').val();
		if (sanitation != 10) {
			$('#sanreason').html("불만족스러웠던 점을 적어주세요<br><textarea class='sanitationreason' style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'></textarea>");
			taginside = true;
		} else if (sanitation == 10 && taginside == false) {
			$('#sanreason').empty();
			taginside = false;
		}
	});
	
	$('input[name="surroundings"]').on('click', function() {
		var taginside = false;
		var surroundings = $('input[name="surroundings"]:checked').val();
		if (surroundings != 10) {
			$('#surreason').html("불만족스러웠던 점을 적어주세요<br><textarea class='surroudingsreason' style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'></textarea>");
			taginside = true;
		} else if (surroundings == 10 && taginside == false) {
			$('#surreason').empty();
			taginside = false;
		}
	});
	
	//	
});
$(window).load(function() {
	//

}); //

function checkReview() {
	var convenience = $('input[name="convenience"]:checked').val();
	var accessibility = $('input[name="accessibility"]:checked').val();
	var kindness = $('input[name="kindness"]:checked').val();
	var sanitation = $('input[name="sanitation"]:checked').val();
	var surroundings = $('input[name="surroundings"]:checked').val();
	var total = $('input[name="total"]:checked').val();
	var convereason = $('.convereason').val();
	var accessreason = $('.accessibilityreason').val();
	var kindreason = $('.kindreason').val();
	var sanireason = $('.sanitationreason').val();
	var surroundreason = $('.surroudingsreason').val();
	var roomID = ${roomID};
	
	if(convenience == undefined) {
		alert('편의성 평가를 해주세요');
		return false;
	} else if (accessibility == undefined) {
		alert('접근성 평가를 해주세요');
		return false;
	} else if (kindness == undefined) {
		alert('친절함 평가를 해주세요');
		return false;
	} else if (sanitation == undefined) {
		alert('위생 평가를 해주세요');
		return false;
	} else if (surroundings == undefined) {
		alert('주변 환경 평가를 해주세요');
		return false;
	} else if (total == undefined) {
		alert('총평을 해주세요');
		return false;
	} else if (convenience != '10' && convereason == '') {
		alert('불만족스러웠던 점을 적어주세요');
		return false;
	} else if (accessibility != '10' && accessreason == '') {
		alert('불만족스러웠던 점을 적어주세요');
		return false;
	} else if (kindness != '10' && kindreason == '') {
		alert('불만족스러웠던 점을 적어주세요');
		return false;
	} else if (sanitation != '10' && sanireason == '') {
		alert('불만족스러웠던 점을 적어주세요');
		return false;
	} else if (surroundings != '10' && surroundreason == '') {
		alert('불만족스러웠던 점을 적어주세요');
		return false;
	} else {
		if (confirm('정말 등록하시겠습니까')) {
			
			$.ajax({
				url:'submitReview',
				type: 'POST',
				data: {roomID: roomID,
					   convenience: convenience,
					   convereason: convereason,
					   accessibility: accessibility,
					   accessreason: accessreason,
					   kindness: kindness,
					   kindreason: kindreason,
					   sanitation: sanitation,
					   sanireason: sanireason,
					   surroundings: surroundings,
					   surroundreason: surroundreason,
					   total: total},
				success: function() {
					   alert('등록 완료');
					   location.href='/npp/accoResult/accoView?accoNo=' + ${roomID};
				},
				error: function(e) {
					alert('서버와의 연결이 좋지 않습니다. 다시 시도하세요.');
				}
				});
			
			}
		}
	}
	
</script>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="/"/> class="logo"><img src="<c:url value = "/resources/images/NPP_logo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="index.html">home</a></li>
<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
	<ul>
		<li><a href="index-1.html">who we are</a></li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">find an agent<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="index-2.html">services</a></li>
<li><a href="index-3.html">SAles</a></li>
<li class="active"><a href="index-4.html">resources</a></li>											
<li><a href="index-5.html">gallery</a></li>											
<li><a href="index-6.html">contact us</a></li>											
    </ul>
		</div>
	</div>
</div>
<div id="search">
<!-- 맨 위 오른쪽 검색 부분 -->
<a href="#" class="searchBtn"></a>

<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>숙소 평가하기</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>숙소 평가</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			여러분들이 묵은 숙소를 평가해주세요. 숙소 평가는 총 여섯 가지의 분야로 나뉘어 있습니다.
		</div>
		<br>
		<div>
			<div>
			 	편의성<br>
			 	<input type="radio" name="convenience" value="1">
			 	&emsp;
			 	<input type="radio" name="convenience" value="2">
			 	&emsp;
			 	<input type="radio" name="convenience" value="3">
			 	&emsp;
			 	<input type="radio" name="convenience" value="4">
			 	&emsp;
			 	<input type="radio" name="convenience" value="5">
			 	&emsp;
			 	<input type="radio" name="convenience" value="6">
			 	&emsp;
			 	<input type="radio" name="convenience" value="7">
			 	&emsp;
			 	<input type="radio" name="convenience" value="8">
			 	&emsp;
			 	<input type="radio" name="convenience" value="9">
			 	&emsp;
			 	<input type="radio" name="convenience" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;
			 	10
		 	</div>
		 	<div id="convreason">
		 	</div>
		 	<div>
			 	접근성<br>
			 	<input type="radio" name="accessibility" value="1">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="2">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="3">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="4">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="5">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="6">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="7">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="8">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="9">
			 	&emsp;
			 	<input type="radio" name="accessibility" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="accessreason">
		 	</div>
		 	<div>
			 	친절함<br>
			 	<input type="radio" name="kindness" value="1">
			 	&emsp;
			 	<input type="radio" name="kindness" value="2">
			 	&emsp;
			 	<input type="radio" name="kindness" value="3">
			 	&emsp;
			 	<input type="radio" name="kindness" value="4">
			 	&emsp;
			 	<input type="radio" name="kindness" value="5">
			 	&emsp;
			 	<input type="radio" name="kindness" value="6">
			 	&emsp;
			 	<input type="radio" name="kindness" value="7">
			 	&emsp;
			 	<input type="radio" name="kindness" value="8">
			 	&emsp;
			 	<input type="radio" name="kindness" value="9">
			 	&emsp;
			 	<input type="radio" name="kindness" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="kinreason">
		 	</div>
		 	<div>
			 	위생<br>
			 	<input type="radio" name="sanitation" value="1">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="2">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="3">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="4">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="5">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="6">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="7">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="8">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="9">
			 	&emsp;
			 	<input type="radio" name="sanitation" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="sanreason">
		 	</div>
		 	<div>
			 	주위환경<br>
			 	<input type="radio" name="surroundings" value="1">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="2">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="3">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="4">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="5">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="6">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="7">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="8">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="9">
			 	&emsp;
			 	<input type="radio" name="surroundings" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="surreason">
		 	</div>
		 	<div>
			 	총점<br>
			 	<input type="radio" name="total" value="1">
			 	&emsp;
			 	<input type="radio" name="total" value="2">
			 	&emsp;
			 	<input type="radio" name="total" value="3">
			 	&emsp;
			 	<input type="radio" name="total" value="4">
			 	&emsp;
			 	<input type="radio" name="total" value="5">
			 	&emsp;
			 	<input type="radio" name="total" value="6">
			 	&emsp;
			 	<input type="radio" name="total" value="7">
			 	&emsp;
			 	<input type="radio" name="total" value="8">
			 	&emsp;
			 	<input type="radio" name="total" value="9">
			 	&emsp;
			 	<input type="radio" name="total" value="10">
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		</div>	
		
		<br>
		<div>
			<button class="btn" onclick="history.back">이전으로</button>
			&nbsp;
			<button class="btn" onclick="checkReview()">평가등록</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo2.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>