<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>침실 및 최대숙박인원</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script>
$(document).ready(function() {
	//

}); //
$(window).load(function() {
	//

}); //

function chkBedroom() {
	var maxGuest = $("#maxGuest option:selected").val();
	var maxBedroom = $("#maxBedroom option:selected").val();
	
	if (maxGuest == '0') {
		alert('최대숙박인원을 선택하세요');
		return false;
	} else if (maxBedroom == '0') {
		alert('게스트가 사용할 수 있는 침실의 수를 선택하세요');
		return false;
	} else {
		
		$.ajax({
			url: 'setaccommodation',
			type: 'POST',
			data: {accommodation: maxGuest},
			error: function(e) {
				alert('암튼 실패');
			}
		});
		
		$.ajax({
			url: 'setbedroom',
			type: 'POST',
			data: {bedroom: maxBedroom},
			error: function(e) {
				alert('암튼 실패');
			}
		});
		
		location.href = 'bathroom';
	}
}

<%-- 상단 메뉴의 돋보기 버튼 클릭시, accmmolist 로 이동 --%>
function moveAccmmolist() {
	var inputAddress = $('#inputAddress').val();
	location.href ="./../accmmolist?inputAddress="+inputAddress;
}
<%-- 서비스 조회 전 로그인 여부 확인--%>
function servicesCheck(loginId) {
	if (loginId != null) {
		location.href="/npp/mychart";
	} else {
		alert("로그인 후 이용 가능합니다.");
	}
}
</script>		

</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="./../"/> class="logo"><img src="<c:url value = "/resources/images/NPPlogo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="../">Home</a></li>
<li class="sub-menu sub-menu-1"><a href="#">About</a>
	<ul>
		<li><a href="<c:url value = "/VR_media" />">VR View</a></li>
		<li class="sub-menu sub-menu-2"><a href="<c:url value = "/Pano_media" />">Pano View<em></em></a></li>
		<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<c:if test="${loginId == null}">
<li><a href='javascript:void(0);' onclick="servicesCheck(${loginId})">services</a></li>
</c:if>
<c:if test="${loginId != null}">
<li><a href='javascript:void(0);' onclick="servicesCheck('${loginId}')">services</a></li>
</c:if>
<li><a href="<c:url value = "/accmmolist" />">Accommodations</a></li>
<c:if test="${loginId == null}">											
	<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
	<li class="active"><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>		
</c:if>
<c:if test="${loginId != null}">
	<li class="sub-menu sub-menu-1"><a href="<c:url value = "/member/userInfoForm" />">MY PAGE</a>
		<ul>
			<li><a href="<c:url value = "/member/userInfoForm" />">Account info</a></li>
			<li><a href="<c:url value = "/hostAcco/selectType" />">Start Hosting</a></li>
			<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
			<li><a href="<c:url value = "/member/logout" />">Logout</a></li>
		</ul>
	</li>
</c:if>		
    </ul>
		</div>
	</div>
</div>
<div id="search">
	
<a href="#" class="searchBtn"></a>

<!-- 상단 메뉴의 검색  -->
<div class="search-form-wrapper">
	<form id="search-form" action="../accmmolist" method="GET" accept-charset="utf-8" class="navbar-form clearfix" >
    <input class="searchPlaceHolder" id="inputAddress" type="text" name="inputAddress" placeholder='방문지를 입력해주세요.'>
   	<a onClick="moveAccmmolist()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>숙소 등록하기</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>침실 및 최대숙박인원</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			최대 숙박 가능 인원을 선택해주세요
		</div>
		<div>
			<select id="maxGuest" name="maxGuest">
				<option value="0">선택하세요</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
				<option value="11">11+</option>
			</select>
		</div>
		<br>
		<div class="caption">
			게스트가 사용 할 수 있는 침실의 수를 선택해주세요
		</div>
		<div>
			<select id="maxBedroom" name="maxBedroom">
				<option value="0">선택하세요</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
				<option value="11">11+</option>
			</select>
		</div>
		<br>
		<div>
			<button class="btn" onclick="location.href='selectType'">이전으로</button>
			&nbsp;
			<button class="btn" onclick="chkBedroom()">다음으로</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>