<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Register Panorama</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script type="text/javascript">
       $(document).ready(function(){
           var objDragAndDrop = $(".dragAndDropDiv");
           var rowCount=${picNum};
            
           $(document).on("dragenter",".dragAndDropDiv",function(e){
               e.stopPropagation();
               e.preventDefault();
               $(this).css('border', '2px solid #0B85A1');
           });
           $(document).on("dragover",".dragAndDropDiv",function(e){
               e.stopPropagation();
               e.preventDefault();
           });
           $(document).on("drop",".dragAndDropDiv",function(e){
                
               $(this).css('border', '2px dotted #0B85A1');
               e.preventDefault();
               var files = e.originalEvent.dataTransfer.files;
               
               if (files.length + rowCount > 3) {
               	alert('사진은 최대 3장까지만 등록 가능합니다');
               	return false;
               }
            
               handleFileUpload(files,objDragAndDrop);
           });
            
           $(document).on('dragenter', function (e){
               e.stopPropagation();
               e.preventDefault();
           });
           $(document).on('dragover', function (e){
             e.stopPropagation();
             e.preventDefault();
             objDragAndDrop.css('border', '2px dotted #0B85A1');
           });
           $(document).on('drop', function (e){
               e.stopPropagation();
               e.preventDefault();
           });
            
           
           function handleFileUpload(files,obj)
           {
           	
				if (rowCount > 3) {
					alert('사진은 최대 3장까지만 등록 가능합니다');
					return false;
				}
           	
              for (var i = 0; i < files.length; i++) 
              {
                   var fd = new FormData();
                   fd.append('file', files[i]);
             
                   var status = new createStatusbar(obj); //Using this we can set progress.
                   status.setFileNameSize(files[i].name,files[i].size);
                   sendFileToServer(fd,status);
             
              }
           }
            
           function createStatusbar(obj){
                    
               rowCount++;
               var row="odd";
               if(rowCount %2 == 0) row ="even";
               this.statusbar = $("<div class='statusbar " + row + "'></div>");
               this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
               this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
               this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
               this.abort = $("<div class='abort'>삭제</div>").appendTo(this.statusbar);
                
               obj.after(this.statusbar);
             
               this.setFileNameSize = function(name,size){
                   var sizeStr="";
                   var sizeKB = size/1024;
                   if(parseInt(sizeKB) > 1024){
                       var sizeMB = sizeKB/1024;
                       sizeStr = sizeMB.toFixed(2)+" MB";
                   }else{
                       sizeStr = sizeKB.toFixed(2)+" KB";
                   }
             
                   this.filename.html(name);
                   this.size.html(sizeStr);
                   $('.abort').attr("filename", name);
               }
                
               this.setProgress = function(progress){       
                   var progressBarWidth =progress*this.progressBar.width()/ 100;  
                   this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
               }
                
               this.setAbort = function(jqxhr){
                   var sb = this.statusbar;
                   var fileName = this.abort.attr('filename');
                   var roomID = $(".roomID").val();
                   this.abort.click(function()
                   {
                	   $.ajax({
                		   url: 'panofileDelete',
                		   type: "POST",
                		   data: {fileName: fileName}
                	   });
                       jqxhr.abort();
                       sb.hide();
                       rowCount--;
                   });
               }
               
           }
            
           function sendFileToServer(formData,status)
           {
               var uploadURL = "panofileUpload/post"; //Upload URL
               var roomID = $('.roomID').val();
               var extraData ={}; //Extra Data.
               var jqXHR=$.ajax({
                       xhr: function() {
                       var xhrobj = $.ajaxSettings.xhr();
                       if (xhrobj.upload) {
                               xhrobj.upload.addEventListener('progress', function(event) {
                                   var percent = 0;
                                   var position = event.loaded || event.position;
                                   var total = event.total;
                                   if (event.lengthComputable) {
                                       percent = Math.ceil(position / total * 100);
                                   }
                                   //Set progress
                                   status.setProgress(percent);
                               }, false);
                           }
                       return xhrobj;
                   },
                   url: uploadURL,
                   type: "POST",
                   contentType:false,
                   processData: false,
                   cache: false,
                   data: formData,
                   success: function(data){
                       status.setProgress(100);
             
                       //$("#status1").append("File upload Done<br>");           
                   }
               }); 
             
               status.setAbort(jqXHR);
           }
            
       });
       
       function insertPic() {
    		if (confirm('사진 등록을 끝내시겠습니까?')) {
    			location.href = '/npp/member/userInfoForm';
    		}
    	}
       
       <%-- 상단 메뉴의 돋보기 버튼 클릭시, accmmolist 로 이동 --%>
       function moveAccmmolist() {
       	var inputAddress = $('#inputAddress').val();
       	location.href ="./../accmmolist?inputAddress="+inputAddress;
       }

       <%-- 호스팅 신청 페이지로 이동 전 신청자인지 확인 --%>
       function hostingCheck(a, registRoomCheck) {
       	if (registRoomCheck == 0) {
       		if (a == 1) {
       			location.href = "../hostAcco/selectType"
       		} else {
       			alert("회원정보에서 호스팅을 신청해주세요.");
       		}
       	} else {
       		alert("이미 등록하신 숙소가 존재합니다.");
       	}
       }
</script>
<style>
    .dragAndDropDiv {
        border: 2px dashed #92AAB0;
        width: 650px;
        height: 200px;
        color: #92AAB0;
        text-align: center;
        vertical-align: middle;
        padding: 10px 0px 10px 10px;
        font-size:200%;
        display: table-cell;
    }
    .progressBar {
        width: 200px;
        height: 22px;
        border: 1px solid #ddd;
        border-radius: 5px; 
        overflow: hidden;
        display:inline-block;
        margin:0px 10px 5px 5px;
        vertical-align:top;
    }
      
    .progressBar div {
        height: 100%;
        color: #fff;
        text-align: right;
        line-height: 22px; /* same as #progressBar height if we want text middle aligned */
        width: 0;
        background-color: #0ba1b5; border-radius: 3px; 
    }
    .statusbar{
        border-top:1px solid #A9CCD1;
        min-height:25px;
        width:99%;
        padding:10px 10px 0px 10px;
        vertical-align:top;
    }
    .statusbar:nth-child(odd){
        background:#EBEFF0;
    }
    .filename{
        display:inline-block;
        vertical-align:top;
        width:250px;
    }
    .filesize{
        display:inline-block;
        vertical-align:top;
        color:#30693D;
        width:100px;
        margin-left:10px;
        margin-right:5px;
    }
    .abort{
        background-color:#A8352F;
        -moz-border-radius:4px;
        -webkit-border-radius:4px;
        border-radius:4px;display:inline-block;
        color:#fff;
        font-family:arial;font-size:13px;font-weight:normal;
        padding:4px 15px;
        cursor:pointer;
        vertical-align:top
    }
    .deletePic{
        background-color:#5DADE2;
        -moz-border-radius:4px;
        -webkit-border-radius:4px;
        border-radius:4px;display:inline-block;
        color:#fff;
        font-family:arial;font-size:13px;font-weight:normal;
        padding:4px 15px;
        cursor:pointer;
        vertical-align:top
    }
</style>
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="./../"/> class="logo"><img src="<c:url value = "/resources/images/NPPlogo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="./../">Home</a></li>
				<li class="sub-menu sub-menu-1"><a href="#">About</a>
			<ul>
				<li><a href="<c:url value = "/VR_media" />">VR View</a></li>
				<li class="sub-menu sub-menu-2"><a href="<c:url value = "/Pano_media" />">Pano View<em></em></a></li>
				<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>
				<li><a href="index-1.html">testimonials</a></li>
			</ul></li>
			<li><a href="<c:url value = "/mychart" />">services</a></li>
			<li><a href="<c:url value = "/accmmolist" />">Accommodations</a></li>
			<c:if test="${loginId == null}">
				<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
				<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
			</c:if>
			<c:if test="${loginId != null}">
					<li class="sub-menu sub-menu-1"><a href="<c:url value = "/member/userInfoForm" />">MY PAGE</a>
				<ul>
					<li><a href="<c:url value = "/member/userInfoForm" />">Accountinfo</a></li>
					<li><a href='javascript:void(0);' onclick="hostingCheck(${hosting}, ${registRoomCheck})">Start Hosting</a></li>
					<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
					<li><a href="<c:url value = "/member/logout" />">Logout</a></li>
				</ul></li>
			</c:if>
			</ul>
			</div>
		</div>
	</div>
	<div id="search">
		<a href="#" class="searchBtn"></a>
		<!-- 상단 메뉴의 검색  -->
		<div class="search-form-wrapper">
		<form id="search-form" action="../accmmolist" method="GET"	accept-charset="utf-8" class="navbar-form clearfix">
			<input class="searchPlaceHolder" id="inputAddress" type="text" name="inputAddress" placeholder='방문지를 입력해주세요.'> 
			<a href="#" onClick="moveAccmmolist()"></a>
		</form>
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>register panorama picture</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>파노라마 사진 첨부</span></h2>
<input type="hidden" value="${roomID }" name="roomID" />
<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			게스트들이 여러분들의 방에대해 알 수 있도록 사진을 넣어주세요. 사진은 최대 3장까지만 등록 가능합니다.
		</div>
		<br>
		<div>
			<div id="fileUpload" class="dragAndDropDiv">Drag & Drop Files Here</div>
		</div>
		<br>
		<div>
			<button class="btn" onclick="insertPic()">사진등록완료</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>
</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>