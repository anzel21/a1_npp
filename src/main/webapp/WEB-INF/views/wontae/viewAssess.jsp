<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>숙소 평가 자세히 보기</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script>
$(document).ready(function() {
	//
	
	//	
});
$(window).load(function() {
	//

}); //

</script>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="/"/> class="logo"><img src="<c:url value = "/resources/images/NPP_logo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="index.html">home</a></li>
<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
	<ul>
		<li><a href="index-1.html">who we are</a></li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">find an agent<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="index-2.html">services</a></li>
<li><a href="index-3.html">SAles</a></li>
<li class="active"><a href="index-4.html">resources</a></li>											
<li><a href="index-5.html">gallery</a></li>											
<li><a href="index-6.html">contact us</a></li>											
    </ul>
		</div>
	</div>
</div>
<div id="search">
<!-- 맨 위 오른쪽 검색 부분 -->
<a href="#" class="searchBtn"></a>

<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>숙소 평가 보기</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>숙소 평가 보기</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			이곳에서는 게스트의 평가를 볼 수 있습니다. 만점이 아닌경우, 게스트가 어디에서 불만족스러운 경험을 했는지도 볼 수 있습니다.
		</div>
		<br>
		<div>
			<div>
			 	편의성<br>
			 	<input type="radio" name="convenience" value="1" disabled="disabled" <c:if test="${assess.convenience == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="2" disabled="disabled" <c:if test="${assess.convenience == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="3" disabled="disabled" <c:if test="${assess.convenience == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="4" disabled="disabled" <c:if test="${assess.convenience == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="5" disabled="disabled" <c:if test="${assess.convenience == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="6" disabled="disabled" <c:if test="${assess.convenience == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="7" disabled="disabled" <c:if test="${assess.convenience == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="8" disabled="disabled" <c:if test="${assess.convenience == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="9" disabled="disabled" <c:if test="${assess.convenience == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="convenience" value="10" disabled="disabled" <c:if test="${assess.convenience == 10 }">checked</c:if> >
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;
			 	10
		 	</div>
		 	<div id="convreason">
		 		<textarea readonly="readonly" style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'>${assess.convereason }</textarea>
		 	</div>
		 	<div>
			 	접근성<br>
			 	<input type="radio" name="accessibility" value="1" disabled="disabled" <c:if test="${assess.accessibility == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="2" disabled="disabled" <c:if test="${assess.accessibility == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="3" disabled="disabled" <c:if test="${assess.accessibility == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="4" disabled="disabled" <c:if test="${assess.accessibility == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="5" disabled="disabled" <c:if test="${assess.accessibility == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="6" disabled="disabled" <c:if test="${assess.accessibility == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="7" disabled="disabled" <c:if test="${assess.accessibility == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="8" disabled="disabled" <c:if test="${assess.accessibility == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="9" disabled="disabled" <c:if test="${assess.accessibility == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="accessibility" value="10" disabled="disabled" <c:if test="${assess.accessibility == 10 }">checked</c:if> >
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="accessreason">
		 		<textarea readonly="readonly" style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'>
		 			${list.accessreason }
		 		</textarea>
		 	</div>
		 	<div>
			 	친절함<br>
			 	<input type="radio" name="kindness" value="1" disabled="disabled" <c:if test="${assess.kindness == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="2" disabled="disabled" <c:if test="${assess.kindness == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="3" disabled="disabled" <c:if test="${assess.kindness == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="4" disabled="disabled" <c:if test="${assess.kindness == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="5" disabled="disabled" <c:if test="${assess.kindness == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="6" disabled="disabled" <c:if test="${assess.kindness == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="7" disabled="disabled" <c:if test="${assess.kindness == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="8" disabled="disabled" <c:if test="${assess.kindness == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="9" disabled="disabled" <c:if test="${assess.kindness == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="kindness" value="10" disabled="disabled" <c:if test="${assess.kindness == 10 }">checked</c:if> >
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="kinreason">
		 		<textarea readonly="readonly" style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'>${assess.kindreason }
		 		</textarea>
		 	</div>
		 	<div>
			 	위생<br>
			 	<input type="radio" name="sanitation" value="1" disabled="disabled" <c:if test="${assess.sanitation == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="2" disabled="disabled" <c:if test="${assess.sanitation == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="3" disabled="disabled" <c:if test="${assess.sanitation == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="4" disabled="disabled" <c:if test="${assess.sanitation == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="5" disabled="disabled" <c:if test="${assess.sanitation == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="6" disabled="disabled" <c:if test="${assess.sanitation == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="7" disabled="disabled" <c:if test="${assess.sanitation == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="8" disabled="disabled" <c:if test="${assess.sanitation == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="9" disabled="disabled" <c:if test="${assess.sanitation == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="sanitation" value="10" disabled="disabled" <c:if test="${assess.sanitation == 10 }">checked</c:if> >
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="sanreason">
		 		<textarea readonly="readonly" style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'>${assess.sanireason }
		 		</textarea>
		 	</div>
		 	<div>
			 	주위환경<br>
			 	<input type="radio" name="surroundings" value="1" disabled="disabled" <c:if test="${assess.surroundings == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="2" disabled="disabled" <c:if test="${assess.surroundings == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="3" disabled="disabled" <c:if test="${assess.surroundings == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="4" disabled="disabled" <c:if test="${assess.surroundings == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="5" disabled="disabled" <c:if test="${assess.surroundings == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="6" disabled="disabled" <c:if test="${assess.surroundings == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="7" disabled="disabled" <c:if test="${assess.surroundings == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="8" disabled="disabled" <c:if test="${assess.surroundings == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="9" disabled="disabled" <c:if test="${assess.surroundings == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="surroundings" value="10" disabled="disabled" <c:if test="${assess.surroundings == 10 }">checked</c:if>>
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		 	<div id="surreason">
		 		<textarea readonly="readonly" style='margin: 0px 0px 10px; width: 879px; height: 176px; resize:none;'>
		 			${assess.surroundreason }
		 		</textarea>
		 	</div>
		 	<div>
			 	총점<br>
			 	<input type="radio" name="total" value="1" disabled="disabled" <c:if test="${assess.total == 1 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="2" disabled="disabled" <c:if test="${assess.total == 2 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="3" disabled="disabled" <c:if test="${assess.total == 3 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="4" disabled="disabled" <c:if test="${assess.total == 4 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="5" disabled="disabled" <c:if test="${assess.total == 5 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="6" disabled="disabled" <c:if test="${assess.total == 6 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="7" disabled="disabled" <c:if test="${assess.total == 7 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="8" disabled="disabled" <c:if test="${assess.total == 8 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="9" disabled="disabled" <c:if test="${assess.total == 9 }">checked</c:if> >
			 	&emsp;
			 	<input type="radio" name="total" value="10" disabled="disabled" <c:if test="${assess.total == 10 }">checked</c:if> >
			 	<br>
			 	1
			 	&emsp;&nbsp;&nbsp;
			 	2
			 	&emsp;&nbsp;&nbsp;
			 	3
			 	&emsp;&nbsp;&nbsp;
			 	4
			 	&emsp;&nbsp;&nbsp;
			 	5
			 	&emsp;&nbsp;&nbsp;
			 	6
			 	&emsp;&nbsp;&nbsp;
			 	7
			 	&emsp;&nbsp;&nbsp;
			 	8
			 	&emsp;&nbsp;&nbsp;
			 	9
			 	&emsp;&nbsp;
			 	10
		 	</div>
		</div>	
		
		<br>
		<div>
			<button class="btn" onclick="history.back();">이전으로</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo2.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>