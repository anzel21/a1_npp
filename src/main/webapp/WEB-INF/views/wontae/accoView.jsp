<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>숙소 상세설명</title>
<style type="text/css">
	@import url("./../resources/css/jw_main/login.css");
	@import url("./../resources/css/jw_main/join.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel='stylesheet' href=<c:url value = "/resources/kjw/fullcalendar/fullcalendar.css" /> />
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/cwt/starRating.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">
<script src=<c:url value = "/resources/kjw/lib/jquery-3.3.1.min.js" />></script>
<script src=<c:url value = "/resources/kjw/lib/moment.min.js" />></script>
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>
<style>
	.map_wrap, .map_wrap * {margin:0; padding:0;font-family:'Malgun Gothic',dotum,'돋움',sans-serif;font-size:12px;}
	.map_wrap {position:relative;width:100%;height:350px;}
	#category {position:absolute;top:10px;left:10px;border-radius: 5px; border:1px solid #909090;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);background: #fff;overflow: hidden;z-index: 2;}
	#category li {float:left;list-style: none;width:50px;px;border-right:1px solid #acacac;padding:6px 0;text-align: center; cursor: pointer;}
	#category li.on {background: #eee;}
	#category li:hover {background: #ffe6e6;border-left:1px solid #acacac;margin-left: -1px;}
	#category li:last-child{margin-right:0;border-right:0;}
	#category li span {display: block;margin:0 auto 3px;width:27px;height: 28px;}
	/* #category li .category_bg {background:url(http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/places_category.png) no-repeat;} */
	.bank {background-position: -10px 0;width: 25px;height: 25px;}
	.mart {background-position: -10px -36px;width: 25px;height: 25px;}
	.pharmacy {background-position: -10px -72px;width: 25px;height: 25px;}
	.gasbar {background-position: -10px -108px;width: 25px;height: 25px;}
	.cafe {background-position: -10px -144px;width: 25px;height: 25px;}
	.cvs {background-position: -10px -180px;width: 25px;height: 25px;}
	.restaurant {background-position: -10px -224px;width: 25px;height: 25px;}
	.metro {background-position: -10px -268px;width: 25px;height: 25px;}
	.hospital {background-position: -10px -312px;width: 25px;height: 25px;}
	.parking {background-position: -10px -356px;width: 25px;height: 25px;}
	.placeinfo_wrap {position:absolute;bottom:28px;left:-150px;width:300px;}
	.placeinfo {position:relative;width:100%;border-radius:6px;border: 1px solid #ccc;border-bottom:2px solid #ddd;padding-bottom: 10px;background: #fff;}
	.placeinfo:nth-of-type(n) {border:0; box-shadow:0px 1px 2px #888;}
	.placeinfo_wrap .after {content:'';position:relative;margin-left:-12px;left:50%;width:22px;height:12px;background:url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/vertex_white.png')}
	.placeinfo a, .placeinfo a:hover, .placeinfo a:active{color:#fff;text-decoration: none;}
	.placeinfo a, .placeinfo span {display: block;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
	.placeinfo span {margin:5px 5px 0 5px;cursor: default;font-size:13px;}
	.placeinfo .title {font-weight: bold; font-size:14px;border-radius: 6px 6px 0 0;margin: -1px -1px 0 -1px;padding:10px; color: #fff;background: #d95050;background: #d95050 url(http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/arrow_white.png) no-repeat right 14px center;}
	.placeinfo .tel {color:#0f7833;}
	.placeinfo .jibun {color:#999;font-size:11px;margin-top:0;}
	.btn {width:49%;}
</style>
<script>
$(document).ready(function() {
	//
	
	//
});
$(window).load(function() {
	//

}); //

function replyCheck() {
	var roomID = ${RoomInfo.roomID};
	var guestID = '${sessionScope.loginId}';
	var replyContent = $('.replyContent').val();
	
	if (replyContent == '') {
		alert('내용을 입력해주세요');
		return false;
	} else if (guestID == '') {
		alert('세션이 만료되었습니다. 다시 로그인하세요');
		return false;
	} else {
		if (confirm('정말 등록하시겠습니까?')) {
			$.ajax({
				url:'insertReply',
				type: 'POST',
				data:{roomID: roomID,
					  guestID: guestID,
					  replyContent: replyContent},
				success: function() {
					window.location.reload();
				}
			});
		}
	}
}

function deleteReply(replynum, roomID) {
	if (confirm('정말 삭제하시겠습니까?')) {
		$.ajax({
			url:'deleteReply',
			type: 'GET',
			data: {replynum: replynum, roomID: roomID},
			success: function(data) {
				window.location.reload();
			}
		});
	}
}

function toReview(roomID) {
	location.href='/npp/evaluation/starrank?roomID=' + roomID;
}

<%-- 호스팅 신청 페이지로 이동 전 신청자인지 확인 --%>
function hostingCheck(a, registRoomCheck) {
	if (registRoomCheck == 0) {
		if (a == 1) {
			location.href = "../hostAcco/selectType"
		} else {
			alert("회원정보에서 호스팅을 신청해주세요.");
		}
	} else {
		alert("이미 등록하신 숙소가 존재합니다.");
	}
}

<%-- 상단 메뉴의 돋보기 버튼 클릭시, accmmolist 로 이동 --%>
function moveAccmmolist() {
	var inputAddress = $('#inputAddress').val();
	location.href ="./../accmmolist?inputAddress="+inputAddress;
}

<%-- 서비스 조회 전 로그인 여부 확인--%>
function servicesCheck(loginId) {
	if (loginId != null) {
		location.href="/npp/mychart";
	} else {
		alert("로그인 후 이용 가능합니다.");
	}
}
</script>
<!-- join.js 에서 회원가입 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/join.js"></script>	

<!-- login.js 에서 login 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/login.js"></script>		
</head>

<body class="not-front">
<!-- JW_Login 화면 동작 -->
<%@ include file = "./../jw_login/loginForm.jsp" %>
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="/"/> class="logo"><img src="<c:url value = "/resources/images/NPPlogo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
				<li><a href="./../">Home</a></li>
				<li><a href="<c:url value = "/accmmolist" />">Accommodations</a></li>
				<c:if test="${loginId == null}">
					<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
					<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
				</c:if>
				<c:if test="${loginId != null}">
						<li class="sub-menu sub-menu-1"><a href="<c:url value = "/member/userInfoForm" />">MY PAGE</a>
					<ul>
						<li><a href="<c:url value = "/member/userInfoForm" />">Accountinfo</a></li>
						<li><a href='javascript:void(0);' onclick="hostingCheck(${hosting}, ${registRoomCheck})">Start Hosting</a></li>
						<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
						<li><a href="<c:url value = "/member/logout" />">Logout</a></li>
					</ul></li>
				</c:if>
			</ul>
			</div>
		</div>
	</div>
	<div id="search">
		<a href="#" class="searchBtn"></a>
		<!-- 상단 메뉴의 검색  -->
		<div class="search-form-wrapper">
		<form id="search-form" action="../accmmolist" method="GET"	accept-charset="utf-8" class="navbar-form clearfix">
			<input class="searchPlaceHolder" id="inputAddress" type="text" name="inputAddress" placeholder='방문지를 입력해주세요.'> 
			<a href="#" onClick="moveAccmmolist()"></a>
		</form>
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>${RoomInfo.roomName }</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>숙소 설명</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption" style="font-size: 16px;">
			${RoomInfo.roomExplain }
		</div>
	</div>
	<br>
	
<div class="row">
<c:forEach var="piclist" items="${piclist}">
	<div class="span3">
	<div class="thumb2 last">
		<div class="thumbnail clearfix">
			<figure class="img-polaroid"><img src="<c:url value="/resources/normalpicture/${piclist.picturePath}"/>" style="width: 100%; height: 200px;"></figure>		
		</div>
	</div>	
	</div>

	  
</c:forEach>
</div>	

	<div class="buttons">
		<button type="button" class="btn btn-primary" onclick="location.href='vrpic?roomID=${RoomInfo.roomID}'">VR View</button>
			&nbsp;&nbsp;&nbsp;
		<button type="button" class="btn btn-success" onclick="location.href='panopic?roomID=${RoomInfo.roomID}'">Panorama View</button>
	</div>
	
	<br>
	
	<div class="map_wrap">
    <div id="map" style="width:100%;height:100%;position:relative;overflow:hidden;"></div>
    <ul id="category">
        <li id="BK9" data-order="0"> 
            <span class="category_bg bank">
            	<img src=<c:url value="/resources/img/icon0.png" /> class="bank">
            </span>
            은행
        </li>       
        <li id="MT1" data-order="1"> 
            <span class="category_bg mart">
            	<img src=<c:url value="/resources/img/icon1.png" /> class="mart">
            </span>
            마트
        </li>  
        <li id="PM9" data-order="2"> 
            <span class="category_bg pharmacy">
            	<img src=<c:url value="/resources/img/icon2.png" /> class="pharmacy">
            </span>
            약국
        </li>  
        <li id="OL7" data-order="3"> 
            <span class="category_bg oil">
            	<img src=<c:url value="/resources/img/icon3.png" /> class="gasbar">
            </span>
            주유소
        </li>  
        <li id="CE7" data-order="4"> 
            <span class="category_bg cafe">
            	<img src=<c:url value="/resources/img/icon4.png" /> class="cafe">
            </span>
            카페
        </li>  
        <li id="CS2" data-order="5"> 
            <span class="category_bg store">
            	<img src=<c:url value="/resources/img/icon5.png" /> class="cvs">
            </span>
            편의점
        </li>  
        <li id="FD6" data-order="6"> 
            <span class="category_bg restaurant">
            	<img src=<c:url value="/resources/img/icon6.png" /> class="restaurant">
            </span>
            음식점
        </li>
        <li id="SW8" data-order="7"> 
            <span class="category_bg subway">
            	<img src=<c:url value="/resources/img/icon7.png" /> class="metro">
            </span>
            지하철역
        </li>
        <li id="HP8" data-order="8"> 
            <span class="category_bg clinic">
            	<img src=<c:url value="/resources/img/icon8.png" /> class="hospital">
            </span>
            병원
        </li>
        <li id="PK6" data-order="9"> 
            <span class="category_bg parking">
            	<img src=<c:url value="/resources/img/icon9.png" /> class="parking">
            </span>
            주차장
        </li>      
    </ul>
</div>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8658714a2fcbb6223cdc4988e317c0cb&libraries=services"></script>
<script>

// 마커를 클릭했을 때 해당 장소의 상세정보를 보여줄 커스텀오버레이입니다
var placeOverlay = new daum.maps.CustomOverlay({zIndex:1}), 
    contentNode = document.createElement('div'), // 커스텀 오버레이의 컨텐츠 엘리먼트 입니다 
    markers = [], // 마커를 담을 배열입니다
    currCategory = ''; // 현재 선택된 카테고리를 가지고 있을 변수입니다
 
var mapContainer = document.getElementById('map'); // 지도를 표시할 div 
	mapContainer.style.width = '870px';
    mapContainer.style.height = '550px';
    mapOption = {
        center: new daum.maps.LatLng("${RoomInfo.latitude}", "${RoomInfo.longitude}"), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  

// 지도를 생성합니다    
var map = new daum.maps.Map(mapContainer, mapOption); 

// 장소 검색 객체를 생성합니다
var ps = new daum.maps.services.Places(map); 

// 마커를 표시할 곳 (임시로 코엑스 좌표 넣음, 제품판에서는 부동산 매물이 있는 곳)
var markerPosition = new daum.maps.LatLng("${RoomInfo.latitude}", "${RoomInfo.longitude}");

// 마커 생성
var marker = new daum.maps.Marker({
	position:markerPosition
});

// 마커 표시
marker.setMap(map);

// 지도 타입 변경 컨트롤을 생성한다
var mapTypeControl = new daum.maps.MapTypeControl();

// 지도의 상단 우측에 지도 타입 변경 컨트롤을 추가한다
map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);	

// 지도에 확대 축소 컨트롤을 생성한다
var zoomControl = new daum.maps.ZoomControl();

// 지도의 우측에 확대 축소 컨트롤을 추가한다
map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);

// 지도에 idle 이벤트를 등록합니다
daum.maps.event.addListener(map, 'idle', searchPlaces);

// 커스텀 오버레이의 컨텐츠 노드에 css class를 추가합니다 
contentNode.className = 'placeinfo_wrap';

// 커스텀 오버레이의 컨텐츠 노드에 mousedown, touchstart 이벤트가 발생했을때
// 지도 객체에 이벤트가 전달되지 않도록 이벤트 핸들러로 daum.maps.event.preventMap 메소드를 등록합니다 
addEventHandle(contentNode, 'mousedown', daum.maps.event.preventMap);
addEventHandle(contentNode, 'touchstart', daum.maps.event.preventMap);

// 커스텀 오버레이 컨텐츠를 설정합니다
placeOverlay.setContent(contentNode);  

// 각 카테고리에 클릭 이벤트를 등록합니다
addCategoryClickEvent();

// 엘리먼트에 이벤트 핸들러를 등록하는 함수입니다
function addEventHandle(target, type, callback) {
    if (target.addEventListener) {
        target.addEventListener(type, callback);
    } else {
        target.attachEvent('on' + type, callback);
    }
}

// 카테고리 검색을 요청하는 함수입니다
function searchPlaces() {
    if (!currCategory) {
        return;
    }
    
    // 커스텀 오버레이를 숨깁니다 
    placeOverlay.setMap(null);

    // 지도에 표시되고 있는 마커를 제거합니다
    removeMarker();
    
    ps.categorySearch(currCategory, placesSearchCB, {useMapBounds:true}); 
}

// 장소검색이 완료됐을 때 호출되는 콜백함수 입니다
function placesSearchCB(data, status, pagination) {
    if (status === daum.maps.services.Status.OK) {
        // 정상적으로 검색이 완료됐으면 지도에 마커를 표출합니다
        
        displayPlaces(data);
    } else if (status === daum.maps.services.Status.ZERO_RESULT) {
        // 검색결과가 없는경우 해야할 처리가 있다면 이곳에 작성해 주세요
        

    } else if (status === daum.maps.services.Status.ERROR) {
        // 에러로 인해 검색결과가 나오지 않은 경우 해야할 처리가 있다면 이곳에 작성해 주세요
        
    }
}

// 지도에 마커를 표출하는 함수입니다
function displayPlaces(places) {

    // 몇번째 카테고리가 선택되어 있는지 얻어옵니다
    // 이 순서는 스프라이트 이미지에서의 위치를 계산하는데 사용됩니다
    var order = document.getElementById(currCategory).getAttribute('data-order');

    

    for ( var i=0; i<places.length; i++ ) {

            // 마커를 생성하고 지도에 표시합니다
            var marker = addMarker(new daum.maps.LatLng(places[i].y, places[i].x), order);

            // 마커와 검색결과 항목을 클릭 했을 때
            // 장소정보를 표출하도록 클릭 이벤트를 등록합니다
            (function(marker, place) {
                daum.maps.event.addListener(marker, 'click', function() {
                    displayPlaceInfo(place);
                });
            })(marker, places[i]);
    }
}

// 마커를 생성하고 지도 위에 마커를 표시하는 함수입니다
function addMarker(position, order) {
    var imageSrc = '../resources/img/icon' + order + '.png', // 마커 이미지 url, 스프라이트 이미지를 씁니다
    
        imageSize = new daum.maps.Size(23, 23),  // 마커 이미지의 크기
        imgOptions =  {
            offset: new daum.maps.Point(11, 28) // 마커 좌표에 일치시킬 이미지 내에서의 좌표
        },
        markerImage = new daum.maps.MarkerImage(imageSrc, imageSize, imgOptions),
            marker = new daum.maps.Marker({
            position: position, // 마커의 위치
            image: markerImage 
        });

    marker.setMap(map); // 지도 위에 마커를 표출합니다
    markers.push(marker);  // 배열에 생성된 마커를 추가합니다

    return marker;
}

// 지도 위에 표시되고 있는 마커를 모두 제거합니다
function removeMarker() {
    for ( var i = 0; i < markers.length; i++ ) {
        markers[i].setMap(null);
    }   
    markers = [];
}

// 클릭한 마커에 대한 장소 상세정보를 커스텀 오버레이로 표시하는 함수입니다
function displayPlaceInfo (place) {
    var content = '<div class="placeinfo">' +
                    '   <a class="title" href="' + place.place_url + '" target="_blank" title="' + place.place_name + '">' + place.place_name + '</a>';   

    if (place.road_address_name) {
        content += '    <span title="' + place.road_address_name + '">' + place.road_address_name + '</span>' +
                    '  <span class="jibun" title="' + place.address_name + '">(지번 : ' + place.address_name + ')</span>';
    }  else {
        content += '    <span title="' + place.address_name + '">' + place.address_name + '</span>';
    }                
   
    content += '    <span class="tel">' + place.phone + '</span>' + 
                '</div>' + 
                '<div class="after"></div>';

    contentNode.innerHTML = content;
    placeOverlay.setPosition(new daum.maps.LatLng(place.y, place.x));
    placeOverlay.setMap(map);  
}


// 각 카테고리에 클릭 이벤트를 등록합니다
function addCategoryClickEvent() {
    var category = document.getElementById('category'),
        children = category.children;
    
    

    for (var i=0; i<children.length; i++) {
        children[i].onclick = onClickCategory;
    }
}

// 카테고리를 클릭했을 때 호출되는 함수입니다
function onClickCategory() {
    var id = this.id,
        className = this.className;

    placeOverlay.setMap(null);

    if (className === 'on') {
        currCategory = '';
        changeCategoryClass();
        removeMarker();
    } else {
        currCategory = id;
        changeCategoryClass(this);
        searchPlaces();
    }
}

// 클릭된 카테고리에만 클릭된 스타일을 적용하는 함수입니다
function changeCategoryClass(el) {
    var category = document.getElementById('category'),
        children = category.children,
        i;

    for ( i=0; i<children.length; i++ ) {
        children[i].className = '';
    }

    if (el) {
        el.className = 'on';
    } 
}

function reset() {
	var moveLatLng = new daum.maps.LatLng("${RoomInfo.latitude}", "${RoomInfo.longitude}");
	map.panTo(moveLatLng);
}

</script>
	
</div>

<br><br><br><br><br><br><br><br><br><br>



<h2><span>숙소 리뷰</span></h2>

	<div class="thumb1">
		<div class="caption">
				편의성
				<span class="starRating">
					<span style="width:${assess.convenience}%">
					</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				접근성
				<span class="starRating">
					<span style="width:${assess.accessibility}%">
					</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				친절함
				<span class="starRating">
					<span style="width:${assess.kindness}%">
					</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				위생
				<span class="starRating">
					<span style="width:${assess.sanitation}%">
					</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				주위 환경
				<span class="starRating">
					<span style="width:${assess.surroundings}%">
					</span>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;
				종합
				<span class="starRating">
					<span style="width:${assess.total}%">
					</span>
				</span>
				<br>
				<input type="button" class="btn" onclick='toReview(${RoomInfo.roomID})' value="리뷰 작성" >
		</div>
		<br>
		<div class="caption">
			<textarea class="replyContent" style="resize:none;height:100%;width:85%;"></textarea>&nbsp;<input type="button" class="btn" onclick='replyCheck()' value="리플 작성">
		</div>
		<div>
			<table class="table">
					<tr>
						<td><b>아이디</b></td>
						<td><b>내용</b></td>
						<td><b>입력일</b></td>
					</tr>
			<c:forEach var="reply" items="${reply }">		
					<tr>
						<td>${reply.guestID }</td>
						<td>${reply.replyContent }</td>
						<td>${reply.inputDate }</td>
						<c:if test="${sessionScope.loginId == reply.guestID }">
						<td><button class="btn" style="width:50pt;" onclick="deleteReply(${data[i].replynum}, ${RoomInfo.roomID})">삭제</button></td></c:if>
					</tr>
			</c:forEach>
			</table>
		</div>
	</div>

</div>
<div class="span3">
	
<h2><span>숙소 예약</span></h2>

<p>
	<b>
	<div
	style="background-color:white; text-align: left; width: 340px; height: 420px; text-align: center; border: solid, black, 1px;">
		<%@include file="../kjw/NPP_RS_select.jsp"%>
		</div>
		</b>
		</p>
		<p></p>

	<br>
	<br>
							

</div>	
</div>


</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
<footer><div class="copyright">Copyright © 2018.<br>NPP FINAL PROJECT</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">NPP FINAL PROJCET IS</div>

<p style="font-size: 12px;">
	Made By<br>
	<br>
	Coding Slaves Team(C.S Team)<br>
	KYU SOO KIM(LEADER)<br>
	JUN WOO KIM<br>
	JIN WON SEO<br>
	WON TAE CHO<br>
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>