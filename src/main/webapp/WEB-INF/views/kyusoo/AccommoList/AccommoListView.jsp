<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Accommodations</title>
<style type="text/css">
	@import url("./resources/css/jw_main/login.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="resources/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="resources/css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="resources/css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen">
<!-- 네이버 로그인 연동 관련 JS 파일 -->
<script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.2.js" charset="utf-8"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

<script type="text/javascript" src="resources/js/jquery.js"></script>  
<script type="text/javascript" src="resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="resources/js/superfish.js"></script>

<script type="text/javascript" src="resources/js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="resources/js/script.js"></script>
<script>
//JW_Login 화면 동작 기능
function openLoginPage() {
	document.getElementById('LoginDiv').style.display = 'block';
	$('#LoginDiv').css('height','460px');
	$('#LoginDiv').css('margin-left','-340px');
	$('#LoginDiv').css('margin-top','-60px');
	document.getElementById('LoginDivOverlay').style.display = 'block';
	// JW_마우스 스크롤 비활성화
	$(document)
			.on(
					"mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll",
					function(e) {
						e.preventDefault();
						return;
					});
	$(document).on("keydown.disableScroll", function(e) {
		var eventKeyArray = [ 32, 33, 34, 35, 36, 37, 38, 39, 40 ];
		for (var i = 0; i < eventKeyArray.length; i++) {
			if (e.keyCode === eventKeyArray[i]) {
				e.preventDefault();
				return;
			}
		}
	});
}
// JW_Login 화면 종료 기능
function loginOverlay() {
	document.getElementById('LoginDiv').style.display = 'none';
	document.getElementById('LoginDivOverlay').style.display = 'none';
	// JW_마우스 스크롤 활성화
	$(document).off(".disableScroll");
}

//회원가입 폼으로 이동
function goJoinForm() {
	location.href = "member/joinForm";
}

//id & pw 조회 폼으로 이동
function goFindIdPw() {
	location.href = "member/id_pwFindForm";
}

//로그인시 입력받는 데이터 Ajax & 유효성검사
function loginData() {
	var msg;
	$.ajax({
		url: 'member/loginData',
		type: 'POST',
		data: $('#loginData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "아이디를 입력해주세요." || msg == "없는 아이디 입니다.") {
				$('#login_idErrDiv').html(msg);
				$('#login_pwErrDiv').html("");
				return false;
			}
			if(msg == "비밀번호를 입력해주세요." || msg == "비밀번호가 일치하지 않습니다.") {
				$('#login_idErrDiv').html("");
				$('#login_pwErrDiv').html(msg);
				return false;
			}
			else {
				location.href ="./";
			}
		},
		error: function (e) {
			alert('저장 실패');
		}
	});
}
<%-- 호스팅 신청 페이지로 이동 전 신청자인지 확인 --%>
function hostingCheck(a, registRoomCheck) {
	if (registRoomCheck == 0) {
		if (a == 1) {
			location.href = "./hostAcco/selectType"
		} else {
			alert("회원정보에서 호스팅을 신청해주세요.");
		}
	} else {
		alert("이미 등록하신 숙소가 존재합니다.");
	}
}
<%-- 상단 메뉴의 돋보기 버튼 클릭시, accmmolist 로 이동 --%>
function moveAccmmolist() {
	var inputAddress = $('#inputAddress').val();
	location.href ="./accmmolist?inputAddress="+inputAddress;
}
<%-- 서비스 조회 전 로그인 여부 확인--%>
function servicesCheck(loginId) {
	if (loginId != null) {
		location.href="/npp/mychart";
	} else {
		alert("로그인 후 이용 가능합니다.");
	}
}
</script>

</head>

<body class="not-front">
<!-- JW_Login 화면 동작 -->
<%@ include file = "../../jw_login/loginForm.jsp" %>
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
	
<header><div class="logo_wrapper"><a href="./" class="logo"><img src="resources/images/NPPlogo.png" alt=""></a></div></header>

<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="./">Home</a></li>
				<%-- <li class="sub-menu sub-menu-1"><a href="#">About</a>
			<ul>
				<li><a href="<c:url value = "/VR_media" />">VR View</a></li>
				<li class="sub-menu sub-menu-2"><a href="<c:url value = "/Pano_media" />">Pano View<em></em></a></li>
				<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>
				<li><a href="index-1.html">testimonials</a></li>
			</ul></li>
			<c:if test="${loginId == null}">
				<li><a href='javascript:void(0);' onclick="servicesCheck(${loginId})">services</a></li>
			</c:if>
			<c:if test="${loginId != null}">
				<li><a href='javascript:void(0);' onclick="servicesCheck('${loginId}')">services</a></li>
			</c:if> --%>
			<li class="active"><a href="<c:url value = "/accmmolist" />">Accommodations</a></li>
			<c:if test="${loginId == null}">
				<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
				<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
			</c:if>
			<c:if test="${loginId != null}">
					<li class="sub-menu sub-menu-1"><a href="<c:url value = "/member/userInfoForm" />">MY PAGE</a>
				<ul>
					<li><a href="<c:url value = "/member/userInfoForm" />">Accountinfo</a></li>
					<li><a href='javascript:void(0);' onclick="hostingCheck(${hosting}, ${registRoomCheck})">Start Hosting</a></li>
					<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
					<li><a href="<c:url value = "/member/logout" />">Logout</a></li>
				</ul></li>
			</c:if>
			</ul>
			</div>
		</div>
	</div>
	<div id="search">
		<a href="#" class="searchBtn"></a>
		<!-- 상단 메뉴의 검색  -->
		<div class="search-form-wrapper">
		<form id="search-form" action="./accmmolist" method="GET"	accept-charset="utf-8" class="navbar-form clearfix">
			<input class="searchPlaceHolder" id="inputAddress" type="text" name="inputAddress" placeholder='방문지를 입력해주세요.'> 
			<a href="#" onClick="moveAccmmolist()"></a>
		</form>
</div>

</div>	
</div>

</div>	
</div>

<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src="resources/images/bg1.jpg" alt="" class="img"></div>
<div class="top2_inner">
<div class="container">
<div class="top2 clearfix">
	
<h1>Accommodations</h1>

</div>
</div>	
</div>
</div>



<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<h2><span>숙소 리스트</span></h2>
<div class="row">
<%-- <c:forEach var="piclist" items="${PicturePath}">
<figure><img src="resources/normalpicture/${PicturePath}" alt="" class="img"></figure>
</c:forEach> --%>


<c:forEach var="aclist" items="${Accolist}" varStatus="loop">
<div class="span3">
<div class="thumb3">
	<div class="thumbnail clearfix">
	<c:forEach var="piclist" items="${PicturePath}" varStatus="pic">
	<c:if test="${loop.index == pic.index}">
	
		<figure><img src="<c:url value="/resources/normalpicture/${piclist}"/>" class="img"></figure>
	</c:if>
	</c:forEach>
		<div class="caption">
			<div class="txt1">${aclist.roomName}</div>
			<div class="txt2">${aclist.location}</div>
			<div class="txt3">₩${aclist.price}</div>
			<a href="accoResult/accoView?accoNo=${aclist.roomID}" class="button2">Read More</a>
		</div>	
	</div>
</div>	
</div>
</c:forEach>
</div>


</div>	
</div>


</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="./" class="logo2"><img src="resources/images/NPPlogo.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2018.<br>NPP FINAL PROJECT</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">NPP FINAL PROJCET IS</div>

<p style="font-size: 12px;">
	Made By<br>
	<br>
	Coding Slaves Team(C.S Team)<br>
	KYU SOO KIM(LEADER)<br>
	JUN WOO KIM<br>
	JIN WON SEO<br>
	WON TAE CHO<br>
</p>

</div>
<div class="span3 offset1">

<div class="bot1_title">Follow Us</div>
	
<div class="social_wrapper">
	<ul class="social clearfix">    
    <li><a href="#"><img src="resources/images/social_ic1.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic2.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic3.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic4.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic5.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic6.png"></a></li>
	</ul>
</div>

</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="resources/js/bootstrap.js"></script>
</body>
</html>

