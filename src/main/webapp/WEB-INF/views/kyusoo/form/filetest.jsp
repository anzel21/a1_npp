<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>

<head>
	<title>Request A website Flat Responsive widget Template :: w3layouts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Request A website Responsive, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"
	/>
	
	<script src="./resources/js/jquery-3.2.1.min.js"></script>s
	
	<!-- <script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script> -->
	
	
	<script>    
    
    $(document).ready(function(){    	
    	
    		$("#industry").on("change", function() {
    			
    			var sel = $("#industry").val();
    			
    			
    			if(sel == 'it')
    			{
    				var str = '<select required="" name="company">'
					+ '<option value="">관심기업</option>'
					+ '<option value="samsung">Samsung</option>'
					+ '<option value="apple">apple</option>'
					+ '<option value="google">Google</option>'
					+ '</select>';
    	    			
    	    		$('#company').html(str);
    			}
    			else if(sel == 'management')
    			{
    				var str = '<select required="" name="company">'
					+ '<option value="">관심기업</option>'
					+ '<option value="manage1">경영1</option>'
					+ '<option value="manage2">경영2</option>'
					+ '<option value="manage3">경영3</option>'
					+ '</select>';
    	    			
    	    		$('#company').html(str);
    			}
    			else if(sel == 'financial')
    			{
    				var str = '<select required="" name="company">'
					+ '<option value="">관심기업</option>'
					+ '<option value="shinhan">신한은행</option>'
					+ '<option value="kookmin">국민은행</option>'
					+ '<option value="woori">우리은행</option>'
					+ '</select>';
    	    			
    	    		$('#company').html(str);
    			}
    			else if(sel == 'media')
    			{
    				var str = '<select required="" name="company">'
					+ '<option value="">관심기업</option>'
					+ '<option value="bloomberg">Bloomberg</option>'
					+ '<option value="kbs">KBS</option>'
					+ '<option value="mbc">MBC</option>'
					+ '<option value="sbs">SBS</option>'
					+ '</select>';
    	    			
    	    		$('#company').html(str);
    			}
    			else
    			{
    				var str = '<select required="" name="company">'
    				+ '<option value="">관심 분야를 먼저 선택해주세요</option>'
					+ '</select>';
					
    				$('#company').html(str);
    			}    			
    
    		});

    	
    });
	
	</script>
	
	<!-- 이미지 미리보기 script -->
	<script>
	function previewImage(targetObj, View_area) {
		var preview = document.getElementById(View_area); //div id
		var ua = window.navigator.userAgent;

	  //ie일때(IE8 이하에서만 작동)
		if (ua.indexOf("MSIE") > -1) {
			targetObj.select();
			try {
				var src = document.selection.createRange().text; // get file full path(IE9, IE10에서 사용 불가)
				var ie_preview_error = document.getElementById("ie_preview_error_" + View_area);


				if (ie_preview_error) {
					preview.removeChild(ie_preview_error); //error가 있으면 delete
				}

				var img = document.getElementById(View_area); //이미지가 뿌려질 곳

				//이미지 로딩, sizingMethod는 div에 맞춰서 사이즈를 자동조절 하는 역할
				img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+src+"', sizingMethod='scale')";
			} catch (e) {
				if (!document.getElementById("ie_preview_error_" + View_area)) {
					var info = document.createElement("<p>");
					info.id = "ie_preview_error_" + View_area;
					info.innerHTML = e.name;
					preview.insertBefore(info, null);
				}
			}
	  //ie가 아닐때(크롬, 사파리, FF)
		} else {
			var files = targetObj.files;
			for ( var i = 0; i < files.length; i++) {
				var file = files[i];
				var imageType = /image.*/; //이미지 파일일경우만.. 뿌려준다.
				if (!file.type.match(imageType))
					continue;
				var prevImg = document.getElementById("prev_" + View_area); //이전에 미리보기가 있다면 삭제
				if (prevImg) {
					preview.removeChild(prevImg);
				}
				var img = document.createElement("img"); 
				img.id = "prev_" + View_area;
				img.classList.add("obj");
				img.file = file;
				img.style.width = '200px'; 
				img.style.height = '200px';
				preview.appendChild(img);
				if (window.FileReader) { // FireFox, Chrome, Opera 확인.
					var reader = new FileReader();
					reader.onloadend = (function(aImg) {
						return function(e) {
							aImg.src = e.target.result;
						};
					})(img);
					reader.readAsDataURL(file);
				} else { // safari is not supported FileReader
					//alert('not supported FileReader');
					if (!document.getElementById("sfr_preview_error_"
							+ View_area)) {
						var info = document.createElement("p");
						info.id = "sfr_preview_error_" + View_area;
						info.innerHTML = "not supported FileReader";
						preview.insertBefore(info, null);
					}
				}
			}
		}
	}
	
</script>
	
	
	
	<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
	<!-- /fonts -->
	<!-- css -->
	<link href="./resources/recruitform/css/style.css" rel="stylesheet" type='text/css' media="all" />
	<!-- /css -->
</head>

<body>
	<div class="banner-layer">
		<h1 class="w3layouts">Applicant with Ragna</h1>
		<div class="content-w3ls">
			<div class="form_w3layouts">
				<c:if test="${User.usertype eq 'applicant'}">
					<!-- form starts here -->
				<form action="writeresume" method="post" class="agile_form" enctype="multipart/form-data">
				<input type="hidden" name="id" value="${User.id}">
					<fieldset>
						<legend>fill out the form</legend>
						<div class="agileits-left">
							<input type="text" name="name" class="username" placeholder="${sessionScope.user.name}" required="" value="${User.name}" readonly="readonly">
						</div>
						<div class="s2-w3ls">
							<input type="text" class="username" placeholder="Usertype" required="" value="${User.usertype}" readonly="readonly">
						</div>
						<div class="agileits-left">
							<input type="text" name="email" class="username" placeholder="example@email.com" value="${User.email}" readonly="readonly">
						</div>
						<div class="agileits-left s2-w3ls">
							<input type="text" name="phone" class="username" placeholder="Phone number" >
						</div>
						
						<div class="clear"></div>
						<div class="section_class_agileits">
							<select required name="industry" id="industry">
								<option value="">관심분야</option>
								<option value="it">IT</option>
								<option value="management">Management</option>
								<option value="financial">Financial</option>
								<option value="media">Media</option>
							</select>
						</div>
						
						<div class="section_class_agileits s2-w3ls" id="company">
							<select required="" name="company">
								<option value="">관심 분야를 먼저 선택해주세요</option>
							</select>
						</div>						
						
						
						<div class="clear"></div>
						<div class="section_class_agileits">
							<select required name="state">
								<option value="">구직상태</option>
								<option value="ready">구직준비중</option>
								<option value="recruiting">구직중</option>
								<option value="worker">재직중</option>
							</select>
						</div>
						
						
						<div class="section_class_agileits s2-w3ls">
							<select required="" name="gender">
								<option value="">성별</option>
								<option value="male">남</option>
								<option value="female">여</option>
							</select>
						</div>								
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>제목</h2>
						</div>						
						<input type="text" class="username" name="title" placeholder="제목" required="">
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>자기소개</h2>
						</div>						
						<textarea placeholder="자기소개" name="introduce" cols="1000" rows="1000"></textarea>						
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>경력사항</h2>
						</div>						
						<textarea placeholder="경력사항" name="career" cols="1000" rows="1000"></textarea>
						
						<div class="clear"></div>
						<div class="submit">
							<input type="file" name="upload" id="profile_pt" onchange="previewImage(this,'View_area')">
						</div>
						
						<div id='View_area' style='position:relative; width: 200px; height: 200px; color: black; border: 0px solid black; dispaly: inline; '></div>
						
						<div class="clear"></div>
						<div class="submit">
							<input type="submit" value="Submit">
						</div>
					</fieldset>
				</form>
				<!-- //form ends here -->
				</c:if>
											
				
				<c:if test="${User.usertype eq 'company'}">
					<form action="writerecruit" method="post" class="agile_form">
				<input type="hidden" name="id" value="${User.id}">
					<fieldset>
						<legend>fill out the form</legend>
						<div class="agileits-left">
							<input type="text" name="name" class="username" placeholder="${sessionScope.user.name}" required="" value="${User.name}" readonly="readonly">
						</div>
						<div class="s2-w3ls">
							<input type="text" class="username" placeholder="Usertype" required="" value="${User.usertype}" readonly="readonly">
						</div>
						<div class="agileits-left">
							<input type="text" name="email" class="username" placeholder="example@email.com" value="${User.email}" readonly="readonly">
						</div>
						<div class="agileits-left s2-w3ls">
							<input type="text" name="phone" class="username" placeholder="Phone number" >
						</div>
						
						<div class="clear"></div>
						<div class="section_class_agileits">
							<select required name="company_type">
								<option value="">사업분야</option>
								<option value="it">IT</option>
								<option value="management">Management</option>
								<option value="financial">Financial</option>
								<option value="media">Media</option>
							</select>
						</div>
						
						<div class="section_class_agileits s2-w3ls">
							<select required name="recruit_type">
								<option value="">구직자 형태</option>
								<option value="ready">구직준비중</option>
								<option value="recruiting">구직중</option>
								<option value="worker">재직중</option>
							</select>
						</div>						
						
						
						<!-- <div class="clear"></div>
						<div class="section_class_agileits">
							<select required name="state">
								<option value="">구직자 형태</option>
								<option value="ready">구직준비중</option>
								<option value="recruiting">구직중</option>
								<option value="worker">재직중</option>
							</select>
						</div>
						
						
						<div class="section_class_agileits s2-w3ls">
							<select required="" name="gender">
								<option value="">성별</option>
								<option value="male">남</option>
								<option value="female">여</option>
							</select>
						</div> -->		
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>제목</h2>
						</div>						
						<input type="text" class="username" name="title" placeholder="제목" required="">
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>업무소개 & 요구사항</h2>
						</div>						
						<textarea placeholder="업무소개 & 요구사항" name="introduce" cols="1000" rows="1000"></textarea>						
						
						
						<div class="clear"></div>
						<div class="agileits-left check">
							<h2>우대사항</h2>
						</div>						
						<textarea placeholder="우대사항" name="preference" cols="1000" rows="1000"></textarea>
						
						
						<div class="clear"></div>
						<div class="submit">
							<input type="submit" value="Submit">
						</div>
					</fieldset>
				</form>
				<!-- //form ends here -->
				
				
				
				
				</c:if>
				
				
				
			</div>
		</div>
		<p class="copyright-agileits">Â© 2018 Request A Website. All Rights Reserved | Design by
			<a href="https://w3layouts.com/" target="_blank">w3layouts</a>
		</p>
	</div>
</body>

</html>