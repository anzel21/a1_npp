<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>VR gallery</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="resources/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="resources/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="resources/css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="resources/css/bootstrap-responsive.css" type="text/css" media="screen">   
<link rel="stylesheet" href="resources/css/touchTouch.css" type="text/css" media="screen">
<link rel="stylesheet" href="resources/css/isotope.css" type="text/css" media="screen">      
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="resources/js/jquery.js"></script>  
<script type="text/javascript" src="resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="resources/js/superfish.js"></script>

<script type="text/javascript" src="resources/js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="resources/js/touchTouch.jquery.js"></script>
<script type="text/javascript" src="resources/js/jquery.isotope.min.js"></script>

<script type="text/javascript" src="resources/js/script.js"></script>
<script>
$(document).ready(function() {
	//
	

	



}); //
$(window).load(function() {
	//
	// isotop
	var $container = $('#isotope-items'),
		$optionSet = $('#isotope-options'), 
	    $optionSets = $('#isotope-filters'), 
	    $optionLinks = $optionSets.find('a'); 
    $container.isotope({ 
        filter: '*',
        layoutMode: 'fitRows'
    });  
   	$optionLinks.click(function(){ 
   		var $this = $(this); 
    	// don't proceed if already selected 
		if ( $this.hasClass('selected') ) { 
			return false; 
		}    		
   		$optionSet.find('.selected').removeClass('selected'); 
   		$this.addClass('selected');

        var selector = $(this).attr('data-filter'); 
        $container.isotope({ 
            filter: selector            
        }); 
      	return false; 
    });    	
	$(window).on("resize", function( event ) {	
		$container.isotope('reLayout');		
	});		

	// touchTouch
	$('.thumb-isotope .thumbnail a').touchTouch();

}); //
</script>		
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
	
<header><div class="logo_wrapper"><a href="./" class="logo"><img src="resources/images/NPPlogo.png" alt=""></a></div></header>

<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="./">Home</a></li>
<li class="sub-menu sub-menu-1"><a href="#">About</a>
	<ul>
		<li><a href="VR_media">VR View</a></li>
		<li class="sub-menu sub-menu-2"><a href="Pano_media">Pano View<em></em></a></li>
		<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="VR_media">services</a></li>

<li class="active"><a href="#">Accommodations</a></li>

<c:if test="${loginId == null && result == null}">											
	<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
	<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>		
</c:if>


<c:if test="${loginId != null || result != null}">
	<li class="sub-menu sub-menu-1"><a href="index-1.html">MY PAGE</a>
		<ul>
			<li id="naverName"></li>
			<li><a href="index-1.html">Account info</a></li>
			<li><a href="hostAcco/selectType">Start Hosting</a></li>
			<li><a href="member/logout">Logout</a></li>
		</ul>
	</li>
</c:if>
						
    </ul>
		</div>
	</div>
</div>


</div>

</div>	
</div>

<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src="resources/images/bg1.jpg" alt="" class="img"></div>
<div class="top2_inner">
<div class="container">
<div class="top2 clearfix">
	
<h1>VR Gallery</h1>

</div>	
</div>	
</div>
</div>



<div id="content">
<div class="container">
<div class="row">
<div class="span12">
	
<h2><span>Our Photos</span></h2>

<div id="isotope-options">
    <ul id="isotope-filters" class="clearfix">
        <li><a href="#" class="selected" data-filter="*">Show All</a></li> 
        <li><a href="#" data-filter=".isotope-filter1">Category 1</a></li> 
        <li><a href="#" data-filter=".isotope-filter2">Category 2</a></li> 
        <li><a href="#" data-filter=".isotope-filter3">Category 3</a></li> 	             
    </ul>            
</div>

<ul class="thumbnails" id="isotope-items">		  
    <li class="span4 isotope-element isotope-filter1">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="resources/images/blank.png">
					<figure>
						<img src="resources/images/gallery01.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter2">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery02.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter3">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery03.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter1">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery04.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter2">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery05.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter3">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery06.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter1">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery07.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter2">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery08.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
    <li class="span4 isotope-element isotope-filter3">
		<div class="thumb-isotope">
			<div class="thumbnail clearfix">
				<a href="images/blank.png">
					<figure>
						<img src="resources/images/gallery09.jpg" alt=""><em></em>
					</figure>
					<div class="caption">							
						Lorem ipsum dolor sit amet conse ctetur
					</div>
				</a>				
			</div>
		</div>
    </li>
       
</ul>










</div>	
</div>


</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="resources/images/NPPlogo.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>
<div class="span3 offset1">

<div class="bot1_title">Follow Us</div>
	
<div class="social_wrapper">
	<ul class="social clearfix">    
    <li><a href="#"><img src="resources/images/social_ic1.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic2.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic3.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic4.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic5.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic6.png"></a></li>
	</ul>
</div>

</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="resources/js/bootstrap.js"></script>
</body>
</html>