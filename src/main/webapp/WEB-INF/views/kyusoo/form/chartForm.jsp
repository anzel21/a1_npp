<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<title>UserInfoForm</title>
<!-- css 파일 가져옴 -->
<style type="text/css">
	@import url("./../resources/css/jw_main/myPage.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "resources/images/favicon.ico"/>
	type="image/x-icon">
<link rel="shortcut icon"
	href=<c:url value = "resources/images/favicon.ico"/>
	type="image/x-icon" />
<link rel="stylesheet"
	href=<c:url value = "resources/css/bootstrap.css"/> type="text/css"
	media="screen">
<link rel="stylesheet"
	href=<c:url value = "resources/css/bootstrap-responsive.css"/>
	type="text/css" media="screen">
<link rel="stylesheet"
	href=<c:url value = "resources/css/style.css"/> type="text/css"
	media="screen">

<script type="text/javascript"
	src=<c:url value = "resources/js/jquery.js" />></script>
<script type="text/javascript"
	src=<c:url value = "resources/js/jquery.easing.1.3.js"/>></script>
<script type="text/javascript"
	src=<c:url value = "resources/js/superfish.js"/>></script>

<script type="text/javascript"
	src=<c:url value = "resources/js/jquery.ui.totop.js" />></script>

<script type="text/javascript"
	src=<c:url value = "resources/js/script.js"/>></script>

<script>
	var newEvent = null;
	var list = null;
	var xx = "<div class = 'reservedRoom' style = 'padding-top: 10px;'>";

	var repairIcon = '<c:url value = "../resources/kjw/repair.png"/>';
	var canIcon = '<c:url value = "../resources/kjw/cancel_red.png"/>';

	function printList(list) {

		for (var i = 0; i < list.length; i++) {
			xx = xx
					+ "<table style = 'border: black solid thin; margin-left:5px; width:240px;'>"
					+ "<tr><td style = 'width:150px;'>" + list[i].subscriberID
					+ "</td>" + "<td class = 'iconTD' rowspan = '2'>"
					+ "<img style = 'width:25px;' src = '" + repairIcon + "'/>"
					+ "</td>" + "<td class = 'iconTD' rowspan = '2'>"
					+ "<img style = 'width:25px;' src = '" + canIcon + "'/>"
					+ "</td></tr>" + "<tr'><td>" + list[i].startDate + "~"
					+ list[i].endDate + "</td></tr></table>";
			xx = xx + "<table><tr height = '15px'><td></td></tr></table>";
		}

		xx = xx + "</div>";
		$('#reservedList').html(xx);
	}
</script>

<!-- 차트 뷰 스크립트 -->
<script type="text/javascript">
window.onload = function() {
 
var dps = [[]];
var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2", // "light1", "dark1", "dark2"
	//exportEnabled: true, // 여기 프린터랑 이미지 저장 옵션 활성화
	animationEnabled: true,
	title: {
		text: ""
	},
	data: [{
		type: "pie",
		showInLegend: "true",
		legendText: "{label}",
		yValueFormatString: "#,###\"%\"",
		indexLabelFontSize: 16,
		indexLabel: "{label} - {y}",
		dataPoints: dps[0]
	}]
});
 
var yValue;
var label;
 
<c:forEach items="${dataPointsList}" var="dataPoints" varStatus="loop">	
	<c:forEach items="${dataPoints}" var="dataPoint">
		yValue = parseFloat("${dataPoint.y}");
		label = "${dataPoint.label}";
		dps[parseInt("${loop.index}")].push({
			label : label,
			y : yValue,
		});		
	</c:forEach>	
</c:forEach> 
 
chart.render();
}
</script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<style>
.iconTD {
	text-align: center;
}
</style>

</head>

<body class="not-front">
	<div id="main">
		<div class="top1_wrapper">
			<div class="top1 clearfix">
				<header>
					<div class="logo_wrapper">
						<a href="./" class="logo"><img
							src="resources/images/NPPlogo.png" alt=""></a>
					</div>
				</header>
				<div class="menu_wrapper clearfix">
					<div class="navbar navbar_">
						<div class="navbar-inner navbar-inner_">
							<a class="btn btn-navbar btn-navbar_" data-toggle="collapse"
								data-target=".nav-collapse_"> <span class="icon-bar"></span>
								<span class="icon-bar"></span> <span class="icon-bar"></span>
							</a>
							<div class="nav-collapse nav-collapse_ collapse">
								<ul class="nav sf-menu clearfix">
									<li><a href="./">home</a></li>
									<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
										<ul>
											<li><a href="index-1.html">who we are</a></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">find
													an agent<em></em>
											</a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li><a href="index-1.html">testimonials</a></li>
										</ul></li>
									<li><a href="../VR_media">services</a></li>
									<!-- test -->
									<li><a href="../test">ACCOMMODATIONS</a></li>

									<c:if test="${loginId == null}">
										<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
										<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
									</c:if>
									<c:if test="${loginId != null}">
										<li class="sub-menu sub-menu-1 active"><a
											href="#" onclick="history.go(0)">MY PAGE</a>
											<ul>
												<li><a href="member/userInfoForm">Account info</a></li>
												<li><a href="hostAcco/selectType">Start Hosting</a></li>
												<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation
												</a></li>
												<li><a href="member/logout">Logout</a></li>
											</ul>
										</li>
									</c:if>
								</ul>
							</div>
						</div>
					</div>
					<div id="search">
						<a href="#" class="searchBtn"></a>
						<div class="search-form-wrapper">
							<form id="search-form" action="search.php" method="GET"
								accept-charset="utf-8" class="navbar-form">
								<input type="text" name="s" value='Search'
									onBlur="if(this.value=='') this.value='Search'"
									onFocus="if(this.value =='Search' ) this.value=''"> <a
									href="#"
									onClick="document.getElementById('search-form').submit()"></a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="inner">
			<div class="top2_wrapper">
				<div class="bg1">
					<img src="resources/images/bg1.jpg" alt="" class="img">
				</div>
				<div class="top2_inner">
					<div class="container">
						<div class="top2 clearfix">
							<h1>My Page</h1>
							<div class="breadcrumbs1"></div>
						</div>
					</div>
				</div>
			</div>

			<div id="content">
				<div class="container">
					<div class="row">
						<div class="span9">
						<!-- 차트 뷰 시작 -->
							<h2> <span>MY ACCOMODATION STAT</span> </h2>
							<div class="mainInfoDiv">
								<!-- 차트 DIV -->								
								<div id="chartContainer" style="height: 370px; width: 100%;"></div>
	
							</div><!-- 전체 Div 종료 -->
							
						</div>

						<div class="span3">
							<h2> <span>My page Menu</span> </h2>
							<div style="text-align: center; float: none;">
								<img src="<c:url value = '/resources/kjw/btf.png' />"> <br>
								<c:if test="${loginId != null}">
									${loginId}
								</c:if>
							</div>

							<ul class="ul1">
								<li><a href="#">프로필 사진 변경</a></li>
								<li><a href="#" onclick="history.go(0)">개인정보 조회</a></li>
								<li><a href="<c:url value = '../RS_Change' />">예약 조회 / 변경 / 취소</a></li>
								<li><a href="mychart">My Statistic</a></li>
								<li><a href="#">Incididunt ut labore et</a></li>
								<li><a href="#">Lorem ipsum dolor</a></li>
								<li><a href="#">Sit amet conse ctetur</a></li>
								<li><a href="#">Ut enim ad minim</a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bot1_wrapper">
			<div class="container">
				<div class="bot1 clearfix">
					<div class="row">
						<div class="span3">
							<div class="bot1_title">Copyright</div>
							<div class="logo2_wrapper">
								<a href="./" class="logo2"><img
									src="resources/images/NPPlogo.png" alt=""></a>
							</div>
							<footer>
								<div class="copyright">
									Copyright © 2014.<br>All rights reserved.
								</div>
							</footer>
						</div>
						<div class="span5">
							<div class="bot1_title">Useful information</div>
							<p>
								<b> Nulla ultricies enim aliquet augue eleifend iaculis. </b>
							</p>
							<p>Nam sollicitudin ligula ac nisi iaculis eu scelerisque
								risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean
								leo nulla, aliquet vitae ultricies sit amet, porttitor id
								sapien. In hac habitasse platea dictumst. Donec pharetra gravida
								augue at hendrerit. Cras ut...</p>
						</div>
						<div class="span3 offset1">
							<div class="bot1_title">Follow Us</div>
							<div class="social_wrapper">
								<ul class="social clearfix">
									<li><a href="#"><img
											src="resources/images/social_ic1.png"></a></li>
									<li><a href="#"><img
											src="resources/images/social_ic2.png"></a></li>
									<li><a href="#"><img
											src="resources/images/social_ic3.png"></a></li>
									<li><a href="#"><img
											src="resources/images/social_ic4.png"></a></li>
									<li><a href="#"><img
											src="resources/images/social_ic5.png"></a></li>
									<li><a href="#"><img
											src="resources/images/social_ic6.png"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src='<c:url value = "resources/js/bootstrap.js"/>'></script>
</body>
</html>