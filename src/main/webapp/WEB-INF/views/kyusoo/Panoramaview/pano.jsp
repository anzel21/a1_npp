<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
</style>
<meta http-equiv="Content-Type">
<script type="text/javascript"
	src="<c:url value="./../resources/360panorama/js/html5pano.js" />">
</script>  
</head>
<body onload="init_pano('canvas')">

<!-- 파노라마 뷰 출력 -->
<div id="PanoViewDiv" class="openPanoStyle">
<!-- 페이지 종료 버튼 -->
	<div class="xClose"><span onclick="PanoOverlay()">X</span></div>
	<br>
    <div align="center"> <br>
      <br>
      <canvas id="canvas" width="800" height="480">
        <p>Your browser does not support the HTML5 canvas element.</p>
      </canvas> <br>
      Drag your mouse to navigate through the panorama. </div>

</div>
<!-- 파노라마뷰 Overlay 화면 -->
<div id="PanoViewOverlay" class="panoOverlayStyle"></div>
</body>
</html>
