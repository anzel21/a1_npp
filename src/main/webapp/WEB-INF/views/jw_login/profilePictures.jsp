<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>프로필 사진 변경</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
$(function() {
	$("#imgInp").on('change', function(){
		readURL(this);
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
    </script>
</head>
<body>
<form action="pictureRegistData" method="post" enctype="multipart/form-data">
<div style="height: 30px; width: 550px; mar"></div>
<input type="file" name="upload" id="imgInp"> 
<input type="submit" value="등록하기" style="float: right; margin-right: 20px;"> 
<br>
<br>
<div style="height: 100px; width: 550px;">
	<div style="height: 310px; width: 270px; float: left; border: 1px solid black;" >
		<div style="height: 30px; width: 270px; text-align: center; border-bottom: 1px solid black;">현재 등록된 사진</div>
		<img src="download?loginId=${loginId}&loginType=${loginType}" style="height: 275px; width: 265px;">
	</div>
	<div style="height: 310px; width: 270px; float: left;  border: 1px solid black;">
		<div style="height: 30px; width: 270px; text-align: center; border-bottom: 1px solid black;">변경할 사진</div>
		<img id="blah" src="#" alt="your image" style="height: 275px; width: 265px;">
	</div>
</div>
</form>
</body>
</html>