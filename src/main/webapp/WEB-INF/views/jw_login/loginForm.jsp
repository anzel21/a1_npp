<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
  
<script>
$(document).ready(start);
var errMsg = '${errorMsg}';
	
function start() {  
	$('#loginSpan').on('click', loginData);
}

</script>	

</head>
<body>
	<div id="LoginDiv" class="openLoginStyle">
	<!-- 페이지 종료 버튼 -->
	<div class="loginClose"><span onclick="loginOverlay()">X</span></div>
	<br>
	<!-- JW_소셜로그인 1 . 네이버 로그인 연동 버튼 -->
	<div class="socialDiv" id="naver_id_login">
		<a href="${naverUrl}">
		<img alt="" src="https://img.huffingtonpost.com/asset/5ab857112000007d06eb2f6f.png?ops=scalefit_630_noupscale" style="height: 50px; width: 394px;">
		</a>
	</div>
	<br>
	<br>
	<!-- JW_소셜로그인 2 . 카카오 로그인 연동 버튼 -->
	<div class="socialDiv" id="kakao_id_login">
		<a href="${kakaoUrl}">
		<img alt="" src="http://usefulpa.s3.amazonaws.com/images/2014/kakao_account_login_btn_large_narrow_ov.png" style="height: 50px; width: 394px;">
		</a>
	</div>
	<br>
		
	<!-- JW_소셜로그인, 자체로그인 구분 -->
	<div class="inputTextDiv">
		<div class="SLorWL">or</div>
	</div>
	
	<form id="loginData">
		<!-- JW_아이디 입력란 -->
		<div class="inputTextDiv">
			<input type="text" class="inputText" placeholder="id" name="id">
		</div>
		<!-- JW_아이디 입력에러 출력용 div -->
		<div class="errDiv" id="login_idErrDiv">&nbsp;</div>
		<!-- JW_비밀번호 입력란 -->
		<div class="inputTextDiv">
			<input type="password" class="inputText" placeholder="password" name="pw">
		</div>
		<!-- JW_비밀번호 입력 에러 출력용 -->
		<div class="errDiv" id="login_pwErrDiv">&nbsp;</div>
	</form>	
		<!-- 선택지 링크  -->
		<div class="inputTextDiv">
			<div class="loginDiv"><span style="cursor: pointer;" id="loginSpan">L o g i n</span></div>
			<div class="searchDiv"><span style="cursor: pointer;" onclick="goFindIdPw()">아이디 조회</span></div>
			<div class="searchDiv"><span style="cursor: pointer;" onclick="goFindIdPw()">비밀번호 조회</span></div>
		</div>
	<hr style="height: 2px; background-color: #FFFFFF">
	
	<!-- JW_회원가입 버튼 -->
	<div class="inputTextDiv">
		<div class="joinDiv"><span style="cursor: pointer;" onclick="goJoinForm()">회 원 가 입</span> </div>
	</div>
	
</div>
<!-- JW_Login 화면 종료 -->
<div id="LoginDivOverlay" class="LoginOverlayStyle"></div>
</body>
</html>