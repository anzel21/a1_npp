<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>비정상 접근 확인</title>
<style type="text/css">
#imgCss {
	margin: 0 auto;
	width: 500px;
	height: 350px;
	text-align: center;
}
</style>
</head>
<body>
<div id="imgCss">
	<img alt="" src=<c:url value ="/resources/images/jw_socialLogin/interceptor_Img.jpg"/> style="width: 500px; height: 300px;">
	<h2>잘못된 접근경로입니다...ㅠ</h2>
</div>
</body>
</html>