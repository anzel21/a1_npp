// PanoramaView 동작 관련
var path = null;
function openPanoPage(a) {
	// 파노라마로 보여줄 이미지의 경로를 가져옴
	path = document.getElementById('popUpimg').src=a;
	img.src = path;
	//  화면 최상단으로 이동
	$('html, body').scrollTop(0);
	
	document.getElementById('PanoViewDiv').style.display = 'block';
	$('#PanoViewDiv').css('height','600px');
	$('#PanoViewDiv').css('margin-left','-100px');
	$('#PanoViewDiv').css('margin-top','-120px');
	document.getElementById('PanoViewOverlay').style.display = 'block';
	// JW_마우스 스크롤 비활성화
	$(document)
			.on(
					"mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll",
					function(e) {
						e.preventDefault();
						return;
					});
	$(document).on("keydown.disableScroll", function(e) {
		var eventKeyArray = [ 32, 33, 34, 35, 36, 37, 38, 39, 40 ];
		for (var i = 0; i < eventKeyArray.length; i++) {
			if (e.keyCode === eventKeyArray[i]) {
				e.preventDefault();
				return;
			}
		}
	});
}

// PanoramaView 의 종료버튼 클릭 시
function PanoOverlay() {
	location.reload();
	document.getElementById('PanoViewDiv').style.display = 'none';
	document.getElementById('PanoViewOverlay').style.display = 'none';
	
	// JW_마우스 스크롤 활성화
	$(document).off(".disableScroll");
}