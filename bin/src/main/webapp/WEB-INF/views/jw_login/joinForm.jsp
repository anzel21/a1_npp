<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>resources</title>
<style type="text/css">
	@import url("./../resources/css/jw_main/login.css");
	@import url("./../resources/css/jw_main/join.css");
</style>
<script src="../resources/js/jquery-3.2.1.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="../resources/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="../resources/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="../resources/css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="../resources/css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="../resources/css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="../resources/js/jquery.js"></script>  
<script type="text/javascript" src="../resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../resources/js/superfish.js"></script>

<script type="text/javascript" src="../resources/js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="../resources/js/script.js"></script>

<script>
$(document).ready(start);
var errMsg = '${errorMsg}';
	
function start() {  
	$('#button1').on('click', joinData);
}

</script>	

<!-- join.js 에서 회원가입 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/join.js"></script>	

<!-- login.js 에서 login 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/login.js"></script>

</head>

<body class="not-front">
<!-- JW_Login 화면 동작 -->
<%@ include file = "loginForm.jsp" %>

<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
	
<header><div class="logo_wrapper"><a href="index.html" class="logo"><img src="../resources/images/logo.png" alt=""></a></div></header>

<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="../">home</a></li>
<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
	<ul>
		<li><a href="index-1.html">who we are</a></li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">find an agent<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="VR_media">services</a></li>
<!-- test -->
<li><a href="test">ACCOMMODATIONS</a></li>

<c:if test="${loginId == null}">											
	<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
	<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>		
</c:if>
<c:if test="${loginId != null}">
	<li class="sub-menu sub-menu-1"><a href="member/userInfoForm">MY PAGE</a>
		<ul>
			<li id="naverName"></li>
			<li><a href="member/userInfoForm">ACCOUNT INFO</a></li>
			<li><a href="member/logout">LOG OUT</a></li>
		</ul>
	</li>
</c:if>				
    </ul>
		</div>
	</div>
</div>
<div id="search">
	
<a href="#" class="searchBtn"></a>

<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>
</div>
<div id="inner">
<div class="top2_wrapper">
<div class="bg1"><img src="../resources/images/bg1.jpg" alt="" class="img"></div>
<div class="top2_inner">
<div class="container">
<div class="top2 clearfix">
	
<h1>Account</h1>

<div class="breadcrumbs1"><a href="index.html">Home</a><span></span><a href="index.html">Pages</a><span></span>Resources</div>
</div>	
</div>	
</div>
</div>
</div>


<div id="content">
<div class="container">
<div class="row">
<div class="span9" style="width: 1200px;">
	
<h2><span>Make an Account</span></h2>
	<%-- 가입정보 입력 --%>
	<div class="frameBlank"></div>
	
	<form id="joinData">
	<%-- 윗 부분 정보 --%>
		<%-- 아이디 --%>
		<div class="rowDiv">
			<div class="listDiv">아이디</div>
			<div class="inputDiv"><input class="inputStyle1" type="text" name="id"> </div> 
		</div>
		<%-- 아이디 에러 --%>
		<div class="errMsgDiv" id="idErrDiv">&nbsp;</div>
		
		<%-- 비밀번호 --%>
		<div class="rowDiv">
			<div class="listDiv">비밀번호</div>
			<div class="inputDiv"><input class="inputStyle1" type="password" name="password1"> </div> 
		</div>
		<%-- 비밀번호 에러 --%>
		<div class="errMsgDiv" id="pwErrDiv">&nbsp;</div>
		
		<%-- 비밀번호 확인 --%>
		<div class="rowDiv">
			<div class="listDiv">비밀번호 확인</div>
			<div class="inputDiv"><input class="inputStyle1" type="password" name="password2"> </div> 
		</div>
		<%-- 비밀번호 확인 에러 --%>
		<div class="errMsgDiv" id="pwErrDiv2">&nbsp;</div>
	<div class="frameBlank">
	</div>
	<%-- 아래 부분 정보 --%>
		<%-- 이름 --%>
		<div class="rowDiv">
			<div class="listDiv">이름</div>
			<div class="inputDiv"><input class="inputStyle1" type="text" name="name"> </div> 
		</div>
		<%-- 이름 에러 --%>
		<div class="errMsgDiv" id="nameErrDiv">&nbsp;</div>
		
		<%-- 전화번호 --%>
		<div class="rowDiv2">
			<div class="listDiv2">전화번호</div>
			<div class="inputDiv2">
				<select id='fruits' class="selectCountryNum" name="countryNum">
				<option value='' selected>-- 국가 번호 선택 --</option>
				<option value='1'>+1) 미국</option>
				<option value='81'>+81) 일본</option>
				<option value='821'>+821) 한국</option>
				</select>
				<br>
				<input class="inputStyle2" type="text" name="phonenumber1">
				&nbsp;-&nbsp;<input class="inputStyle3" type="text" name="phonenumber2">
				&nbsp;-&nbsp;<input class="inputStyle3" type="text" name="phonenumber3"> </div> 
		</div>
		<%-- 전화번호 에러 --%>
		<div class="errMsgDiv" id="phoneErrDiv">&nbsp;</div>
		
		<%-- 생년월일 --%>
		<div class="rowDiv">
			<div class="listDiv">생년월일</div>
			<div class="inputDiv">
				<input class="inputStyle1" type="text" name="birthdateCheck" style="width: 150px;">
			 </div> 
		</div>
		<%-- 생년월일 에러 --%>
		<div class="errMsgDiv" id="birthDateErrDiv">&nbsp;</div>
		
		<div class="rowDiv" style="width: 550px;">
			<div class="listDiv3">본인의 방을 등록해보세요.</div>
			<div class="listDiv4"> 등록하기</div>
			&nbsp;&nbsp;<input type="checkbox" name="typeCheck"> 
		</div>
		
	<div class="frameBlank"></div>
	<%-- 연결 버튼 --%>
	<div class="FrameButtonsCenter">
		<div class="listDiv4"><input class="btn btn-primary" type="button" value="회원가입" id="button1"></div>
		<div class="listDiv4"><input class="btn btn-danger" type="button" value="뒤로가기" onclick="goBack()"></div>
	</div>
	<div class="frameBlank3"></div>
	</form>

<br>


<!-- 이 아래 Footer -->

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="../resources/images/logo2.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>
<div class="span3 offset1">

<div class="bot1_title">Follow Us</div>
	
<div class="social_wrapper">
	<ul class="social clearfix">    
    <li><a href="#"><img src="../resources/images/social_ic1.png"></a></li>
    <li><a href="#"><img src="../resources/images/social_ic2.png"></a></li>
    <li><a href="#"><img src="../resources/images/social_ic3.png"></a></li>
    <li><a href="#"><img src="../resources/images/social_ic4.png"></a></li>
    <li><a href="#"><img src="../resources/images/social_ic5.png"></a></li>
    <li><a href="#"><img src="../resources/images/social_ic6.png"></a></li>
	</ul>
</div>
</div>
</div>
</div>	
</div>	
</div>	
</div>
</div>
</div>	
</div>
<script type="text/javascript" src="../resources/js/bootstrap.js"></script>
</body>
</html>