<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>UserInfoForm</title>
<!-- css 파일 가져옴 -->
<style type="text/css">
	@import url("./../resources/css/jw_main/myPage.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "../resources/images/favicon.ico"/>
	type="image/x-icon">
<link rel="shortcut icon"
	href=<c:url value = "../resources/images/favicon.ico"/>
	type="image/x-icon" />
<link rel="stylesheet"
	href=<c:url value = "../resources/css/bootstrap.css"/> type="text/css"
	media="screen">
<link rel="stylesheet"
	href=<c:url value = "../resources/css/bootstrap-responsive.css"/>
	type="text/css" media="screen">
<link rel="stylesheet"
	href=<c:url value = "../resources/css/style.css"/> type="text/css"
	media="screen">

<script type="text/javascript"
	src=<c:url value = "../resources/js/jquery.js" />></script>
<script type="text/javascript"
	src=<c:url value = "../resources/js/jquery.easing.1.3.js"/>></script>
<script type="text/javascript"
	src=<c:url value = "../resources/js/superfish.js"/>></script>

<script type="text/javascript"
	src=<c:url value = "../resources/js/jquery.ui.totop.js" />></script>

<script type="text/javascript"
	src=<c:url value = "../resources/js/script.js"/>></script>

<script>
$(document).ready(start);
var errMsg = '${errorMsg}';
var logintype = '${loginType}';
function start() {  
	if (logintype == "") {
		$('#button1').on('click', webInfoData);
	} else if (logintype != null) {
		$('#button2').on('click', socialInfoData);
	}
}

	var newEvent = null;
	var list = null;
	var xx = "<div class = 'reservedRoom' style = 'padding-top: 10px;'>";

	var repairIcon = '<c:url value = "../resources/kjw/repair.png"/>';
	var canIcon = '<c:url value = "../resources/kjw/cancel_red.png"/>';

	function printList(list) {

		for (var i = 0; i < list.length; i++) {
			xx = xx
					+ "<table style = 'border: black solid thin; margin-left:5px; width:240px;'>"
					+ "<tr><td style = 'width:150px;'>" + list[i].subscriberID
					+ "</td>" + "<td class = 'iconTD' rowspan = '2'>"
					+ "<img style = 'width:25px;' src = '" + repairIcon + "'/>"
					+ "</td>" + "<td class = 'iconTD' rowspan = '2'>"
					+ "<img style = 'width:25px;' src = '" + canIcon + "'/>"
					+ "</td></tr>" + "<tr'><td>" + list[i].startDate + "~"
					+ list[i].endDate + "</td></tr></table>";
			xx = xx + "<table><tr height = '15px'><td></td></tr></table>";
		}

		xx = xx + "</div>";
		$('#reservedList').html(xx);
	};
</script>
<!-- userInfo.js 에서 회원정보 관련 JS파일 불러옴 -->
<script type="text/javascript" src="../resources/js/jw/userInfo.js"></script>

<style>
.iconTD {
	text-align: center;
}
</style>

</head>

<body class="not-front">
	<div id="main">
		<div class="top1_wrapper">
			<div class="top1 clearfix">
				<header>
					<div class="logo_wrapper">
						<a href="./../" class="logo"><img
							src="../resources/images/NPPlogo.png" alt=""></a>
					</div>
				</header>
				<div class="menu_wrapper clearfix">
					<div class="navbar navbar_">
						<div class="navbar-inner navbar-inner_">
							<a class="btn btn-navbar btn-navbar_" data-toggle="collapse"
								data-target=".nav-collapse_"> <span class="icon-bar"></span>
								<span class="icon-bar"></span> <span class="icon-bar"></span>
							</a>
							<div class="nav-collapse nav-collapse_ collapse">
								<ul class="nav sf-menu clearfix">
									<li><a href="./../">home</a></li>
									<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
										<ul>
											<li><a href="index-1.html">who we are</a></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">find
													an agent<em></em>
											</a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li><a href="index-1.html">testimonials</a></li>
										</ul></li>
									<li><a href="../VR_media">services</a></li>
									<!-- test -->
									<li><a href="../test">ACCOMMODATIONS</a></li>

									<c:if test="${loginId == null}">
										<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
										<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
									</c:if>
									<c:if test="${loginId != null}">
										<li class="sub-menu sub-menu-1 active"><a
											href="#" onclick="history.go(0)">MY PAGE</a>
											<ul>
												<li><a href="member/userInfoForm">Account info</a></li>
												<li><a href="hostAcco/selectType">Start Hosting</a></li>
												<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation
												</a></li>
												<li><a href="member/logout">Logout</a></li>
											</ul>
										</li>
									</c:if>
								</ul>
							</div>
						</div>
					</div>
					<div id="search">
						<a href="#" class="searchBtn"></a>
						<div class="search-form-wrapper">
							<form id="search-form" action="search.php" method="GET"
								accept-charset="utf-8" class="navbar-form">
								<input type="text" name="s" value='Search'
									onBlur="if(this.value=='') this.value='Search'"
									onFocus="if(this.value =='Search' ) this.value=''"> <a
									href="#"
									onClick="document.getElementById('search-form').submit()"></a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="inner">
			<div class="top2_wrapper">
				<div class="bg1">
					<img src="../resources/images/bg1.jpg" alt="" class="img">
				</div>
				<div class="top2_inner">
					<div class="container">
						<div class="top2 clearfix">
							<h1>My Page</h1>
							<div class="breadcrumbs1"></div>
						</div>
					</div>
				</div>
			</div>

			<div id="content">
				<div class="container">
					<div class="row">
						<div class="span9">
							<h2> <span>ACCOUNT INFO</span> </h2>\
							
							<div class="mainInfoDiv">
							<form id="userInfoData">
								<div style="height: 180px;">
									<div class=topLeftDiv>
										<div class="blankDiv"></div>
										<div class="cellDivSmall"> <!-- ID cell -->
										<!-- WebMember 로 로그인 한 경우 -->
										<c:if test="${loginType == null}">
											<div class="cellNameDivSmall">ID</div> 
											<div class="cellTypeDivSmall"><input type="text" value="${webVo.id}" readonly="readonly" name="id"></div>
											<div class="errMsg" style="margin-left: 50%" id="idErrDiv"></div>
										</c:if> 
										<!-- SocialMember 로 로그인 한 경우 -->
										<c:if test="${loginType != null}">
											<div class="cellNameDivSmall">Email_ID</div> 
											<div class="cellTypeDivSmall"><input type="text" value="${socialVo.emailid}" readonly="readonly" name="id"></div>
											<div class="errMsg" style="margin-left: 50%" id="idErrDiv"></div>
										</c:if>
										</div> <!-- ID cell 종료 -->
										
										<div class="cellDivSmall"> <!-- 이름 cell -->
										<!-- WebMember 로 로그인 한 경우 -->
										<c:if test="${loginType == null}">
											<div class="cellNameDivSmall">이름</div> 
											<div class="cellTypeDivSmall"><input type="text" value="${webVo.name}" readonly="readonly" name="name"></div>
											<div class="errMsg" style="margin-left: 50%" id="nameErrDiv"></div>
										</c:if>	
										<!-- SocialMember 로 로그인 한 경우 -->
										<c:if test="${loginType != null}">
											<div class="cellNameDivSmall">이름</div> 
											<div class="cellTypeDivSmall"><input type="text" value="${socialVo.name}" readonly="readonly" name="name"></div>
											<div class="errMsg" style="margin-left: 50%" id="nameErrDiv"></div>
										</c:if>		
										</div><!-- 이름 cell 종료 -->
									</div> <!-- 왼쪽 상단 Div 종료-->
									<div class="topRightDiv"> 
										<div class="blankDiv"></div>
										<div class="hostingSpan">
											<span class="hostingIntroSpan"> 본인의 방을 등록해보세요 </span>
										</div>
										<div class="hostingSpan"> <!-- 호스팅 cell -->
											<!-- WebMember 로 로그인 한 경우 -->
											<c:if test="${loginType == null}">
												<!-- 호스팅 신청을 안한 경우 (type = 0) -->
												<c:if test="${webVo.type == 0}">
													<span class="hostingCheckSpan">호스팅 등록 </span>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" style="width: 30px; height: 30px;" name="type">
												</c:if>
												<!-- 호스팅 신청을 한 경우 (type = 1) -->
												<c:if test="${webVo.type == 1}">
													<span class="hostingCheckSpan">호스팅 등록 </span>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="checked" style="width: 30px; height: 30px;" name="type">
												</c:if>
											</c:if>
											<!-- SocialMember 로 로그인 한 경우 -->
											<c:if test="${loginType != null}">
												<!-- 호스팅 신청을 안한 경우 (type = 0) -->
												<c:if test="${socialVo.type == 0}">
													<span class="hostingCheckSpan">호스팅 등록 </span>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" style="width: 30px; height: 30px;" name="type">
												</c:if>
												<!-- 호스팅 신청을 한 경우 (type = 1) -->
												<c:if test="${socialVo.type == 1}">
													<span class="hostingCheckSpan">호스팅 등록 </span>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="checked" style="width: 30px; height: 30px;" name="type">
												</c:if>
											</c:if>
										</div> <!-- 호스팅 cell 종료 -->
									</div><!-- 오른쪽 상단 Div 종료 -->
								</div> <!-- 위쪽 2개 Div 종료 -->
							
							<div style="height: 850px;">
									<div style="height: 465px;">
								<!-- WebMember 로 로그인 한 경우 -->
								<c:if test="${loginType == null}">
									<div class="cellDiv">
										<div class="cellNameDiv">비밀번호</div> 
										<div class="cellTypeDiv"><input type="password" style="width: 430px;" value="${webVo.pw}" name="password1"></div>
										<div class="errMsg" style="margin-left: 30%" id="pwErrDiv"></div>
									</div>
								</c:if>
									<div class="cellDiv">
									<!-- WebMember 로 로그인 한 경우 -->
									<c:if test="${loginType == null}">
										<div class="cellNameDiv">생년월일</div> 
										<div class="cellTypeDiv"><input type="text" style="width: 430px;" value="${webVo.birthdate}" name="birthdateCheck"></div>
										<div class="errMsg" style="margin-left: 30%" id="birthDateErrDiv"></div>
									</c:if>
									<!-- SocialMember 로 로그인 한 경우 -->
									<c:if test="${loginType != null}">
										<div class="cellNameDiv">생년월일</div> 
										<!-- SocialMember에 등록된 생년월일이 없는 경우 -->
										<c:if test="${socialVo.birthdate == null}">
											<div class="cellTypeDiv"><input type="text" style="width: 430px;" name="birthdateCheck"></div>
										</c:if>
										<!-- SocialMember에 등록된 생년월일이 있는 경우 -->
										<c:if test="${socialVo.birthdate != null}">
											<div class="cellTypeDiv"><input type="text" style="width: 430px;" value="${socialVo.birthdate}" name="birthdateCheck"></div>
										</c:if>
										<div class="errMsg" style="margin-left: 30%" id="birthDateErrDiv"></div>
									</c:if>
									</div> <!-- 생년월일 cell 종료  -->
									<div class="cellDiv2">
										<div class="cellNameDiv">전화번호</div>
										<div class="cellTypeDiv2">
										<!-- WebMember 로 로그인 한 경우 -->
										<c:if test="${loginType == null}">
											<select id='CountryNum' class="selectCountryNum" name="countryNum">
											<option value='' <c:if test="${countryNum == null}">selected</c:if>>-- 국가 번호 선택 --</option>
											<option value='1' <c:if test="${countryNum == 1}">selected</c:if>>+1) 미국</option>
											<option value='81' <c:if test="${countryNum == 81}">selected</c:if>>+81) 일본</option>
											<option value='82' <c:if test="${countryNum == 82}">selected</c:if>>+82) 한국</option>
											</select>
											<br>
											<input class="phoneDiv" type="text" name="phonenumber1" value="${phonenumber1}">
											&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber2" value="${phonenumber2}">
											&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber3" value="${phonenumber3}">
										</c:if>
										
										<!-- SocialMember 로 로그인 한 경우 -->
										<c:if test="${loginType != null}">
											<!-- 전화번호가 등록이 안된 경우 ( null 인 경우 ) -->
											<c:if test="${socialVo.phonenumber == null}">
												<select id='CountryNum' class="selectCountryNum" name="countryNum">
													<option value='' <c:if test="${countryNum == null}">selected</c:if>>-- 국가 번호 선택 --</option>
													<option value='1' <c:if test="${countryNum == 1}">selected</c:if>>+1) 미국</option>
													<option value='81' <c:if test="${countryNum == 81}">selected</c:if>>+81) 일본</option>
													<option value='82' <c:if test="${countryNum == 82}">selected</c:if>>+82) 한국</option>
												</select>
												<br>
												<input class="phoneDiv" type="text" name="phonenumber1" value="${phonenumber1}">
												&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber2" value="${phonenumber2}">
												&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber3" value="${phonenumber3}">
											</c:if>
											<!-- 전화번호가 등록이 되어있는 경우 ( null 이 아닌 경우 ) -->
											<c:if test="${socialVo.phonenumber != null}">
												<select id='CountryNum' class="selectCountryNum" name="countryNum">
												<option value='' <c:if test="${countryNum == null}">selected</c:if>>-- 국가 번호 선택 --</option>
												<option value='1' <c:if test="${countryNum == 1}">selected</c:if>>+1) 미국</option>
												<option value='81' <c:if test="${countryNum == 81}">selected</c:if>>+81) 일본</option>
												<option value='82' <c:if test="${countryNum == 82}">selected</c:if>>+82) 한국</option>
												</select>
												<br>
												<input class="phoneDiv" type="text" name="phonenumber1" value="${phonenumber1}">
												&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber2" value="${phonenumber2}">
												&nbsp;-&nbsp;<input class="phoneDiv" type="text" name="phonenumber3" value="${phonenumber3}">
											</c:if>
										</c:if>
										</div> 
										<div class="errMsg" style="margin-left: 30%" id="phoneErrDiv"></div>
									</div> <!-- 전화번호 cell 종료 -->
									
									<div class="cellDiv">
									
										<div class="cellNameDiv">자기소개</div> 
										<!-- WebMember 로 로그인 한 경우 -->
										<c:if test="${loginType == null}">
											<!-- 자기소개글을 등록한 경우 -->
											<c:if test="${webVo.selfintroduction != null}">
												<div class="cellTypeDiv" style="height: 150px;"><textarea style="height: 150px; width: 430px;" name="selfintroduction">${webVo.selfintroduction}</textarea></div>
											</c:if>
											<!-- 자기소개 글을 등록하지 않은 경우 -->
											<c:if test="${webVo.selfintroduction == null}">
												<div class="cellTypeDiv" style="height: 150px;"><textarea style="height: 150px; width: 430px;" name="selfintroduction"></textarea></div>
											</c:if>
										</c:if>
										<!-- SocialMember 로 로그인 한 경우 -->
										<c:if test="${loginType != null}">
											<!-- 자기소개글을 등록한 경우 -->
											<c:if test="${socialVo.selfintroduction != null}">
												<div class="cellTypeDiv" style="height: 150px;"><textarea style="height: 150px; width: 430px;" name="selfintroduction">${socialVo.selfintroduction}</textarea></div>
											</c:if>
											<!-- 자기소개 글을 등록하지 않은 경우 -->
											<c:if test="${socialVo.selfintroduction == null}">
												<div class="cellTypeDiv" style="height: 150px;"><textarea style="height: 150px; width: 430px;" name="selfintroduction"></textarea></div>
											</c:if>
										</c:if>										
									</div> <!-- 자기소개 sell 종료 -->
									</div>
								<div style="height: 270px;"> <!-- 맨 아래 div -->
									<div class="lowInfo"><span> --- 추가 정보 입력 --- </span></div>
									<div class="lowIntro"><span> 회원님께 맞는 숙소를 찾아드리겠습니다. </span></div>
									<div class="blankDiv"></div>
									<div class="cellDiv">	
										<div class="cellNameDiv">성별</div> 
										<div class="cellTypeDivSmall">
										<select id='extraVo.gender' class="selectCountryNum" name="gender">
												<option value='미정' <c:if test="${extraVo.gender == null || extraVo.gender == '미정'}">selected</c:if>>-- 성별 선택 --</option>
												<option value='남' <c:if test="${extraVo.gender == '남'}">selected</c:if>>남</option>
												<option value='여' <c:if test="${extraVo.gender == '여'}">selected</c:if>>여</option>
												</select>
										</div>
									</div>
									<div class="cellDiv">
										<div class="cellNameDiv">직업</div> 
										<div class="cellTypeDivSmall"><input type="text" value="${extraVo.job}" name="job"></div>
									</div>
									<div class="cellDiv">	
										<div class="cellNameDiv">관심분야</div> 
										<div class="cellTypeDiv3">
											<input type="radio" name="chk_concern" value="맛집 탐방" class="attractRadio" 
											<c:if test="${extraVo.concern == '맛집 탐방' || extraVo.concern == '미정' || extraVo.concern == null}">checked</c:if>>맛집 탐방&nbsp;&nbsp;
											<input type="radio" name="chk_concern" value="문화 탐방" class="attractRadio" <c:if test="${extraVo.concern == '문화 탐방'}">checked</c:if>>문화 탐방&nbsp;&nbsp;
											<input type="radio" name="chk_concern" value="자연경관 구경" class="attractRadio" <c:if test="${extraVo.concern == '자연경관 구경'}">checked</c:if>>자연경관 구경&nbsp;&nbsp;
											<input type="radio" name="chk_concern" value="레저활동" class="attractRadio" <c:if test="${extraVo.concern == '레저활동'}">checked</c:if>>레저활동
										</div>
									</div>
									<div class="blankDiv"></div>
									<%-- 연결 버튼 --%>
								<div class="cellDiv">
									<div style="width: 40%; float: left; text-align: right;" >
									<c:if test="${loginType == null}">
									<input type="button" value="등록하기" style="height: 40; width: 100;" id="button1">
									</c:if>
									<c:if test="${loginType != null}">
									<input type="button" value="등록하기" style="height: 40; width: 100;" id="button2">
									</c:if>
									</div>
									<div style="width: 40%; float: right; text-align: left;" >
									<input type="button" value="뒤로가기" style="height: 40; width: 100;" onclick="goBack()"> 
									</div>
								</div> <!-- cellDiv 종료 -->
								</div> <!-- 270Div 종료 -->
								</div> <!-- 850 Div 종료 -->
								</form>
							</div><!-- mainInfoDiv 종료 -->
							
						</div>
						
						<div class="span3">
							<h2> <span>My page Menu</span> </h2>
							<div style="text-align: center; float: none;">
								<img src="<c:url value = '/resources/kjw/btf.png' />"> <br>
								<c:if test="${loginId != null}">
									${loginId}
								</c:if>
							</div>

							<ul class="ul1">
								<li><a href="#">프로필 사진 변경</a></li>
								<li><a href="#" onclick="history.go(0)">개인정보 조회</a></li>
								<li><a href="<c:url value = '../RS_Change' />">예약 조회 / 변경 / 취소</a></li>
								<li><a href="#">Elit sed do</a></li>
								<li><a href="#">Incididunt ut labore et</a></li>
								<li><a href="#">Lorem ipsum dolor</a></li>
								<li><a href="#">Sit amet conse ctetur</a></li>
								<li><a href="#">Ut enim ad minim</a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="bot1_wrapper">
			<div class="container">
				<div class="bot1 clearfix">
					<div class="row">
						<div class="span3">
							<div class="bot1_title">Copyright</div>
							<div class="logo2_wrapper">
								<a href="index.html" class="logo2"><img
									src="../resources/images/NPPlogo.png" alt=""></a>
							</div>
							<footer>
								<div class="copyright">
									Copyright © 2014.<br>All rights reserved.
								</div>
							</footer>
						</div>
						<div class="span5">
							<div class="bot1_title">Useful information</div>
							<p>
								<b> Nulla ultricies enim aliquet augue eleifend iaculis. </b>
							</p>
							<p>Nam sollicitudin ligula ac nisi iaculis eu scelerisque
								risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean
								leo nulla, aliquet vitae ultricies sit amet, porttitor id
								sapien. In hac habitasse platea dictumst. Donec pharetra gravida
								augue at hendrerit. Cras ut...</p>
						</div>
						<div class="span3 offset1">
							<div class="bot1_title">Follow Us</div>
							<div class="social_wrapper">
								<ul class="social clearfix">
									<li><a href="#"><img
											src="../resources/images/social_ic1.png"></a></li>
									<li><a href="#"><img
											src="../resources/images/social_ic2.png"></a></li>
									<li><a href="#"><img
											src="../resources/images/social_ic3.png"></a></li>
									<li><a href="#"><img
											src="../resources/images/social_ic4.png"></a></li>
									<li><a href="#"><img
											src="../resources/images/social_ic5.png"></a></li>
									<li><a href="#"><img
											src="../resources/images/social_ic6.png"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src='<c:url value = "../resources/js/bootstrap.js"/>'></script>
</body>
</html>