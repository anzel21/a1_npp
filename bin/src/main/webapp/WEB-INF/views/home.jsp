<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>NPP - Accommodations finder</title>
<style type="text/css">
	@import url("./resources/css/jw_main/login.css");
</style>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="resources/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="resources/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="resources/css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="resources/css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="resources/css/camera.css" type="text/css" media="screen">
<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen">
<!-- 네이버 로그인 연동 관련 JS 파일 -->
<script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.2.js" charset="utf-8"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

<script type="text/javascript" src="resources/js/jquery.js"></script>  
<script type="text/javascript" src="resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="resources/js/superfish.js"></script>
<script type="text/javascript" src="resources/js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="resources/js/camera.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile.customized.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.caroufredsel.js"></script>
<script type="text/javascript" src="resources/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="resources/js/script.js"></script>


<script>
$(document).ready(function() {
	//
	$('#camera_wrap').camera({
		//thumbnails: true
		//alignment			: 'centerRight', 
		autoAdvance			: true,		
		mobileAutoAdvance	: true,
		//fx					: 'simpleFade',
		height: '48%',
		hover: false,
		loader: 'none',
		navigation: false,
		navigationHover: false,
		mobileNavHover: false,
		playPause: false,
		pauseOnClick: false,
		pagination			: true,
		time: 7000,
		transPeriod: 1000,
		minHeight: '300px'
	});

	//	carouFredSel
	$('#slider3 .carousel.main ul').carouFredSel({
		auto: {
			timeoutDuration: 8000					
		},
		responsive: true,
		prev: '.prev3',
		next: '.next3',
		width: '100%',
		scroll: {
			items: 1,
			duration: 1000,
			easing: "easeOutExpo"
		},			
		items: {
        	width: '330',
			height: 'variable',	//	optionally resize item-height			  
			visible: {
				min: 1,
				max: 4
			}
		},
		mousewheel: false,
		swipe: {
			onMouse: true,
			onTouch: true
			}
	});
	
	
	//	뷰 숙소 슬라이더 넘기기
	$('#slider2 .carousel.main ul').carouFredSel({
		auto: {
			timeoutDuration: 8000					
		},
		responsive: true,
		prev: '.prev2',
		next: '.next2',
		width: '100%',
		scroll: {
			items: 1,
			duration: 1000,
			easing: "easeOutExpo"
		},			
		items: {
        	width: '330',
			height: 'variable',	//	optionally resize item-height			  
			visible: {
				min: 1,
				max: 4
			}
		},
		mousewheel: false,
		swipe: {
			onMouse: true,
			onTouch: true
			}
	}); 
	
	
	$(window).bind("resize",updateSizes_vat).bind("load",updateSizes_vat);
	function updateSizes_vat(){		
		$('#slider3 .carousel.main ul').trigger("updateSizes");		
	}
	updateSizes_vat();
}); //
$(window).load(function() {
	//

}); //



//JW_Login 화면 동작 기능
function openLoginPage() {
	document.getElementById('LoginDiv').style.display = 'block';
	$('#LoginDiv').css('height','460px');
	$('#LoginDiv').css('margin-left','-340px');
	$('#LoginDiv').css('margin-top','-60px');
	document.getElementById('LoginDivOverlay').style.display = 'block';
	// JW_마우스 스크롤 비활성화
	$(document)
			.on(
					"mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll",
					function(e) {
						e.preventDefault();
						return;
					});
	$(document).on("keydown.disableScroll", function(e) {
		var eventKeyArray = [ 32, 33, 34, 35, 36, 37, 38, 39, 40 ];
		for (var i = 0; i < eventKeyArray.length; i++) {
			if (e.keyCode === eventKeyArray[i]) {
				e.preventDefault();
				return;
			}
		}
	});
}
// JW_Login 화면 종료 기능
function loginOverlay() {
	document.getElementById('LoginDiv').style.display = 'none';
	document.getElementById('LoginDivOverlay').style.display = 'none';
	// JW_마우스 스크롤 활성화
	$(document).off(".disableScroll");
}

//회원가입 폼으로 이동
function goJoinForm() {
	location.href = "member/joinForm";
}

//id & pw 조회 폼으로 이동
function goFindIdPw() {
	location.href = "member/id_pwFindForm";
}

//로그인시 입력받는 데이터 Ajax & 유효성검사
function loginData() {
	var msg;
	$.ajax({
		url: 'member/loginData',
		type: 'POST',
		data: $('#loginData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "아이디를 입력해주세요." || msg == "없는 아이디 입니다.") {
				$('#login_idErrDiv').html(msg);
				$('#login_pwErrDiv').html("");
				return false;
			}
			if(msg == "비밀번호를 입력해주세요." || msg == "비밀번호가 일치하지 않습니다.") {
				$('#login_idErrDiv').html("");
				$('#login_pwErrDiv').html(msg);
				return false;
			}
			else {
				location.href ="./";
			}
		},
		error: function (e) {
			alert('저장 실패');
		}
	});
}

</script>		

</head>

<body class="front">
<!-- JW_Login 화면 동작 -->

<%@ include file = "jw_login/loginForm.jsp" %>

<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
	
<header><div class="logo_wrapper"><a href="./" class="logo"><img src="resources/images/NPPlogo.png" alt=""></a></div></header>

<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li class="active"><a href="./">Home</a></li>
<li class="sub-menu sub-menu-1"><a href="#">About</a>
	<ul>
		<li><a href="VR_media">VR View</a></li>
		<li class="sub-menu sub-menu-2"><a href="Pano_media">Pano View<em></em></a></li>
		<li class="sub-menu sub-menu-2"><a href="#">history<em></em></a></li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="mychart">services</a></li>

<li><a href="accmmolist">Accommodations</a></li>

<c:if test="${loginId == null}">											
	<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
	<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>		
</c:if>


<c:if test="${loginId != null}">
	<li class="sub-menu sub-menu-1"><a href="member/userInfoForm">MY PAGE</a>
		<ul>
			<li><a href="member/userInfoForm">Account info</a></li>
			<li><a href="hostAcco/selectType">Start Hosting</a></li>
			<li><a href="<c:url value = "/RS_Change" />">Reservation<br>Confirmation</a></li>
			<li><a href="member/logout">Logout</a></li>
		</ul>
	</li>
</c:if>	
						
    </ul>
		</div>
	</div>
</div>
<div id="search">
	
<a href="#" class="searchBtn"></a>

<!-- 검색부분인듯 하오다  -->
<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<div id="inner">

<div class="find_wrapper">

<div class="find_inner">
<div class="find">
	
<div class="txt1">welcome to</div>

<div class="txt2">NPP</div>

<div class="txt3">accommodations</div>
<div class="txt3">finder</div>

<div class="line"></div>

<div class="txt4">THE EASIEST WAY TO FIND PROPERTY</div>

<div class="find-form-wrapper clearfix">
  <form id="find-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form clearfix" >
    <input type="text" name="s" value='keyword' onBlur="if(this.value=='') this.value='keyword'" onFocus="if(this.value =='keyword' ) this.value=''">
    <a href="#" onClick="document.getElementById('find-form').submit()"></a>
  </form>
</div>


</div>	
</div>

<div id="slider_wrapper">
	<div id="slider" class="clearfix">
		<div id="camera_wrap">
			<div data-src="resources/images/slide01.jpg"></div>
			<div data-src="resources/images/slide02.jpg"></div>													
			<div data-src="resources/images/slide03.jpg"></div>
			<div data-src="resources/images/slide04.jpg"></div>
			<div data-src="resources/images/slide05.jpg"></div>
		</div>	
	</div>		
</div>	
</div>	


<br>
<br>


<!-- 숙소정보 출력 div -->


<div id="slider3_wrapper">
<div class="container">

<div class="slider3_title"><span>Accommodations</span></div>	

<div id="slider3">
<a class="prev3" href="#"></a>
<a class="next3" href="#"></a>		
<div class="carousel-box row">
	<div class="inner span12">			
		<div class="carousel main">
			<ul>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel01.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$570.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel02.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$240.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel03.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$480.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel04.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$120.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel05.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$730.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>
				<li>
					<div class="offer">
						<div class="offer_inner">
							<a href="#">
								<figure><img src="resources/images/carousel06.jpg" alt="" class="img"></figure>
								<div class="caption">
									<div class="txt1">LOREM IPSUM DOL AMET</div>
									<div class="txt2">Lorem ipsum dolor sit amet, conse etur et adipiscing elit. Duis vel nisifes. Vestibulum ullamcorper dolore ipsum.</div>
									<div class="txt3">$470.000</div>
									<div class="txt4">Read More</div>
								</div>																
							</a>
						</div>						
					</div>
				</li>																									
			</ul>
		</div>
	</div>
</div>
</div>

</div>	
</div>



<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="resources/images/NPPlogo.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2018.<br>NPP FINAL PROJECT</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>
<div class="span3 offset1">

<div class="bot1_title">Follow Us</div>
	
<div class="social_wrapper">
	<ul class="social clearfix">    
    <li><a href="#"><img src="resources/images/social_ic1.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic2.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic3.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic4.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic5.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic6.png"></a></li>
	</ul>
</div>

</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>

<script type="text/javascript" src="resources/js/bootstrap.js"></script>
</body>
</html>
<!-- . Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>
<div class="span3 offset1">

<div class="bot1_title">Follow Us</div>
	
<div class="social_wrapper">
	<ul class="social clearfix">    
    <li><a href="#"><img src="resources/images/social_ic1.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic2.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic3.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic4.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic5.png"></a></li>
    <li><a href="#"><img src="resources/images/social_ic6.png"></a></li>
	</ul>
</div>

</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="resources/js/bootstrap.js"></script>
</body>
</html> -->