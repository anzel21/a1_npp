<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE=html>
<html>
<head>
<meta charset='UTF-8' />
<title>FullCalendar</title>

<link rel='stylesheet'
	href="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.css' />" />
<script src="<c:url value = '/resources/kjw/lib/jquery-3.3.1.min.js' />"></script>
<script src="<c:url value = '/resources/kjw/lib/moment.min.js' />"></script>

<%-- FullCalendar API 및 한국어 로케일 로딩 --%>

<link rel='stylesheet'
	href="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.css' />" />
<script src="<c:url value = '/resources/kjw/lib/jquery-3.3.1.min.js' />"></script>
<script src="<c:url value = '/resources/kjw/lib/moment.min.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/locale/ko.js' />"></script>
<script>
	var calendarSwitch = 0;
	var guestSwitch = 0;
	var selectedGuestNum = 0;
	var adultGuestNum = 0;
	var kidGuestNum = 0;
	var newEvent;
	var listforOneRoom;
	
	$(document).ready(function() {
		$('#calendar').hide();
		$('#guestNumber').hide();
		calendarSwitch = 0;
		$('#selectedguestnum').html(selectedGuestNum);
		$('#adultGuestNum').val(adultGuestNum);
		$('#kidGuestNum').val(kidGuestNum);
		$('#startDate').html('체크인');
		$('#endDate').html('체크아웃');
	});
	
	
	// 예약일정에 따른 캘린더 생성
	function makeCalendar(listforOneRoom) {
		$('#calendar').fullCalendar({
			locale : 'ko', // 요일 및 년/월 한글로 표시되게 설정. 한글표기 되려면 API Lib 임포트해야함.

			header : {
				left : 'prev', //지난달로 이동하는 버튼
				center : 'title', //XXXX년 XX월 표시 
				right : 'next' //다음달로 이동하는 버튼
			},

			showNonCurrentDates : false, // 해당 월 아니면 일자 표시가 안되게 설정
			fixedWeekCount : false,

			events : listforOneRoom, // JSON 형식으로 받아온 자료를, API event 형식으로 표시시켜줌.
			selectConstraint : listforOneRoom,
			//rendering : 'background',
			//color : '#ff0000',
			displayEventTime : false, // 예약일만 표시되고 시간은 표시안되게 하는 기능
			selectable : true, //선택 기능 부여
			unselectAuto : true,
			validRange : function(nowDate) {
				var calStartDate = moment(nowDate.clone()).format('YYYY-MM-01');
				var calEndDate = moment(nowDate.clone().add(2, 'years')).format('YYYY-MM');
				return {
					start : calStartDate,
					end : calEndDate
				};
			}, // 이번달부터 2년 후까지만 달력이 표시되게 설정함.
			select : function(startDate, endDate) {
				$('#startDate').html("   " + startDate.format());
				$('#endDate').html("   " + endDate.format());
				newEvent = {
					start : '' + startDate.format(),
					end : '' + endDate.format(),
					overlap : '' + "false",
					rendering : "" + 'background',
					color : "" + '#00ff00'
				}; // 객체로 만들어서,
				if (listforOneRoom[listforOneRoom.length - 1].color == '#00ff00') {
					listforOneRoom.pop();
					reloadCalendar();
				} else {
					listforOneRoom.push(newEvent); // JSON으로 불러온 기존 리스트에 더해준다.
					$('#calendar').fullCalendar('renderEvents', listforOneRoom); // 일정 선택 시 해당 기간 표시해 줌.
				}
			},
			selectOverlap : false

		});
	}

	function reloadCalendar() {
		$('#calendar').fullCalendar('destroy');
		loadList();
	}

	function clearCalendar() {
		$('#calendar').fullCalendar('destroy');
		newEvent = null;
		$('#startDate').html('체크인');
		$('#endDate').html('체크아웃');
		loadList();
	}

	function checkdateClicked() {
		if (calendarSwitch == 0) {
			calendarSwitch = 1;
			$('#calendar').show();
		} else {
			calendarSwitch = 0;
			$('#calendar').hide();
		}
	}

	function selectguestnum() {
		if (guestSwitch == 0) {
			guestSwitch = 1;
			$('#guestNumber').show();
		} else {
			guestSwitch = 0;
			$('#guestNumber').hide();
		}
	}

	function minusAdultGuest() {
		adultGuestNum--;
		if (adultGuestNum <= 0)
			adultGuestNum = 0;
		$('#adultGuestNum').val(adultGuestNum);
		recalculateGuestNum();
	}

	function plusAdultGuest() {
		adultGuestNum++;
		$('#adultGuestNum').val(adultGuestNum);
		recalculateGuestNum();
	}

	function minusKidGuest() {
		kidGuestNum--;
		if (kidGuestNum <= 0)
			kidGuestNum = 0;
		$('#kidGuestNum').val(kidGuestNum);
		recalculateGuestNum();
	}

	function plusKidGuest() {
		kidGuestNum++;
		$('#kidGuestNum').val(kidGuestNum);
		recalculateGuestNum();
	}

	function recalculateGuestNum() {
		selectedGuestNum = kidGuestNum + adultGuestNum;
		$('#selectedguestnum').html(selectedGuestNum);
	}

	function goReservation() {
		
		if (newEvent != null) {
			
			$.ajax({
				url : '<c:url value = "/setReserve" />',
				type : 'post',
				data : newEvent,
				dataType : 'json',
				success : function(x) {
					alert("신규 예약 추가 성공");
				},

				error : function(e) {
					JSON.stringify();
				}
			});

		} else
			alert('기간을 선택하지 않았습니다.');
		
	}
	
</script>

<style>
.fc-center h2 {
	float: none;
	font-style: normal;
	font-size: medium;
	font-size-adjust: none;
	text-decoration: none;
}

#calendar {
	margin-top: 15px;
	width: 340px;
	height: 280px;
	font-size-adjust: none;
	font-size: 8pt;
	text-align: left;
	width: 320px;
}

#selectingDate {
	width: 340px;
	height: 420px;
	text-align: left;
	border: solid, black, 1px;
}

#selectingDateTable {
	width: 340px;
	color: gray;
}

.date {
	width: 80px;
	text-align: right;
	border-color: grey;
	border-style: solid;
	font-size: 8pt;
	float: left;
}

.numofPeople {
	margin-top: 5px;
	border: none;
}
</style>

</head>


<body>


	<div id='selectingDate'>
		<%-- FullCalendar API 내부 명령으로 폰트 사이즈 축소가 어려우므로, 
		외부 div 혹은 span에 CSS로  Font 크기 강제 조정해야함.--%>

		<table style="background-color: white;">

			<tr>
				<td><small> 가격: 서버에서 불러올 값<br> 별점 *****(별점 게시한 사람
						수)
				</small></td>
			</tr>

			<tr style="font-size: 8pt;">
				<td><small>날짜</small><br>

					<div style="border-color: grey; border-style: solid; width: 325px;">
						<table>
							<tr>
								<td style="width: 270px"><small>
										<div id="startDate" class="date"></div>
								</small>
									<div class="date"
										style="border: none; text-align: center; font-size: 14pt; font-weight: bolder;">→</div>
									<small>
										<div id="endDate" class="date"></div>
								</small></td>

								<td><image
										src="<c:url value = '/resources/kjw/magnifier.png' />"
										style="width:16px; height:16px;"
										onclick='javascript:checkdateClicked()'> <image
										src="<c:url value = '/resources/kjw/cancel_red.png' />"
										style="margin-left:10px; width:16px; height:16px;"
										onclick='javascript:clearCalendar()'></td>

							</tr>
						</table>
					</div> <small>
						<div id='calendar'></div>
				</small></td>
			</tr>

			<tr>
				<td><small>인원</small><br>
					<div style="border-color: grey; border-style: solid; width: 325px;">
						<table>
							<tr>
								<td style="width: 280px;"><small>게스트 <span
										id="selectedguestnum"></span>명
								</small></td>
								<td><image
										style=" margin-left:20px; width:20px; height:20px;"
										src="<c:url value = '/resources/kjw/arrow_under.png' />"
										onclick='javascript:selectguestnum()'></td>

							</tr>
						</table>
					</div>

					<div id='guestNumber'
						style="border-color: grey; border-style: solid; background-color: white; width: 325px;">
						<table>



							<tr>
								<td style="width: 150px;"><small>성인</small></td>
								<td style="width: 50px;"><image
										style="width:25px; height:25px"
										src="<c:url value = '/resources/kjw/minus.png' />"
										onclick='javascript:minusAdultGuest()'></image></td>
								<td style="text-align: right; width: 50px;"><input
									id="adultGuestNum" class="numofPeople" type="text"
									style="width: 30px"></td>
								<td style="width: 50px; text-align: right;"><image
										style="width:25px; height:25px"
										src="<c:url value = '/resources/kjw/plus.png' />"
										onclick='javascript:plusAdultGuest()'></image></td>
							</tr>

							<tr>
								<td style="width: 150px;"><small>어린이</small></td>
								<td style="width: 50px;"><image
										style="width:25px; height:25px"
										src="<c:url value = '/resources/kjw/minus.png' />"
										onclick='javascript:minusKidGuest()'></image></td>
								<td style="text-align: right; width: 50px;"><input
									id="kidGuestNum" class="numofPeople" type="text"
									style="width: 30px"></td>
								<td style="width: 50px; text-align: right;"><image
										style="width:25px; height:25px"
										src="<c:url value = '/resources/kjw/plus.png' />"
										onclick='javascript:plusKidGuest()'></image></td>
							</tr>
						</table>

						<small>최대 $()명 숙박 가능합니다.</small>

					</div></td>
			</tr>

			<tr style="height: 10px;"></tr>
			<tr>
				<td>
					<div style="width: 325px; height: 20px;">
						<div
							style="margin: 0 auto; color: white; text-align: center; width: 300px; background-color: purple;"
							onclick="javascript:goReservation()">
							<small style="font-size: 8pt;">예약하기</small>
						</div>
					</div>
				</td>
			</tr>

		</table>

	</div>

</body>
</html>

