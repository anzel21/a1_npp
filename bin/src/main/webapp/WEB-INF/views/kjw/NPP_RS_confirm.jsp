<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE=html>
<html>
<head>
<meta charset='UTF-8' />
<title>FullCalendar</title>

<link rel='stylesheet'
	href='./resources/kjw/fullcalendar/fullcalendar.css' />
<script src='./resources/kjw/lib/jquery-3.3.1.min.js'></script>
<script src='./resources/kjw/lib/moment.min.js'></script>



<%-- FullCalendar API 및 한국어 로케일 로딩 --%>
<script src='./resources/kjw/fullcalendar/fullcalendar.js'></script>
<script src='./resources/kjw/fullcalendar/locale/ko.js'></script>
<script>
	$(document).ready(function() {
		//DB에서 전체 예약 일정 불러옴
		$.ajax({
			url : './getreserveList',
			type : 'POST',
			dataType : 'json',
			success : function(list) {
				makeCalendar(list);
			},
			error : function(e) {
				JSON.stringify();
			},
		});

		// 예약일정에 따른 캘린더 생성
		function makeCalendar(list) {
			$('#calendar').fullCalendar({
				locale : 'ko', // 요일 및 년/월 한글로 표시되게 설정. 한글표기 되려면 API Lib 임포트해야함.
				header : {
					left : 'prev', //지난달로 이동하는 버튼
					center : 'title', //XXXX년 XX월 표시 
					right : 'next' //다음달로 이동하는 버튼
				},
				showNonCurrentDates : false, // 해당 월 아니면 일자 표시가 안되게 설정
				fixedWeekCount : false,
				events : list, // JSON 형식으로 받아온 자료를, API event 형식으로 표시시켜줌.
				selectConstraint : list,
				validRange : function(nowDate) {
					var calStartDate = moment(nowDate.clone()).format('YYYY-MM-01');
					var calEndDate = moment(nowDate.clone().add(2, 'years')).format('YYYY-MM');
					return {
						start : calStartDate,
						end : calEndDate
					};
				}, // 이번달부터 2년 후까지만 달력이 표시되게 설정함.
				displayEventTime : false
			// 예약일만 표시되고 시간은 표시안되게 하는 기능
			});
		}
	});
</script>

<style>
#EEE {
	width: 320px;
	height: 480px;
	font-size: 8pt;
	text-align: center;
}
</style>

</head>
<body>
	<div id='EEE'>
		<%-- FullCalendar API 내부 명령으로 폰트 사이즈 축소가 어려우므로, 
		외부 div 혹은 span에 CSS로  Font 크기 강제 조정해야함.--%>
		<div id='calendar'></div>
	</div>

</body>
</html>