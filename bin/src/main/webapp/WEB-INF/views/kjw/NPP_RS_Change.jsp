<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>contacts</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" />
	type="image/x-icon">
<link rel="shortcut icon"
	href=<c:url value = "/resources/images/favicon.ico"/>
	type="image/x-icon" />

<link rel="stylesheet"
	href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css"
	media="screen">
<link rel="stylesheet"
	href=<c:url value = "/resources/css/bootstrap-responsive.css" />
	type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" />
	type="text/css" media="screen">

<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.js" />></script>
<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.easing.1.3.js"/>></script>
<script type="text/javascript"
	src=<c:url value = "/resources/js/superfish.js"/>></script>

<script type="text/javascript"
	src=<c:url value = "/resources/js/jquery.ui.totop.js" />></script>

<script type="text/javascript"
	src=<c:url value = "/resources/js/script.js"/>></script>


<link rel='stylesheet'
	href="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.css' />" />
<script src="<c:url value = '/resources/kjw/lib/moment.min.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/fullcalendar.js' />"></script>
<script
	src="<c:url value = '/resources/kjw/fullcalendar/locale/ko.js' />"></script>
<script>
	var newEvent = null;
	var originalEvent = null;
	var listforGuest =null;
	var newlistforGuest = null;
	var xx =  null;
	
	var repairIcon = '<c:url value = "/resources/kjw/repair.png"/>';
	var canIcon = '<c:url value = "/resources/kjw/cancel_red.png"/>';
	var selectedNum_outSide;
	var selectedRoomNum_outSide;
	
	function printlistForGuest(listforGuest){
		xx =  "<div class = 'reservedRoom' style = 'padding-top: 10px;'>";
		for (var i = 0; i < listforGuest.length; i++) {
			xx = xx	+ "<table style = 'border: black solid thin; margin-left:5px; width:240px;'>"
					+ "<tr><td style = 'width:150px;'>"+ listforGuest[i].subscriberID +"</td>"
					+ "<td class = 'iconTD' rowspan = '2'>" + "<img style = 'width:25px;' onclick='javascript:changeReservation("+ listforGuest[i].reservationNum + "," + listforGuest[i].roomID +")' src = '" + repairIcon + "'/>"
					+"</td>" + "<td class = 'iconTD' rowspan = '2'>" + "<img onclick='javascript:cancelReservation(" + listforGuest[i].reservationNum +" )' style = 'width:25px;' src = ' " + canIcon + "'/>"
					+"</td></tr>"
					+ "<tr'><td>" + listforGuest[i].startDate + "~"+ listforGuest[i].endDate + "</td></tr></table>";
					xx = xx + "<table><tr height = '15px'><td></td></tr></table>";
		}
		
		xx = xx + "</div>";
		$('#reservedList').html(xx);
	}
	
	function changeReservation(selectedNum,selectedRoomNum){
		selectedNum_outSide = selectedNum;
		selectedRoomNum_outSide = selectedRoomNum;
		$('#reseravtionNUM_selected').html(" 예약번호: " + selectedNum_outSide);
		$('#calendar').fullCalendar('destroy');
		loadList();
	}
	
	
	function loadList(){
		$.ajax({
			url : '<c:url value = "/createCalforChange" />',
			type : 'post',
			data : { selectedNum_outSide, selectedRoomNum_outSide },
			dataType : 'json',
			success : function(listforOneRoom) {
				originalEvent = listforOneRoom[listforOneRoom.length - 1];
				makeCalendar(listforOneRoom);
				$('#originalStart').html(originalEvent.start); 
				$('#originalEnd').html(originalEvent.end);
				$('#updatedStart').html(""); 
				$('#updatedEnd').html("");
				$('#calendar').show();
				$('#intelDIV').show();
				$('#selectBoxforReservation').show();
			},
			error : function(e) {
				JSON.stringify();
			}
		});
	}
	
		function makeCalendar(listforOneRoom) {
			$('#calendar').fullCalendar({
				locale : 'ko', // 요일 및 년/월 한글로 표시되게 설정. 한글표기 되려면 API Lib 임포트해야함.
				defaultDate : listforOneRoom[listforOneRoom.length - 1].start ,
				header : {
					left : 'prev', //지난달로 이동하는 버튼
					center : 'title', //XXXX년 XX월 표시 
					right : 'next' //다음달로 이동하는 버튼
				},

				showNonCurrentDates : false, // 해당 월 아니면 일자 표시가 안되게 설정
				fixedWeekCount : false,

				events : listforOneRoom, // JSON 형식으로 받아온 자료를, API event 형식으로 표시시켜줌.
				selectConstraint : listforOneRoom,
				//rendering : 'background',
				//color : '#ff0000',
				displayEventTime : false, // 예약일만 표시되고 시간은 표시안되게 하는 기능
				selectable : true, //선택 기능 부여
				unselectAuto : true,
				validRange : function(nowDate) {
					var calStartDate = moment(nowDate.clone()).format('YYYY-MM-01');
					var calEndDate = moment(nowDate.clone().add(2, 'years')).format('YYYY-MM');
					return {
						start : calStartDate,
						end : calEndDate
					};
				}, // 이번달부터 2년 후까지만 달력이 표시되게 설정함.
				
				select : function(startDate, endDate) {
					if(confirm("예약을 " + startDate.format() + " ~ " + endDate.format() + "로 변경합니까?")){
						newEvent = {
							start : '' + startDate.format(),
							end : '' + endDate.format(),
							overlap : '' + "false",
							rendering : "" + 'background',
							color : "" + '#00ff00'
						}; // 객체로 만들어서,
						
						if (listforOneRoom[listforOneRoom.length - 1].color == '#00ff00') {
							listforOneRoom.pop();
							originalEvent.color = "" + "#ffab00";
							listforOneRoom.push(originalEvent);
							listforOneRoom.push(newEvent); // JSON으로 불러온 기존 리스트에 더해준다.
							newlistforGuest = listforOneRoom;
							$('#updatedStart').html(newEvent.start); 
							$('#updatedEnd').html(newEvent.end);
							reloadCalendar();
						}
						
					}
					
				},
				selectOverlap : false
			});
		}
		
		
		function reloadCalendar() {
			$('#calendar').fullCalendar('destroy');
			makeCalendar(newlistforGuest);
		}
	
		
	function cancelReservation(selectedNum){
		var x = selectedNum;
		if(confirm(x+'번 예약을 취소하시겠습니까?')){
			$.ajax({
				url : '<c:url value = "/cancelReservation" />',
				type : 'post',
				data : { x },
				dataType : 'json',
				success : function(res) {
					if(res==1) {
						alert("예약이 취소되었습니다.");
						loadListforGuest();
						printlistForGuest(listforGuest);
					}
					else alert("[오류]예약취소 실패");
				},
				error : function(e) {
					JSON.stringify();
				}
			});
			
		}
		return;
	}
		
	function updateReservation(){
		
		if(confirm('새로 선택한 기간으로 예약을 변경하시겠습니까?')){
			newEvent.reservationNum = selectedNum_outSide;
			$.ajax({
				url : '<c:url value = "/updateReservation" />',
				type : 'post',
				data : newEvent,
				dataType : 'json',
				success : function(res) {
					if(res==1) {
						alert("예약이 갱신되었습니다.");
						var empty = " ";
						$('#reservedList').html(empty); // UI의 우측 스크롤바 리스트 비움.
						loadListforGuest(); //DB에서 전체 예약 일정 불러옴
						changeReservation(selectedNum_outSide,selectedRoomNum_outSide);
					}
					else alert("[오류]예약갱신 실패");
				},
				error : function(e) {
					JSON.stringify();
				}
			});
			
		}
		
		
	} 
	
	function cancelChange(){
		if(confirm("예약변경을 취소합니까?")){
			$('#reseravtionNUM_selected').html("");			
			$('#originalStart').html(""); 
			$('#originalEnd').html("");
			$('#updatedStart').html(""); 
			$('#updatedEnd').html("");
			$('#reservationChange').hide();
			$('#calendar').hide();
			$('#intelDIV').hide();
			$('#selectBoxforReservation').hide();
			$('#calendar').fullCalendar('destroy');
		}
		else return;
	}
	
	function loadListforGuest() {
		listforGuest = null;
		$.ajax({
					url : "<c:url value = '/getreserveListforGuest' />",
					type : 'GET',
					dataType : 'json',
					success : function(listforGuest) {
						printlistForGuest(listforGuest);
					},
					error : function(e) {
						JSON.stringify();
					},
				});
	}
	
	$(document).ready(function() {
		$('#reservationChange').hide();
		$('#calendar').hide();
		$('#intelDIV').hide();
		$('#selectBoxforReservation').hide();
		loadListforGuest(); //DB에서 전체 예약 일정 불러옴
	});

	$(window).load(function() {
		
	}); //
	
	
</script>

<style>
.iconTD {
	text-align: center;
}

.fc-center h2 {
	float: none;
	font-style: normal;
	font-size: medium;
	font-size-adjust: none;
	text-decoration: none;
}

#calendar {
	padding-top: 20px;
	margin-left : 5px;
	margin-top: 10px;
	width: 53%;
	height: 280px;
	font-size-adjust: none;
	font-size: 8pt;
	text-align: left;
	background-color: rgba(255, 255, 255, 0.9);
	margin-left: 5px;
}

#selectBoxforReservation {
	float: left;
	margin-left: 5px;
	width: 53%;
	height: 80px;
	background-color: rgba(255, 255, 255, 0.9);
	text-align: center;
}

.selectButton {
	width: 55px;
	height: 55px;
}

#intelDIV {
	margin-left: 5px;
	margin-top: 10px;
	margin-right: 5px;
	height: 380px;
	width: 44%;
	background-color: rgba(255, 255, 255, 0.8);
}

#intelContainer {
	background-image: url("./resources/kjw/divback.jpg");
	background-repeat: no-repeat;
	background-size: cover;
}

.dateform {
	border: 1px;
	border-color: gray;
	border-style: solid;
	float: left;
	height: 20px;
	width: 80px;
	background-color: rgba(255, 255, 255, 0.9);
}
</style>



<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
	<div id="main">

		<div class="top1_wrapper">
			<div class="top1 clearfix">

				<header>
					<div class="logo_wrapper">
						<a href="index.html" class="logo"><img src="<c:url value = '/resources/images/logo.png'/>"
							alt=""></a>
							
					</div>
				</header>

				<div class="menu_wrapper clearfix">
					<div class="navbar navbar_">


						<div class="navbar-inner navbar-inner_">
							<a class="btn btn-navbar btn-navbar_" data-toggle="collapse"
								data-target=".nav-collapse_"> <span class="icon-bar"></span>
								<span class="icon-bar"></span> <span class="icon-bar"></span>
							</a>

							<div class="nav-collapse nav-collapse_ collapse">
								<ul class="nav sf-menu clearfix">
									<li class="active"><a href="./">home</a></li>
									<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
										<ul>
											<li><a href="index-1.html">who we are</a></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">find
													an agent<em></em>
											</a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
												<ul>
													<li><a href="index-1.html">Lorem ipsum dolor</a></li>
													<li><a href="index-1.html">Sit amet</a></li>
													<li><a href="index-1.html">Adipiscing elit</a></li>
													<li><a href="index-1.html">Nunc suscipit</a></li>
													<li><a href="404.html">404 page not found</a></li>
												</ul></li>
											<li><a href="index-1.html">testimonials</a></li>
										</ul></li>
									<li><a href="VR_media">services</a></li>
									<li><a href="index-3.html">ACCOMMODATIONS</a></li>

									<c:if test="${loginId == null}">
										<li><a href="#" onclick="openLoginPage()">LOGIN</a></li>
										<li><a href="javascript:void(0)" onclick="goJoinForm()">JOIN</a></li>
									</c:if>
									
									<c:if test="${loginId != null}">
										<li class="sub-menu sub-menu-1"><a href="./member/userInfoForm">MY
												PAGE</a>
											<ul>
												<li><a href='./member/userInfoForm'>Account info</a></li>
												<li><a href="<c:url value = '/hostAcco/selectType' />">Start Hosting</a></li>
												<%-- 예약변경/조회 --%>
												<li><a href="<c:url value = '/RS_Change' /> ">RESERVATION<br>CONFIRMATION
												<li><a href="<c:url value = '/member/logout' />">LOG OUT</a></li>
											</ul></li>
									</c:if>
								</ul>
							</div>
						</div>
					</div>
					<div id="search">

						<a href="#" class="searchBtn"></a>

						<div class="search-form-wrapper">
							<form id="search-form" action="search.php" method="GET"
								accept-charset="utf-8" class="navbar-form">
								<input type="text" name="s" value='Search'
									onBlur="if(this.value=='') this.value='Search'"
									onFocus="if(this.value =='Search' ) this.value=''"> <a
									href="#"
									onClick="document.getElementById('search-form').submit()"></a>
							</form>
						</div>

					</div>
				</div>

			</div>
		</div>

		<div id="inner">

			<div class="top2_wrapper">
				<div class="bg1">
					<img src="<c:url value = '/resources/images/bg1.jpg'/>" alt="" class="img">
				</div>
				<div class="top2_inner">
					<div class="container">
						<div class="top2 clearfix">
							<h1>My Page</h1>
							<div class="breadcrumbs1">
								<a href="index.html">Home</a><span></span><a href="index.html">Pages</a><span></span>Contacts
							</div>
						</div>
					</div>
				</div>
			</div>


			<div id="content">

				<div class="container">
					<div class="row">


						<div class="span9">
							<h2>
								<span>예약 조회 / 변경 / 취소</span>
							</h2>

							<div id="reservedList"
								style="float: left; height: 400px; width: 30%; border: thin; border-color: gray; border-style: solid; overflow-y: scroll;">
							</div>

							<div id="intelContainer"
								style="float: left; height: 400px; width: 69%; border: thin; border-color: gray; border-style: solid;">

								<div id="calendar" style="float: left;"></div>

								<div id="intelDIV" style="float: right;">
									<br>
									<table style="width: 200px;">
										<tr>
											<td><div id="reseravtionNUM_selected"></td>
										</tr>
										<tr>
											<td>
												</div>
											</td>
										</tr>
										<tr>
											<td>기존 체크인 ~ 체크아웃 기간</td>
										</tr>
										<tr>
											<td><div class='dateform' id="originalStart"></div>
												<div
													style="width: 20px; text-align: center; background-color: transparent; border: none;"
													class='dateform'>~</div>
												<div class='dateform' id="originalEnd"></div></td>
										</tr>
										<tr height="10px"></tr>

										<tr>
											<td>신규 체크인 ~ 체크아웃 기간</td>
										</tr>

										<tr>
											<td>
												<div class='dateform' id="updatedStart"></div>

												<div
													style="width: 20px; text-align: center; background-color: transparent; border: none;"
													class='dateform'>~</div>

												<div class='dateform' id="updatedEnd"></div>
											</td>
										</tr>

									</table>
								</div>

								<div id="selectBoxforReservation">
									<img class="selectButton" style="margin-right: 40px;"
										src="<c:url value = '/resources/kjw/check_green.png' />"
										onclick="javascript:updateReservation();"> <img
										class="selectButton" style="margin-left: 40px;"
										src="<c:url value = '/resources/kjw/cancel_red.png' />"
										onclick="javascript:cancelChange();">
								</div>

							</div>

						</div>



						<div class="span3">
							<h2>
								<span>My page Menu</span>
							</h2>

							<div style="text-align: center; float: none;">
								<img src="<c:url value = '/resources/kjw/btf.png' />"> <br>

								<c:if test="${loginId != null}">
									${loginId}
								</c:if>
							</div>

							<ul class="ul1">
								<li><a href="#">프로필 사진 변경</a></li>
								<li><a href="./member/userInfoForm">개인정보 조회</a></li>
								<li><a href="<c:url value = '/RS_Change' />">예약 조회 / 변경 / 취소</a></li>
								<li><a href="#">Elit sed do</a></li>
								<li><a href="#">Incididunt ut labore et</a></li>
								<li><a href="#">Lorem ipsum dolor</a></li>
								<li><a href="#">Sit amet conse ctetur</a></li>
								<li><a href="#">Ut enim ad minim</a></li>
							</ul>

						</div>
					</div>

				</div>

			</div>
		</div>

		<div class="bot1_wrapper">
			<div class="container">
				<div class="bot1 clearfix">
					<div class="row">
						<div class="span3">

							<div class="bot1_title">Copyright</div>

							<div class="logo2_wrapper">
								<a href="index.html" class="logo2"><img
									src="<c:url value = '/resources/images/logo2.png'/>" alt=""></a>
							</div>

							<footer>
								<div class="copyright">
									Copyright © 2014.<br>All rights reserved.
								</div>
							</footer>

						</div>
						<div class="span5">

							<div class="bot1_title">Useful information</div>

							<p>
								<b> Nulla ultricies enim aliquet augue eleifend iaculis. </b>
							</p>

							<p>Nam sollicitudin ligula ac nisi iaculis eu scelerisque
								risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean
								leo nulla, aliquet vitae ultricies sit amet, porttitor id
								sapien. In hac habitasse platea dictumst. Donec pharetra gravida
								augue at hendrerit. Cras ut...</p>

						</div>
						<div class="span3 offset1">

							<div class="bot1_title">Follow Us</div>

							<div class="social_wrapper">
								<ul class="social clearfix">
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic1.png'/>"></a></li>
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic2.png'/>"></a></li>
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic3.png'/>"></a></li>
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic4.png'/>"></a></li>
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic5.png'/>"></a></li>
									<li><a href="#"><img src="<c:url value = '/resources/images/social_ic6.png'/>"></a></li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>
	<script type="text/javascript"
		src='<c:url value = "/resources/js/bootstrap.js"/>'></script>



</body>
</html>