<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>숙소 사진</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script type="text/javascript">
       $(document).ready(function(){
           var objDragAndDrop = $(".dragAndDropDiv");
           var rowCount=0;
            
           $(document).on("dragenter",".dragAndDropDiv",function(e){
               e.stopPropagation();
               e.preventDefault();
               $(this).css('border', '2px solid #0B85A1');
           });
           $(document).on("dragover",".dragAndDropDiv",function(e){
               e.stopPropagation();
               e.preventDefault();
           });
           $(document).on("drop",".dragAndDropDiv",function(e){
                
               $(this).css('border', '2px dotted #0B85A1');
               e.preventDefault();
               var files = e.originalEvent.dataTransfer.files;
               
               if (files.length + rowCount > 6) {
               	alert('사진은 최대 6장까지만 등록 가능합니다');
               	return false;
               }
            
               handleFileUpload(files,objDragAndDrop);
           });
            
           $(document).on('dragenter', function (e){
               e.stopPropagation();
               e.preventDefault();
           });
           $(document).on('dragover', function (e){
             e.stopPropagation();
             e.preventDefault();
             objDragAndDrop.css('border', '2px dotted #0B85A1');
           });
           $(document).on('drop', function (e){
               e.stopPropagation();
               e.preventDefault();
           });
            
           
           function handleFileUpload(files,obj)
           {
           	
				if (rowCount > 7) {
					alert('사진은 최대 6장까지만 등록 가능합니다');
					return false;
				}
           	
              for (var i = 0; i < files.length; i++) 
              {
                   var fd = new FormData();
                   fd.append('file', files[i]);
             
                   var status = new createStatusbar(obj); //Using this we can set progress.
                   status.setFileNameSize(files[i].name,files[i].size);
                   sendFileToServer(fd,status);
             
              }
           }
            
           function createStatusbar(obj){
                    
               rowCount++;
               var row="odd";
               if(rowCount %2 == 0) row ="even";
               this.statusbar = $("<div class='statusbar "+row+"'></div>");
               this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
               this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
               this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
               this.abort = $("<div class='abort'>삭제</div>").appendTo(this.statusbar);
                
               obj.after(this.statusbar);
             
               this.setFileNameSize = function(name,size){
                   var sizeStr="";
                   var sizeKB = size/1024;
                   if(parseInt(sizeKB) > 1024){
                       var sizeMB = sizeKB/1024;
                       sizeStr = sizeMB.toFixed(2)+" MB";
                   }else{
                       sizeStr = sizeKB.toFixed(2)+" KB";
                   }
             
                   this.filename.html(name);
                   this.size.html(sizeStr);
               }
                
               this.setProgress = function(progress){       
                   var progressBarWidth =progress*this.progressBar.width()/ 100;  
                   this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
               }
                
               this.setAbort = function(jqxhr){
                   var sb = this.statusbar;
                   this.abort.click(function()
                   {
                       jqxhr.abort();
                       sb.hide();
                       rowCount--;
                   });
               }
               
           }
            
           function sendFileToServer(formData,status)
           {
               var uploadURL = "fileUpload/post"; //Upload URL
               var extraData ={}; //Extra Data.
               var jqXHR=$.ajax({
                       xhr: function() {
                       var xhrobj = $.ajaxSettings.xhr();
                       if (xhrobj.upload) {
                               xhrobj.upload.addEventListener('progress', function(event) {
                                   var percent = 0;
                                   var position = event.loaded || event.position;
                                   var total = event.total;
                                   if (event.lengthComputable) {
                                       percent = Math.ceil(position / total * 100);
                                   }
                                   //Set progress
                                   status.setProgress(percent);
                               }, false);
                           }
                       return xhrobj;
                   },
                   url: uploadURL,
                   type: "POST",
                   contentType:false,
                   processData: false,
                   cache: false,
                   data: formData,
                   success: function(data){
                       status.setProgress(100);
             
                       //$("#status1").append("File upload Done<br>");           
                   }
               }); 
             
               status.setAbort(jqXHR);
           }
            
       });
</script>
<style>
    .dragAndDropDiv {
        border: 2px dashed #92AAB0;
        width: 650px;
        height: 200px;
        color: #92AAB0;
        text-align: center;
        vertical-align: middle;
        padding: 10px 0px 10px 10px;
        font-size:200%;
        display: table-cell;
    }
    .progressBar {
        width: 200px;
        height: 22px;
        border: 1px solid #ddd;
        border-radius: 5px; 
        overflow: hidden;
        display:inline-block;
        margin:0px 10px 5px 5px;
        vertical-align:top;
    }
      
    .progressBar div {
        height: 100%;
        color: #fff;
        text-align: right;
        line-height: 22px; /* same as #progressBar height if we want text middle aligned */
        width: 0;
        background-color: #0ba1b5; border-radius: 3px; 
    }
    .statusbar{
        border-top:1px solid #A9CCD1;
        min-height:25px;
        width:99%;
        padding:10px 10px 0px 10px;
        vertical-align:top;
    }
    .statusbar:nth-child(odd){
        background:#EBEFF0;
    }
    .filename{
        display:inline-block;
        vertical-align:top;
        width:250px;
    }
    .filesize{
        display:inline-block;
        vertical-align:top;
        color:#30693D;
        width:100px;
        margin-left:10px;
        margin-right:5px;
    }
    .abort{
        background-color:#A8352F;
        -moz-border-radius:4px;
        -webkit-border-radius:4px;
        border-radius:4px;display:inline-block;
        color:#fff;
        font-family:arial;font-size:13px;font-weight:normal;
        padding:4px 15px;
        cursor:pointer;
        vertical-align:top
    }
    .deletePic{
        background-color:#5DADE2;
        -moz-border-radius:4px;
        -webkit-border-radius:4px;
        border-radius:4px;display:inline-block;
        color:#fff;
        font-family:arial;font-size:13px;font-weight:normal;
        padding:4px 15px;
        cursor:pointer;
        vertical-align:top
    }
</style>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="/"/> class="logo"><img src="<c:url value = "/resources/images/NPP_logo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="index.html">home</a></li>
<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
	<ul>
		<li><a href="index-1.html">who we are</a></li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">find an agent<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="index-2.html">services</a></li>
<li><a href="index-3.html">SAles</a></li>
<li class="active"><a href="index-4.html">resources</a></li>											
<li><a href="index-5.html">gallery</a></li>											
<li><a href="index-6.html">contact us</a></li>											
    </ul>
		</div>
	</div>
</div>
<div id="search">
<!-- 맨 위 오른쪽 검색 부분 -->
<a href="#" class="searchBtn"></a>

<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>숙소 등록하기</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>사진 첨부</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			게스트들이 여러분들의 방에대해 알 수 있도록 사진을 넣어주세요. 사진은 최대 6장까지만 등록 가능합니다.
		</div>
		<br>
		<div>
			<div id="fileUpload" class="dragAndDropDiv">Drag & Drop Files Here</div>
		</div>
		<br>
		<div>
			<button class="btn" onclick="insertPic()">사진등록</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo2.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>