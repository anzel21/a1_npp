<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>숙소 위치 설정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href=<c:url value = "/resources/images/favicon.ico" /> type="image/x-icon">
<link rel="shortcut icon" href=<c:url value = "/resources/images/favicon.ico"/> type="image/x-icon" />

<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap.css" /> type="text/css" media="screen">
<link rel="stylesheet" href=<c:url value = "/resources/css/bootstrap-responsive.css" /> type="text/css" media="screen">    
<link rel="stylesheet" href=<c:url value = "/resources/css/style.css" /> type="text/css" media="screen">

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.js" /> ></script>  
<script type="text/javascript" src=<c:url value = "/resources/js/jquery.easing.1.3.js"/> ></script>
<script type="text/javascript" src=<c:url value = "/resources/js/superfish.js"/> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/jquery.ui.totop.js" /> ></script>

<script type="text/javascript" src=<c:url value = "/resources/js/script.js"/> ></script>

<script>
$(document).ready(function() {



}); //
$(window).load(function() {
	//

}); //

</script>		
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="not-front">
<div id="main">

<div class="top1_wrapper">
<div class="top1 clearfix">
<!-- 상위 메뉴바 -->
<header><div class="logo_wrapper"><a href=<c:url value="/"/> class="logo"><img src="<c:url value = "/resources/images/NPP_logo.png" />" alt="" class="logoPic"></a></div></header>
<div class="menu_wrapper clearfix">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
<li><a href="index.html">home</a></li>
<li class="sub-menu sub-menu-1"><a href="index-1.html">about</a>
	<ul>
		<li><a href="index-1.html">who we are</a></li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">find an agent<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>
		<li class="sub-menu sub-menu-2"><a href="index-1.html">history<em></em></a>
			<ul>
				<li><a href="index-1.html">Lorem ipsum dolor</a></li>
				<li><a href="index-1.html">Sit amet</a></li>
				<li><a href="index-1.html">Adipiscing elit</a></li>
				<li><a href="index-1.html">Nunc suscipit</a></li>
				<li><a href="404.html">404 page not found</a></li>
			</ul>
		</li>		
		<li><a href="index-1.html">testimonials</a></li>						
	</ul>						
</li>
<li><a href="index-2.html">services</a></li>
<li><a href="index-3.html">SAles</a></li>
<li class="active"><a href="index-4.html">resources</a></li>											
<li><a href="index-5.html">gallery</a></li>											
<li><a href="index-6.html">contact us</a></li>											
    </ul>
		</div>
	</div>
</div>
<div id="search">
<!-- 맨 위 오른쪽 검색 부분 -->
<a href="#" class="searchBtn"></a>

<div class="search-form-wrapper">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>

</div>	
</div>

</div>	
</div>

<!-- 숙소 명칭 나오는 부분 -->
<div id="inner">

<div class="top2_wrapper">
<div class="bg1"><img src=<c:url value = "/resources/images/bg1.jpg" /> alt="Company Logo" class="img"></div>
	<div class="top2_inner">
		<div class="container">
			<div class="top2 clearfix">
				
				<h1>숙소 등록하기</h1>
			
			</div>	
		</div>	
	</div>
</div>

<!-- 본격적인 설명부 -->

<div id="content">
<div class="container">
<div class="row">
<div class="span9">
	
<h2><span>숙소 위치</span></h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<div class="caption">
			숙소의 위치를 표시하세요
		</div>
		<div>
			<input type="text" id="address" class="addressInput" placeholder="도로명주소/지번주소를 입력하세요" >
			<button class="btn" onclick='searchAddress()'>주소찾기</button>
		</div>
		<div>
		<div id="map" style="width:100%;height:350px;"></div>
		<p id="result"></p>
			<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8658714a2fcbb6223cdc4988e317c0cb&libraries=services"></script>
			<script>
			 
			var saved = false;
			var mapContainer = document.getElementById('map'); // 지도를 표시할 div 
				mapContainer.style.width = '870px';
			    mapContainer.style.height = '550px';
			    mapOption = {
			        center: new daum.maps.LatLng(37.56677020886982, 126.9790201433088), // 지도의 중심좌표
			        level: 3 // 지도의 확대 레벨
			    };  
			
			// 지도를 생성합니다    
			var map = new daum.maps.Map(mapContainer, mapOption); 
			
			// 장소 검색 객체를 생성합니다
			var ps = new daum.maps.services.Places(map); 
			
			// 마커를 표시할 곳
			var markerPosition = new daum.maps.LatLng(37.56677020886982, 126.9790201433088);
			
			// 마커 생성
			var marker = new daum.maps.Marker({
				position:markerPosition
			});
			
			// 마커 표시
			marker.setMap(map);
			
			// 지도 타입 변경 컨트롤을 생성한다
			var mapTypeControl = new daum.maps.MapTypeControl();
			
			// 지도의 상단 우측에 지도 타입 변경 컨트롤을 추가한다
			map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);	
			
			// 지도에 확대 축소 컨트롤을 생성한다
			var zoomControl = new daum.maps.ZoomControl();
			
			// 지도의 우측에 확대 축소 컨트롤을 추가한다
			map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);
			
			// 맵 중앙이 바뀌었을 시 이벤트를 추가한다
			daum.maps.event.addListener(map, 'center_changed', function(){
				// 맵의 중앙 좌표 구하기
				var latlng = map.getCenter();
				// 마커가 들어갈 곳을 설정한다 (위도, 경도)
				markerPosition = new daum.maps.LatLng(latlng.getLat(), latlng.getLng());
				// 기존의 마커를 지운다
				marker.setMap(null);
				// 마커를 생성한다
				marker = new daum.maps.Marker({
					position:markerPosition
				});
				// 마커를 표시한다
				marker.setMap(map);
			});
			
			// 주소로 좌표찾기
			function searchAddress() {
				// 주소를 받는다
				var address = $("#address").val();
				// 주소-좌표 변환 객체 생성
				var geocoder = new daum.maps.services.Geocoder();
				// 주소로 좌표 검색
				geocoder.addressSearch(address, function(result, status){
					// 결과가 있을경우
					if (status == daum.maps.services.Status.OK) {
						var coords = new daum.maps.LatLng(result[0].y, result[0].x);
						// 기존의 마커는 제거
						marker.setMap(null);
						// 결과값으로 받은 위치를 마커로 표시한다
						marker = new daum.maps.Marker({
							map: map,
							position:coords
						});
						// 지도의 중심을 마커가 표시된 곳으로 바꾼다
						map.setCenter(coords);
					} else if (status == daum.maps.services.Status.ZERO_RESULT) {
						alert('결과가 없습니다. 지번주소/도로명주소를 정확히 입력했는지 확인해주세요');
					} else if (status == daum.maps.services.Status.ERROR) {
						alert('인터넷 연결이 좋지 않습니다. 다시 시도하세요');
					} 
					
				});
				
			}
			
			function setLocation(callback) {
				var position = map.getCenter();
				var callback = function(result,status) {
					if (status == daum.maps.services.Status.OK) {
						
						var check = confirm("주소 : " + result[0].address.address_name
								            + "\n저장하시겠습니까?");
						
						if (check == true) {
							$.ajax({
								url: 'saveLoc',
								type: 'POST',
								data: {latitude: position.getLat(), longitude: position.getLng()},
								error: function(e) {
									alert(e);
								}
							});
							saved = true;
						}
					}
				};
				
				// 주소-좌표 변환 객체를 생성합니다
				var geocoder = new daum.maps.services.Geocoder();
				
				geocoder.coord2Address(position.getLng(), position.getLat(), callback);
			};
			
			function chkLoc() {
				if (saved == false) {
					alert('먼저 숙소의 위치를 저장해주세요');
				} else {
					location.href = 'ammenities';
				}
			}
			</script>
			<button class="btn" onclick="setLocation()">위치저장</button>
		</div>
		<br>
		<div>
			<button class="btn" onclick="location.href='bathroom'">이전으로</button>
			&nbsp;
			<button class="btn" onclick="chkLoc()">다음으로</button>
		</div>
	</div>
	
</div>

</div>

</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="bot1 clearfix">
<div class="row">
<div class="span3">

<div class="bot1_title">Copyright</div>	
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo2.png" alt=""></a></div>

<footer><div class="copyright">Copyright © 2014.<br>All rights reserved.</div></footer>

</div>	
<div class="span5">

<div class="bot1_title">Useful information</div>

<p>
	<b>
		Nulla ultricies enim aliquet augue eleifend iaculis.
	</b>
</p>

<p>
	Nam sollicitudin ligula ac nisi iaculis eu scelerisque risus ultricies. Nullam eu elit risus, vel interdum urna. Aenean leo nulla, aliquet vitae ultricies sit amet, porttitor id sapien. In hac habitasse platea dictumst. Donec pharetra gravida augue at hendrerit. Cras ut...
</p>

</div>

</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src=<c:url value = "/resources/js/bootstrap.js" />></script>
</body>
</html>