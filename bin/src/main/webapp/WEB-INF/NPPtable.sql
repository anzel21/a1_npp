<<<<<<< HEAD
create sequence roomIDSequence;

create table roomInfo 
(
    roomID number(5) not null primary key
    , hostID varchar2(30) not null
    , location varchar2(300) not null
    , latitude number not null
    , longitude number not null
    , bedroom number(2) not null
    , bathroom number(2) not null
    , bathroomType number(1) default 0
    , price number(10) not null
    , roomExplain varchar2(1000) not null
    , amnenities varchar2(500) not null
    , accommodation number(3) not null
    , constraint FK_hostID foreign key(hostId) references webmember(ID)
=======
create table webMember (
    id varchar2(30) primary key
    ,pw varchar2(15) not null
    ,name varchar2(10) not null
    ,birthdate varchar2(10) not null
    ,phonenumber varchar2(25) not null
    ,type number(2) default 0
);

create table socialMember (
   	emailId varchar2(30) primary key
    ,identifier varchar2(15) unique
    ,name varchar2(10) 
    ,birthdate varchar2(10)
    ,gender varchar2(2)
    ,type number(2) default 0
    ,logintype varchar2(10) 
>>>>>>> origin/jw1
);

create table assessment (
    assessnum number not null primary key
    ,convenience number not null
    ,convereason varchar2(1000)
    ,accessibility number not null
    ,accessreason varchar2(1000)
    ,kindness number not null
    ,kindreason varchar2(1000)
    ,sanitation number not null
    ,sanireason varchar2(1000)
    ,surroundings number not null
    ,surroundreason varchar2(1000)
    ,evaluation varchar2(1000)
);

create sequence assessnum_seq;