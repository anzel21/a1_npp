// 회원가입시 입력받는 데이터 Ajax & 유효성검사
function joinData() {
	var msg;
	$.ajax({
		url: 'joinData',
		type: 'POST',
		data: $('#joinData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "아이디를 입력해주세요." || msg == "특수기호는 입력하실 수 없습니다." 
				|| msg == "아이디는 15자 이내로 입력해주세요." || msg == "동일한 아이디가 존제합니다.") {
				$('#idErrDiv').html(msg);
				$('#pwErrDiv').html("");
				$('#pwErrDiv2').html("");
				$('#nameErrDiv').html("");
				$('#phoneErrDiv').html("");
				$('#birthDateErrDiv').html("");
				
				return false;
			}
			if(msg == "비밀번호를 입력해주세요." || msg == "비밀번호는 5~15자리 이내로 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#pwErrDiv').html(msg);
				$('#pwErrDiv2').html("");
				$('#nameErrDiv').html("");
				$('#phoneErrDiv').html("");
				$('#birthDateErrDiv').html("");
				return false;
			}
			if(msg == "비밀번호를 입력해주세요. " || msg == "비밀번호가 일치하지 않습니다." || msg == "비밀번호는 5~15자리 이내로 입력해주세요. ") {
				$('#idErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#pwErrDiv2').html(msg);
				$('#nameErrDiv').html("");
				$('#phoneErrDiv').html("");
				$('#birthDateErrDiv').html("");
				return false;
			}
			if(msg == "이름을 입력해주세요." || msg == "잘못된 이름입니다.") {
				$('#idErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#pwErrDiv2').html("");
				$('#nameErrDiv').html(msg);
				$('#phoneErrDiv').html("");
				$('#birthDateErrDiv').html("");
				return false;
			}
			if(msg == "국가번호를 선택해주세요." || msg == "전화번호를 입력해주세요." || msg == "전화번호는 숫자로만 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#pwErrDiv2').html("");
				$('#nameErrDiv').html("");
				$('#phoneErrDiv').html(msg);
				$('#birthDateErrDiv').html("");
				return false;
			}
			if(msg == "생년월일을 입력해주세요." || msg == "생년월일은 숫자로만 입력해주세요." || msg == "생년월일 8자리를 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#pwErrDiv2').html("");
				$('#nameErrDiv').html("");
				$('#phoneErrDiv').html("");
				$('#birthDateErrDiv').html(msg);
				return false;
			}
			else {
			location.href="/../npp";
			}
		},
		error: function (e) {
			alert('저장 실패');				
		}
	});
}

//뒤로가기
function goBack() {
	history.back();
}