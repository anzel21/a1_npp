// 회원정보 수정시 입력받는 데이터 Ajax & 유효성검사 (Web)
function webInfoData() {
	var msg;
	$.ajax({
		url: 'webInfoData',
		type: 'POST',
		data: $('#userInfoData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "비밀번호는 5~15자리 이내로 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html(msg);
				$('#birthDateErrDiv').html("");
				$('#phoneErrDiv').html("");
				return false;
			}
			if(msg == "국가번호를 선택해주세요." || msg == "전화번호를 입력해주세요." || msg == "전화번호는 숫자로만 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#birthDateErrDiv').html("");
				$('#phoneErrDiv').html(msg);
				return false;
			}
			if(msg == "생년월일을 입력해주세요." || msg == "생년월일은 숫자로만 입력해주세요." || msg == "생년월일 8자리를 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#birthDateErrDiv').html(msg);
				$('#phoneErrDiv').html("");
				return false;
			}
			else {
			location.reload();
			}
		},
		error: function (e) {
			alert('저장 실패');				
		}
	});
};

function socialInfoData() {
	var msg;
	$.ajax({
		url: 'socialInfoData',
		type: 'POST',
		data: $('#userInfoData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "비밀번호는 5~15자리 이내로 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html(msg);
				$('#birthDateErrDiv').html("");
				$('#phoneErrDiv').html("");
				return false;
			}
			if(msg == "국가번호를 선택해주세요." || msg == "전화번호를 입력해주세요." || msg == "전화번호는 숫자로만 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#birthDateErrDiv').html("");
				$('#phoneErrDiv').html(msg);
				return false;
			}
			if(msg == "생년월일을 입력해주세요." || msg == "생년월일은 숫자로만 입력해주세요." || msg == "생년월일 8자리를 입력해주세요.") {
				$('#idErrDiv').html("");
				$('#nameErrDiv').html("");
				$('#pwErrDiv').html("");
				$('#birthDateErrDiv').html(msg);
				$('#phoneErrDiv').html("");
				return false;
			}
			else {
			location.reload();
			}
		},
		error: function (e) {
			alert('저장 실패');				
		}
	});
};
//뒤로가기
function goBack() {
	history.back();
};