//JW_Login 화면 동작 기능
function openLoginPage() {
	document.getElementById('LoginDiv').style.display = 'block';
	$('#LoginDiv').css('height','460px');
	$('#LoginDiv').css('margin-left','-340px');
	$('#LoginDiv').css('margin-top','-60px');
	document.getElementById('LoginDivOverlay').style.display = 'block';
	// JW_마우스 스크롤 비활성화
	$(document)
			.on(
					"mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll",
					function(e) {
						e.preventDefault();
						return;
					});
	$(document).on("keydown.disableScroll", function(e) {
		var eventKeyArray = [ 32, 33, 34, 35, 36, 37, 38, 39, 40 ];
		for (var i = 0; i < eventKeyArray.length; i++) {
			if (e.keyCode === eventKeyArray[i]) {
				e.preventDefault();
				return;
			}
		}
	});
}
// JW_Login 화면 종료 기능
function loginOverlay() {
	document.getElementById('LoginDiv').style.display = 'none';
	document.getElementById('LoginDivOverlay').style.display = 'none';
	// JW_마우스 스크롤 활성화
	$(document).off(".disableScroll");
}

//회원가입 폼으로 이동
function goJoinForm() {
	location.href = "./joinForm";
}

//로그인시 입력받는 데이터 Ajax & 유효성검사
function loginData() {
	var msg;
	$.ajax({
		url: './loginData',
		type: 'POST',
		data: $('#loginData').serialize(),
		dataType:'text',
		success : function(errorMsg) {
			msg = errorMsg;
			if(msg == "아이디를 입력해주세요." || msg == "없는 아이디 입니다.") {
				$('#login_idErrDiv').html(msg);
				$('#login_pwErrDiv').html("");
				return false;
			}
			if(msg == "비밀번호를 입력해주세요." || msg == "비밀번호가 일치하지 않습니다.") {
				$('#login_idErrDiv').html("");
				$('#login_pwErrDiv').html(msg);
				return false;
			}
			else {
				location.reload();
			}
		},
		error: function (e) {
			alert('저장 실패');				
		}
	});
}

